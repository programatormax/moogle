{

"header": {
  "guid": "{FF58F93F-13C4-4740-B398-DDEA7C207EFE}",
  "type": "{502FFDC5-DF55-47A3-A516-CD2A6F687270}",
  "load": "static",
  "dependence": "{00000000-0000-0000-0000-000000000000}",
  "description": "Addons for options", 
  "version": "1.0.0.0"
 },

"body": {

  "option": {

    "proxy": {
      "desc": "Proxy settings",
      "hidden": false,
      "enable": {
        "desc": "Use proxy",
        "hidden": false,
        "readonly": false,
        "value": false
       },

      "ip": {
        "desc": "Address",
        "hidden": false,
        "readonly": false,
        "value": "39.107.183.55"
        },

      "port": {
        "desc": "Port",
        "hidden": false,
        "readonly": false,
        "value": 3128
        },

      "protocol": {
        "desc": "Protocol",
        "hidden": false,
        "readonly": false,
        "value": "http"
        },

      "country": {
        "desc": "Country",
        "hidden": false,
        "readonly": false,
        "value": "China"
        },

      "type": {
        "desc": "Security",
        "hidden": false,
        "readonly": false,
        "value": "transparent"
       }

     },

   "search3": {
      "desc": "Search configuration 4",
      "hidden": false,

      "name": {
        "desc": "Name of search system",
        "hidden": false,
        "readonly": false,
        "value": "DuckDuck"
        },

       "link": {
        "desc": "Basic link",
        "hidden": false,
        "readonly": false,
        "value": ""
        },

      "pattern": {
        "desc": "Pattern link",
        "hidden": false,
        "readonly": false,
        "value": "https://duckduckgo.com/?q=%Search%"
        },

      "enable": {
        "desc": "Involved",
        "hidden": false,
        "readonly": false,
        "value": true
       },

      "index": {
       "desc": "Index",
       "hidden": false,
       "readonly": false,
       "value": 3
      }

     },

   "search2": {
      "desc": "Search configuration 3",
      "hidden": false,

      "name": {
        "desc": "Name of search system",
        "hidden": false,
        "readonly": false,
        "value": "Yahoo"
        },

       "link": {
        "desc": "Basic link",
        "hidden": false,
        "readonly": false,
        "value": ""
        },

      "pattern": {
        "desc": "Pattern link",
        "hidden": false,
        "readonly": false,
        "value": "https:\/\/search.yahoo.com\/search?p=%Search%&fr=yfp-t&ei=UTF-8&fp=1"
        },

      "enable": {
        "desc": "Involved",
        "hidden": false,
        "readonly": false,
        "value": true
       },

     "index": {
       "desc": "Index",
       "hidden": false,
       "readonly": false,
       "value": 2
      }

     },

   "search1": {
      "desc": "Search configuration 2",
      "hidden": false,

      "name": {
        "desc": "Name of search system",
        "hidden": false,
        "readonly": false,
        "value": "Bing"
        },

       "link": {
        "desc": "Basic link",
        "hidden": false,
        "readonly": false,
        "value": ""
        },

      "pattern": {
        "desc": "Pattern link",
        "hidden": false,
        "readonly": false,
        "value": "https:\/\/www.bing.com\/search?q=%Search%"
        },

      "enable": {
        "desc": "Involved",
        "hidden": false,
        "readonly": false,
        "value": true
       },

     "index": {
       "desc": "Index",
       "hidden": false,
       "readonly": false,
       "value": 1
      }

     },

   "search0": {
      "desc": "Search configuration 1",
      "hidden": false,

      "name": {
        "desc": "Name of search system",
        "hidden": false,
        "readonly": false,
        "value": "Google"
        },

       "link": {
        "desc": "Basic link",
        "hidden": false,
        "readonly": false,
        "value": ""
        },

      "pattern": {
        "desc": "Pattern link",
        "hidden": false,
        "readonly": false,
        "value": "https:\/\/www.google.com\/search?source=hp&ei=ClRGYKf1O8qWa8W7tJFx&iflsig=AINFCbYAAAAAYEZiG3MpJU-YofHuctJ4aCtjE87WoKWd&q=%Search%"
        },

      "enable": {
        "desc": "Involved",
        "hidden": false,
        "readonly": false,
        "value": true
       },

      "index": {
       "desc": "Index",
       "hidden": false,
       "readonly": false,
       "value": 0
      }

     },

    "language": {
      "desc": "Current Language",
      "hidden": false,
      "name": {
        "desc": "Value",
        "hidden": false,
        "readonly": false,
        "value": "English"
        },
      },


     "page": {
      "desc": "Page control",
      "hidden": false,
      "enable": {
        "desc": "Auto start home page",
        "hidden": false,
        "readonly": false,
        "value": false
       },
      "home": {
        "desc": "Home page",
        "hidden": false,
        "readonly": false,
        "value": ""
       },
  
      "last": {
        "desc": "Last page",
        "hidden": false,
        "readonly": false,
        "value": ""
       }
     },


   "storage": {
      "desc": "Storages",
      "hidden": false,
      "download": {
        "desc": "Download directory",
        "hidden": false,
        "readonly": false,
        "value": "C:\\ProgramData\\TAO\\Downloads\\"
        }
      },

   "cookies": {
      "desc": "Cookies",
      "hidden": false,
      "clear": {
        "desc": "Clear cookies after exit browser",
        "hidden": false,
        "readonly": false,
        "value": false,
        }
      },

   "other": {
      "desc": "Other",
      "hidden": false,
      "import": {
        "desc": "Import data from other browsers",
        "hidden": false,
        "readonly": false,
        "value": true,
        },

      "sync": {
        "desc": "Synchronization with cloud",
        "hidden": false,
        "readonly": false,
        "value": false,
         },

      "cloud": {
        "desc": "Cloud address",
        "hidden": false,
        "readonly": false,
        "value": "127.0.0.1",
        },

      "vpnpath": {
        "desc": "Open vpn default path",
        "hidden": false,
        "readonly": false,
        "value": "C:\\Program Files\\OpenVPN\\bin\\",
        }

      }
    
  }
 }
}