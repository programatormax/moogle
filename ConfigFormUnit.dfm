object ConfigForm: TConfigForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Whois'
  ClientHeight = 853
  ClientWidth = 958
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 958
    Height = 16
    Align = alTop
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitWidth = 4
  end
  object ValueListEditor1: TValueListEditor
    Left = 0
    Top = 329
    Width = 958
    Height = 524
    Align = alClient
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goAlwaysShowEditor, goThumbTracking]
    TabOrder = 0
    TitleCaptions.Strings = (
      'Parameter'
      'Value')
    ColWidths = (
      475
      477)
  end
  object Memo1: TMemo
    Left = 0
    Top = 16
    Width = 958
    Height = 313
    Align = alTop
    BorderStyle = bsNone
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object IdWhois1: TIdWhois
    Host = 'whois.iana.org'
    Left = 12
    Top = 31
  end
end
