unit StorageData;

interface

type

  TDictionary = record
    N: String;
    M: String;
    V: String;
    T: String;
  end;

   TLinkTypeSearch = record
     F: Byte;
     N: String;
     P: String;
   end;

const

   DICTIONARY: array [0..37] of TDictionary = (
     (N: 'google'; M: '/search'; V: 'q'; T: 'search'),
     (N: 'yahoo'; M: '/search'; V: 'p'; T: 'search'),
     (N: 'bing'; M: '/search'; V: 'q'; T: 'search'),
     (N: 'duckduckgo'; M: '/'; V: 'q'; T: 'search'),
     (N: 'yandex'; M: '/search'; V: 'text'; T: 'search'),
     (N: 'mail'; M: '/search'; V: 'q'; T: 'search'),
     (N: 'pornhub'; M: '/video/search'; V: 'search'; T: 'search'),
     (N: 'redtube'; M: '/'; V: 'search'; T: 'search'),
     (N: 'vk'; M: '/search'; V: 'cq'; T: 'search'),
     (N: 'fandom'; M: '/wiki/special:search'; V: 'query'; T: 'search'),
     (N: 'youtube'; M: '/results'; V: 'search_query'; T: 'search'),
     (N: 'aliexpress'; M: '/wholesale'; V: 'searchtext'; T: 'search'),
     (N: 'wikipedia'; M: '/wiki/special:search'; V: 'search'; T: 'search'),
     (N: 'rutube'; M: '/search'; V: 'query'; T: 'search'),
     (N: 'rambler'; M: '/search'; V: 'query'; T: 'search'),
     (N: 'booking'; M: '/'; V: 'ss'; T: 'search'),      //?????
     (N: 'hh'; M: '/search/vacancy'; V: 'text'; T: 'search'),
     (N: 'youtube'; M: '/watch'; V: 'v'; T: 'view'),
     (N: 'pornhub'; M: '/view_video.php'; V: 'viewkey'; T: 'view'),
     (N: 'redtube'; M: ''; V: '0'; T: 'view'),
     (N: 'pochta'; M: ''; V: '1'; T: 'search'),
     (N: 'postaonline'; M: '/trackandtrace/-/zasilka/cislo'; V: 'parcelnumbers'; T: 'track'),
     (N: 'stripchat'; M: ''; V: '0'; T: 'view'),
     (N: 'rokfin'; M: ''; V: '2'; T: 'view'),
     (N: 'rumble'; M: '/search/video'; V: 'q'; T: 'search'),
     (N: 'rumble'; M: ''; V: '0'; T: 'view'),
     (N: 'odysee'; M: '/$/search'; V: 'q'; T: 'search'),
     (N: 'odysee'; M: ''; V: '1'; T: 'view'),
     (N: 'booking'; M: ''; V: 'ss'; T: 'search'),
     (N: 'resulthunter'; M: ''; V: 'q'; T: 'search'),
     (N: 'upwork'; M: ''; V: 'q'; T: 'search'),
     (N: 'amazon'; M: ''; V: 'k'; T: 'search'),
     (N: 'usps'; M: '/go/trackconfirmaction'; V: 'qtc_tlabels1'; T: 'track'),
     (N: 'usps'; M: '/go/trackconfirmaction'; V: 'tlabels'; T: 'track'),
     (N: 'aliexpress'; M: '/logisticsdetail.htm'; V: 'tradeid'; T: 'track'),
     (N: 'fedex'; M: '/fedextrack'; V: 'trknbr'; T: 'track'),
     (N: 'dhl'; M: ''; V: 'tracking-id'; T: 'track'),
     (N: 'amazon'; M: '/s'; V: 'k'; T: 'search')
   );

implementation

end.
