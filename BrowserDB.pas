unit BrowserDB;

interface

uses

  Windows,
  SysUtils,
  Classes,
  DateUtils,
  SqliteTable3,
  Vcl.ComCtrls,
  SHFolder,
  SuperObject,
  IdURI,
  IdWhois,
  Winsock2,
  RestApiClient,
  IniFiles,
  HTTPApp,
  WinApi.RichEdit,
  WinApi.ShellApi,
  Vcl.Controls,
  Vcl.Graphics,
  StdCtrls,
  ExtCtrls,
  EntityProjectUnit;

const

  TABLE_NAME1 = 'browser';
  TABLE_NAME2 = 'settings';

  TYPE_SEARCH         = 0;
  TYPE_LINK           = 1;
  TYPE_START          = 3;
  TYPE_STOP           = 4;
  TYPE_BOOKMARK       = 5;
  TYPE_FAVORITE       = 6;
  TYPE_DOWNLOAD       = 7;
  TYPE_FILE           = 8;
  TYPE_FAULT          = 9;
  TYPE_HOME           = 10;
  TYPE_PACKET_SUCCESS = 11;
  TYPE_PACKET_FAULT   = 12;
  TYPE_WHOIS          = 13;
  TYPE_MYIP           = 14;

type

  TNotebookNode = class
  private
    FIcon: Integer;
    FDesc: String;
  public
    property Icon: Integer read FIcon write FIcon;
    property Desc: String read FDesc write FDesc;
  end;

  TBrowserDataBase = class
  private
    FIdWhois1: TIdWhois;
    FMemo: TStringList;
    FValueListEditor: TStringList;
    FSqlite: TSQLiteDatabase;
    FMoogle: ISuperObject;
    FBookmark: ISuperObject;
    procedure Reset;
    function IsItIPV4(Str: String): Boolean;
    function IsAvalible(ipAddressStr:AnsiString;dwPort: Word): Boolean;
    function Whois(Whois: String; var Refer: String): Boolean;
    function GetDatabaseFile: String;
    function GetInstallDir: String;
    function GetPacketDir: String;
    function GetDatabaseDir: String;
    function CheckPacket(G: String): Boolean;
    procedure ImportPacketObject(Pck: ISuperObject);
    procedure ImportPacketFile(FileName: String);
    procedure ImportPacketList;
    function DNWhois(Str: String; var Html: String): Boolean;
    function DNProxy(Str: String; var Html: String): Boolean;
    function DNMyip(Str: String; var Html: String): Boolean;
    function DNVpn(Str: String; var Html: String): Boolean;
  public
    property DB: ISuperObject read FMoogle;
    function Clear: Boolean;
    function MyIP(var IP: String; var Country: String): Boolean;
    function TinyUrl(Url: String): String;
    function DarkNet(Str: String; var Html: String): Boolean;
    function Parser(Link: String; I: Cardinal; Def: String): String;
    procedure GetListOptions(List: TListView);
    procedure GetListProxy(List: TTreeView);
    procedure GetListAnonymous(List: TTreeView);
    function GetListHistory(List: TListView; Search: String): Boolean;
    function History(Id: UInt64; Str: AnsiString): Boolean;
    function IsDomain(Str: AnsiString): Boolean;
    function IsDownload(Str: AnsiString): Boolean;
    function GetDownloadFile(Str: String): String;
    function ProxyPage(Addr,Port,Protocol,Desc: String; var Html: String): Boolean;
    function Dictionary(Id: Word): String;
    function ReadString(Par: String; Def: string): string;
    function WriteString(Par: String; Val: string): Boolean;
    function ReadJson(Par: String; Def: ISuperObject): ISuperObject;
    function WriteJson(Par: String; Val: ISuperObject): Boolean;
    function ClearNotebook(NoteBookView1: TTreeView): Boolean;
    function LoadNotebook(NoteBookView1: TTreeView): Boolean;
    function SaveNotebook(NoteBookView1: TTreeView): Boolean;
    function ExportNotebook(FileName: String): Boolean;
    function ImportNotebook(FileName: String): Boolean;
    function RtfdNotebook(Rtf: TRichEdit): Boolean;
    function LoadSearch(Strings: TStrings; V: Boolean): Boolean;
    procedure QuickUpdate;
    constructor Create;
    destructor Destroy; override;
  end;

function NodeToRtf(Node: TTreeNode; RTF: TRichEdit): Boolean;
function RtfToNode(Node: TTreeNode; RTF: TRichEdit): Boolean;

implementation

procedure InsertHyperLink(Rtf: TRichEdit; const HyperlinkText: string; const HyperlinkURL: string = '');
var
  HyperlinkPrefix, FullHyperlink: string;
  Fmt: CHARFORMAT2;
  StartPos: Integer;
begin
  if HyperlinkURL <> '' then
  begin
    HyperlinkPrefix := Format('HYPERLINK "%s"', [HyperlinkURL]);
    FullHyperlink := HyperlinkPrefix + HyperlinkText;
  end else begin
    FullHyperlink := HyperlinkText;
  end;
  StartPos := Rtf.SelStart;
  Rtf.SelText := FullHyperlink;
  Rtf.SelStart := StartPos;
  Rtf.SelLength := Length(FullHyperlink);
  FillChar(Fmt, SizeOf(Fmt), 0);
  Fmt.cbSize := SizeOf(Fmt);
  Fmt.dwMask := CFM_LINK;
  Fmt.dwEffects := CFE_LINK;
  if HyperlinkURL<>'' then
  begin
    // per MSDN: "RichEdit doesn�t allow the CFE_LINKPROTECTED attribute to be
    // set directly by programs. Maybe it will allow it someday after enough
    // testing is completed to ensure that things cannot go awry"...
    //
    {
    Fmt.dwMask := Fmt.dwMask or CFM_LINKPROTECTED;
    Fmt.dwEffects := Fmt.dwEffects or CFE_LINKPROTECTED;
    }
  end;
  SendMessage(Rtf.Handle, EM_SETCHARFORMAT, SCF_SELECTION, LPARAM(@Fmt));
  if HyperlinkURL <> '' then
  begin
    Rtf.SelStart := StartPos;
    Rtf.SelLength := Length(HyperlinkPrefix);
    FillChar(Fmt, SizeOf(Fmt), 0);
    Fmt.cbSize := SizeOf(Fmt);
    Fmt.dwMask := CFM_HIDDEN;
    Fmt.dwEffects := CFE_HIDDEN;
    SendMessage(Rtf.Handle, EM_SETCHARFORMAT, SCF_SELECTION, LPARAM(@Fmt));
  end;
  Rtf.SelStart:=StartPos + Length(FullHyperlink);
  Rtf.SelLength:=0;
end;

function RtfToNode(Node: TTreeNode; RTF: TRichEdit): Boolean;
var S: String;
    n: Integer;
    q: Integer;
    i: Integer;
begin
  if (Assigned(RTF)=True) and (Assigned(Node)=True) and (Rtf.Lines.Count>0) then
  begin
    S:='';
    q:=0;
    i:=0;
    for n:=0 to Rtf.Lines.Count-1 do
    begin
      if Rtf.Lines.Strings[n]='' then q:=q+1;
      if q=1 then
      begin
        i:=n+1;
        Break;
      end;
    end;
    if q=1 then
    begin
      for n:=i to Rtf.Lines.Count-1 do
      begin
        S:=S+Trim(Rtf.Lines.Strings[n])+#13#10;
      end;
    end
    else
    begin
      S:=Rtf.Lines.Text;
    end;
    TNotebookNode(Node.Data).Desc:=Trim(S);
  end
  else
  begin
    Result:=False;
  end;
end;

function NodeToRtf(Node: TTreeNode; RTF: TRichEdit): Boolean;
begin
  if Assigned(RTF)=True then
  begin
    RTF.Lines.BeginUpdate;
    RTF.Lines.Clear;
    if Assigned(Node)=True then
    begin
      if Node.ImageIndex=6 then
      begin
        RTF.SelAttributes.Style:=[fsBold];
        RTF.SelAttributes.Size:=18;
        //if Root=True then RTF.SelAttributes.Size:=32 else RTF.SelAttributes.Size:=18;
        RTF.SelText:=Trim(Node.Text);
        RTF.SelText:=#13#10;
        RTF.SelText:=#13#10;
        if (Assigned(Node.Data)=True) and (TNotebookNode(Node.Data).Desc<>'') then
        begin
          RTF.SelAttributes.Style:=[];
          RTF.SelAttributes.Size:=16;
          RTF.SelText:=Trim(TNotebookNode(Node.Data).Desc);
          RTF.SelText:=#13#10;
          RTF.SelText:=#13#10;
        end;
      end
      else
      begin
        InsertHyperLink(RTF,Node.Text,Node.Text);
        RTF.SelAttributes.Style:=[];
        RTF.SelAttributes.Size:=16;
        RTF.SelText:=#13#10;
        RTF.SelText:=#13#10;
        if (Assigned(Node.Data)=True) and (TNotebookNode(Node.Data).Desc<>'') then
        begin
          RTF.SelAttributes.Style:=[];
          RTF.SelAttributes.Size:=16;
          RTF.SelText:=Trim(TNotebookNode(Node.Data).Desc);
          RTF.SelText:=#13#10;
          RTF.SelText:=#13#10;
        end;
      end;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
    Rtf.Lines.EndUpdate;
  end
  else
  begin
    Result:=False;
  end;
end;

function JsonToRtf(Obj: ISuperObject; Root: Boolean; Rtf: TRichEdit): TTreeNode;
var q: Integer;
begin
  if Obj.I['image']=6 then
  begin
    RTF.SelAttributes.Style:=[fsBold];
    if Root=True then RTF.SelAttributes.Size:=32 else RTF.SelAttributes.Size:=18;
    RTF.SelText:=Trim(Obj.S['name']);
    RTF.SelText:=#13#10;
    RTF.SelText:=#13#10;
    if Obj.S['desc']<>'' then
    begin
      RTF.SelAttributes.Style:=[];
      RTF.SelAttributes.Size:=16;
      RTF.SelText:=Trim(Obj.S['desc']);
      RTF.SelText:=#13#10;
      RTF.SelText:=#13#10;
    end;
  end
  else
  begin
    if Obj.S['desc']<>'' then
    begin
      RTF.SelAttributes.Style:=[];
      RTF.SelAttributes.Size:=16;
      RTF.SelText:=Trim(Obj.S['desc']);
      RTF.SelText:=#13#10;
      RTF.SelText:=#13#10;
    end;
    InsertHyperLink(RTF,Obj.S['name'],Obj.S['name']);
    RTF.SelAttributes.Style:=[];
    RTF.SelAttributes.Size:=16;
    RTF.SelText:=#13#10;
    RTF.SelText:=#13#10;
  end;
  if (Assigned(Obj.A['child'])=True) and
     (Assigned(Obj.A['child'].O[0])=True) then
  begin
    q:=0;
    while (Assigned(Obj.A['child'].O[q])=True) do
    begin
      JsonToRtf(Obj.A['child'].O[q],False,Rtf);
      q:=q+1;
    end;
  end;
end;

function NodeToJson(Node: TTreeNode): String;
var n: Integer;
begin
  if Assigned(Node)=True then
  begin
    Result:='';
    Result:=Result+'{';
    Result:=Result+'"name": "'+Node.Text+'",';
    Result:=Result+'"image": '+IntToStr(Node.ImageIndex)+',';
    Result:=Result+'"select": '+IntToStr(Node.SelectedIndex)+',';
    Result:=Result+'"state": '+IntToStr(Node.StateIndex)+',';
    if Assigned(Node.Data)=True then
    begin
      Result:=Result+'"desc": "'+TNotebookNode(Node.Data).Desc+'",';
    end
    else
    begin
     Result:=Result+'"desc": "",';
    end;
    if (Node.HasChildren=True) and (Node.Count>0) then
    begin
      Result:=Result+'"child": [';
      for n:=0 to Node.Count-1 do
      begin
        if n<Node.Count then Result:=Result+NodeToJson(Node.Item[n])+',' else Result:=Result+NodeToJson(Node.Item[n]);
      end;
      Result:=Result+']';
    end
    else
    begin
      Result:=Result+'"child": []';
    end;
    Result:=Result+'}';
  end
  else
  begin
    Result:='{}';
  end;
end;

function JsonToNode(Obj: ISuperObject; Parent: TTreeNode; List: TTreeView): TTreeNode;
var q: Integer;
    Node: TTreeNode;
begin
  if Assigned(Parent)=True then Node:=List.Items.AddChild(Parent,Obj.S['name']) else Node:=List.Items.Add(nil,Obj.S['name']);
  Node.ImageIndex:=Obj.I['image'];
  Node.SelectedIndex:=Obj.I['select'];
  Node.StateIndex:=Obj.I['state'];
  Node.Data:=TNotebookNode.Create;
  TNotebookNode(Node.Data).Desc:=Obj.S['desc'];
  TNotebookNode(Node.Data).Icon:=Obj.I['image'];
  if (Assigned(Obj.A['child'])=True) and
     (Assigned(Obj.A['child'].O[0])=True) then
  begin
    q:=0;
    while (Assigned(Obj.A['child'].O[q])=True) do
    begin
      JsonToNode(Obj.A['child'].O[q],Node,List);
      q:=q+1;
    end;
  end;
end;

function TreeToObj(List: TTreeView): ISuperObject;
var n: Integer;
    R: String;
begin
  R:='{';
  if (Assigned(List)=True) and (List.Items.Count>0) then
  begin
    R:=R+'"notebook": [';
    for n:=0 to List.Items.Count-1 do
    begin
      if Assigned(List.Items.Item[n].Parent)=False then
      begin
        if n<List.Items.Count then R:=R+NodeToJson(List.Items.Item[n])+',' else R:=R+NodeToJson(List.Items.Item[n]);
      end;
    end;
    R:=R+']';
  end
  else
  begin
    R:=R+'"notebook": []';
  end;
  R:=R+'}';
  Result:=SO(R);
end;

function ObjToTree(Obj: ISuperObject; List: TTreeView): Boolean;
var q: Integer;
begin
  Result:=False;
  if (Assigned(List)=True) and
     (Assigned(Obj)=True) and
     (Assigned(Obj.O['notebook'])=True) then
  begin
    List.Items.BeginUpdate;
    List.Items.Clear;
    q:=0;
    while Assigned(Obj.A['notebook'].O[q]) do
    begin
      JsonToNode(Obj.A['notebook'].O[q],nil,List);
      q:=q+1;
    end;
    List.Items.EndUpdate;
    Result:=True;
  end;
end;

function URLEncode(Value : String) : String;
const
  ValidURLChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$-_@.&+-!*"''(),;/#?:';
Var I : Integer;
Begin
   Result := '';
   For I := 1 To Length(Value) Do
      Begin
         If Pos(UpperCase(Value[I]), ValidURLChars) > 0 Then
            Result := Result + Value[I]
         Else
            Begin
               If Value[I] = ' ' Then
                  Result := Result + '+'
               Else
                  Begin
                     Result := Result + '%';
                     Result := Result + IntToHex(Byte(Value[I]), 2);
                  End;
            End;
      End;
End;

constructor TBrowserDataBase.Create;
var Req: String;
begin
  Reset;
  FIdWhois1:=TIdWhois.Create(nil);
  FMemo:=TStringList.Create;
  FValueListEditor:=TStringList.Create;
  ForceDirectories(GetDatabaseDir);
  FSqlite:=TSQLiteDatabase.Create(GetDatabaseDir+GetDatabaseFile);
  FSqlite.ExecSQL('PRAGMA synchronous=0');
  FSqlite.ExecSQL('PRAGMA encoding = "UTF-8"');
  FSqlite.ExecSQL('PRAGMA cache_size=3000');
  FSqlite.ExecSQL('PRAGMA temp_store=MEMORY');
  if FSqlite.TableExists('logger')=False then
  begin
    Req:='create table logger (';
    Req:=Req+'datetimes INTEGER,';
    Req:=Req+'types INTEGER,';
    Req:=Req+'informs TEXT)';
    FSqlite.ExecSQL(Req);
  end;
  if FSqlite.TableExists('inifile')=False then
  begin
    Req:='create table inifile (';
    Req:=Req+'parameter TEXT,';
    Req:=Req+'value TEXT)';
    FSqlite.ExecSQL(Req);
  end;
  if FileExists(GetDatabaseDir+'cache.json')=True then
  begin
    FMoogle:=TSuperObject.ParseFile(GetDatabaseDir+'cache.json',False);
  end
  else
  begin
    FMoogle:=SO('{"os":{}, "project":{}, "language":{}, "option":{}, "proxy":{}, "packet":[]}');
  end;
  FMoogle.S['os.guid']:=Core.Guid;
  FMoogle.S['os.system']:=Core.Family;
  FMoogle.S['os.capacity']:=Core.Capacity;
  FMoogle.S['os.path']:=Core.Path;
  FMoogle.S['os.name']:=Core.Name;
  FMoogle.S['os.build']:=Core.Build;
  FMoogle.S['os.login']:=Core.Login;
  FMoogle.B['os.admin']:=Core.Admin;
  FMoogle.S['project.guid']:=Project.Guid;
  FMoogle.S['project.system']:=Project.System;
  FMoogle.S['project.capacity']:=Project.Capacity;
  FMoogle.S['project.path']:=Project.Path;
  FMoogle.S['project.name']:=Project.Name;
  FMoogle.S['project.version']:=Project.Version;
  FMoogle.S['project.build']:=Project.Build;
  FMoogle.I['project.size']:=Project.Size;
  FMoogle.S['project.vendor']:=Project.Vendor;
  FMoogle.S['project.copyright']:=Project.Copyright;
  FMoogle.B['project.license']:=Project.Available;
  FMoogle.S['project.issue']:=Project.IssueDate;
  FMoogle.S['project.expiry']:=Project.ExpiryDate;
  ImportPacketList;
  FMoogle.SaveTo(GetDatabaseDir+'cache.json');
  Req:='Moogle start. User: '+Core.Login+';Admin: '+BoolToStr(Core.Admin,True);
  History(TYPE_START,Req);
end;

destructor TBrowserDataBase.Destroy;
begin
  History(TYPE_STOP,'Moogle stop');
  if Assigned(FSqlite) then
  begin
    FSqlite.Destroy;
    FSqlite:=nil;
  end;
  if Assigned(FMoogle) then
  begin
    FMoogle.SaveTo(GetDatabaseDir+'cache.json');
  end;
  if Assigned(FValueListEditor) then
  begin
    FValueListEditor.Destroy;
    FValueListEditor:=nil;
  end;
  if Assigned(FMemo) then
  begin
    FMemo.Destroy;
    FMemo:=nil;
  end;
  if Assigned(FIdWhois1) then
  begin
    FIdWhois1.Destroy;
    FIdWhois1:=nil;
  end;
  Reset;
  inherited Destroy;
end;

procedure TBrowserDataBase.Reset;
begin
  FSqlite:=nil;
  FIdWhois1:=nil;
  FMemo:=nil;
  FValueListEditor:=nil;
end;

function TBrowserDataBase.IsItIPV4(Str: String): Boolean;
var t: String;
    n: Integer;
begin
  Result:=True;
  T:='0123456789.';
  for n:=1 to Length(Str) do
  begin
    if Pos(Str[n],T)=0 then
    begin
      Result:=False;
      Break;
    end;
  end;
end;

function TBrowserDataBase.IsAvalible(ipAddressStr:AnsiString;dwPort: Word): Boolean;
var
  client : sockaddr_in;
  sock   : Integer;
  Block:  u_long;
  ret    : Integer;
  wsdata : WSAData;
  FD: FD_Set;
  tv: timeval;
  Res: Integer;
begin
 Result:=False;
 Block:=1;
 ret:=WSAStartup($0002, wsdata);
  if ret<>0 then exit;
  try
    client.sin_family:= AF_INET;
    client.sin_port:= htons(dwPort);
    client.sin_addr.s_addr:=inet_addr(PAnsiChar(ipAddressStr));
    sock:=socket(AF_INET, SOCK_STREAM,0);
    ioctlsocket(sock,FIONBIO,Block);
    Res:=connect(sock,TSockAddr(client),SizeOf(client));
    FD_ZERO(FD);
    _FD_SET(Sock,FD);
    tv.tv_sec:=60;
    tv.tv_usec:=0;
    res:=select(sock+1,nil,@FD,nil,@tv);
    if res>0 then Result:=True;



    Block:=0;
    ioctlsocket(sock,FIONBIO,Block);
    shutdown(sock,SD_BOTH);
    closesocket(sock);
  finally
    WSACleanup;
  end;
end;

function TBrowserDataBase.Whois(Whois: String; var Refer: String): Boolean;
var n: Integer;
    l: String;
    m: Integer;
    q: Integer;
    Domain: String;
    Response: String;
begin
  Result:=False;
  Refer:='';
  if (Whois<>'') and (pos('.',Whois)>0) then
  begin
    FMemo.Clear;
    FValueListEditor.Clear;
    Domain:=Trim(LowerCase(Whois));
    Domain:=StringReplace(Domain,'www.','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'http://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'https://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'ftp://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,' ','',[rfReplaceAll, rfIgnoreCase]);
    for n:=1 to Length(Domain) do
    begin
      if Domain[n]='/' then
      begin
        Domain:=Copy(Domain,1,n-1);
        Break;
      end;
    end;
    if IsItIPV4(Domain)=False then
    begin
      q:=0;
      for n:=1 to Length(Domain) do
      begin
        if Domain[n]='.' then q:=q+1;
      end;
      if q>1 then
      begin
        for n:=1 to Length(Domain) do
        begin
          if Domain[n]='.' then
          begin
            Delete(Domain,1,n);
            Break;
          end;
        end;
      end;
      q:=0;
      for n:=1 to Length(Domain) do
      begin
        if Domain[n]='.' then q:=q+1;
      end;
      if q>1 then
      begin
        for n:=1 to Length(Domain) do
        begin
          if Domain[n]='.' then
          begin
            Delete(Domain,1,n);
            Break;
          end;
        end;
      end;
    end;
    Response:=FIdWhois1.WhoIs(Domain);
    if Response<>'' then
    begin
      l:='';
      for n:=1 to Length(Response) do
      begin
        if (Response[n]<>#$0D) and (Response[n]<>#$0A) then
        begin
          l:=l+Response[n];
        end
        else
        begin
          l:=Trim(l);
          if l<>'' then
          begin
            if (l[1]='%') or (l[1]='#') or (pos(':',l)=0) then
            begin
              l:=StringReplace(l,'%','',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'#','',[rfReplaceAll, rfIgnoreCase]);
              l:=Trim(l);
              if l<>'' then FMemo.Add(l);
            end
            else
            begin
              l:=StringReplace(l,'Last updated on','Last updated on: ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'     ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'    ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'   ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'  ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,': ','=',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'= ','=',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'>>>','',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'<<<','',[rfReplaceAll, rfIgnoreCase]);
              l:=Trim(l);
              if l<>'' then
              begin
                if (pos('NOTICE=',l)>0) or (pos('TERMS OF USE=',l)>0) then
                begin
                  l:=StringReplace(l,'=',': ',[rfReplaceAll, rfIgnoreCase]);
                  FMemo.Add(l);
                end
                else
                begin
                  if (l[Length(l)]='=') then l:=l+'(None)';
                  if (l[Length(l)]=':') then
                  begin
                    l[Length(l)]:='=';
                    l:=l+'(None)';
                  end;
                  if pos('=',l)>0 then
                  begin
                    q:=0;
                    for m:=1 to Length(l) do
                    begin
                      if l[m]=' ' then q:=q+1;
                    end;
                    if q>7 then FMemo.Add(l) else FValueListEditor.Add(l);
                  end
                  else
                  begin
                    FMemo.Add(l);
                  end;
                end;
              end;
            end;
            l:='';
          end;
        end;
      end;
      l:=Trim(l);
      if l<>'' then
      begin
        if (l[1]='%') or (l[1]='#') or (pos(':',l)=0) then
        begin
          l:=StringReplace(l,'%','',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'#','',[rfReplaceAll, rfIgnoreCase]);
          l:=Trim(l);
          if l<>'' then FMemo.Add(l);
        end
        else
        begin
          l:=StringReplace(l,'Last updated on','Last updated on: ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'     ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'    ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'   ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'  ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,': ','=',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'= ','=',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'>>>','',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'<<<','',[rfReplaceAll, rfIgnoreCase]);
          l:=Trim(l);
          if l<>'' then
          begin
            if (pos('NOTICE=',l)>0) or (pos('TERMS OF USE=',l)>0) then
            begin
              l:=StringReplace(l,'=',': ',[rfReplaceAll, rfIgnoreCase]);
              FMemo.Add(l);
            end
            else
            begin
              if (l[Length(l)]='=') then l:=l+'(None)';
              if (l[Length(l)]=':') then
              begin
                l[Length(l)]:='=';
                l:=l+'(None)';
              end;
              if pos('=',l)>0 then
              begin
                q:=0;
                for m:=1 to Length(l) do
                begin
                  if l[m]=' ' then q:=q+1;
                end;
                if q>7 then FMemo.Add(l) else FValueListEditor.Add(l);
              end
              else
              begin
                FMemo.Add(l);
              end;
            end;
          end;
        end;
        l:='';
      end;
      if FValueListEditor.IndexOfName('refer')<>-1 then Refer:=FValueListEditor.Values['refer'];
      Result:=True;
    end;
  end;
end;

function TBrowserDataBase.GetInstallDir: String;
begin
  Result:=ExtractFilePath(ParamStr(0));
end;

function TBrowserDataBase.GetPacketDir: String;
begin
  Result:=GetInstallDir+'packets\';
end;

function TBrowserDataBase.GetDatabaseFile: String;
begin
  Result:='history.db';
end;

function TBrowserDataBase.GetDatabaseDir: String;
begin
  Result:='C:\ProgramData\TAO\';
end;

function TBrowserDataBase.CheckPacket(G: String): Boolean;
var n: Integer;
begin
  Result:=False;
  if G='{00000000-0000-0000-0000-000000000000}' then
  begin
    Result:=True;
  end
  else
  begin
    if (Assigned(FMoogle)=True) and (Assigned(FMoogle.O['packet'])=True) then
    begin
      n:=0;
      while Assigned(FMoogle.A['packet'].O[n])=True do
      begin
        if FMoogle.A['packet'].O[n].S['guid']=G then
        begin
          Result:=True;
          Break;
        end;
        n:=n+1;
      end;
    end;
  end;
end;

procedure TBrowserDataBase.ImportPacketObject(Pck: ISuperObject);
var Skeep: Boolean;
begin
  if Assigned(FMoogle)=True then
  begin
    if Assigned(Pck)=True then
    begin
      if (Assigned(Pck.O['header'])=True) and
         (Assigned(Pck.O['header.guid'])=True) and
         (Assigned(Pck.O['header.type'])=True) and
         (Assigned(Pck.O['header.file'])=True) and
         (Assigned(Pck.O['header.load'])=True) and
         (Assigned(Pck.O['header.dependence'])=True) and
         (Assigned(Pck.O['header.description'])=True) and
         (Assigned(Pck.O['header.version'])=True) and
         (Assigned(Pck.O['body'])=True) then
      begin
        if (Pck.S['header.load']='static') then Skeep:=CheckPacket(Pck.S['header.guid']) else Skeep:=False;
        if Skeep=False then
        begin
          if CheckPacket(Pck.S['header.dependence'])=True then
          begin
            //Language packet
            if (Pck.S['header.type']='{5E754884-8C7B-455B-A136-CDB926FF5C16}') then
            begin
              if (Assigned(Pck.O['body.language'])=True) then
              begin
                try
                  FMoogle.Merge(Pck.O['body']);
                  FMoogle.A['packet'].Add(Pck.O['header']);
                  History(TYPE_PACKET_SUCCESS,'Load '+Pck.S['header.file']);
                except
                  on E: Exception do
                    History(TYPE_PACKET_FAULT,E.Message);
                end;
               //FMoogle.SaveTo('C:\Language.txt');
              end
              else
              begin
                History(TYPE_PACKET_FAULT,'Invalid packet language format '+Pck.S['header.file']);
              end;
            end
            else
            begin
              //Option packet
              if (Pck.S['header.type']='{502FFDC5-DF55-47A3-A516-CD2A6F687270}') then
              begin
                if (Assigned(Pck.O['body.option'])=True) then
                begin
                  try
                    FMoogle.Merge(Pck.O['body']);
                    FMoogle.A['packet'].Add(Pck.O['header']);
                    History(TYPE_PACKET_SUCCESS,'Load '+Pck.S['header.file']);
                  except
                    on E: Exception do
                      History(TYPE_PACKET_FAULT,E.Message);
                  end;
                end
                else
                begin
                  History(TYPE_PACKET_FAULT,'Invalid packet option format '+Pck.S['header.file']);
                end;
              end
              else
              begin
                //Proxy packet
                if (Pck.S['header.type']='{655EB0F2-4951-4912-B5D4-AF4D7A5D6401}') then
                begin
                  if (Assigned(Pck.O['body.proxy'])=True) then
                  begin
                    try
                      FMoogle.Merge(Pck.O['body']);
                      FMoogle.A['packet'].Add(Pck.O['header']);
                      History(TYPE_PACKET_SUCCESS,'Load '+Pck.S['header.file']);
                    except
                      on E: Exception do
                        History(TYPE_PACKET_FAULT,E.Message);
                    end;
                  end
                  else
                  begin
                    History(TYPE_PACKET_FAULT,'Invalid packet proxy format '+Pck.S['header.file']);
                  end;
                end
                else
                begin
                  //Next types
                  History(TYPE_PACKET_FAULT,'Invalid packet type '+Pck.S['header.file']);
                end;
              end;
            end;
          end
          else
          begin
            History(TYPE_PACKET_FAULT,'Invalid packet dependence '+Pck.S['header.file']);
          end;
        end
        else
        begin
          History(TYPE_PACKET_SUCCESS,'Skip '+Pck.S['header.file']);
        end;
      end
      else
      begin
        History(TYPE_PACKET_FAULT,'Invalid packet format '+Pck.S['header.file']);
      end;
    end
    else
    begin
      History(TYPE_PACKET_FAULT,'Packet object not found');
    end;
  end
  else
  begin
    History(TYPE_PACKET_FAULT,'Main object not found');
  end;
end;

procedure TBrowserDataBase.ImportPacketFile(FileName: String);
var Pck: ISuperObject;
      P: ISuperObject;
begin
  if FileExists(FileName)=True then
  begin
    try
      Pck:=TSuperObject.ParseFile(FileName,False);
      Pck.S['header.file']:=ExtractFileName(FileName);
      ImportPacketObject(Pck);
    except
      on E: Exception do
      History(TYPE_PACKET_FAULT,E.Message);
    end;
  end
  else
  begin
    History(TYPE_PACKET_FAULT,'File not found - '+FileName);
  end;
end;

procedure TBrowserDataBase.ImportPacketList;
var F: TSearchRec;
begin
  if FindFirst(GetPacketDir+'*',faAnyFile-faHidden-faReadOnly-faDirectory,F)=0 then
  begin
   try
    repeat
      ImportPacketFile(GetPacketDir+F.Name);
    until FindNext(F)<>0
   finally
     FindClose(F);
   end;
  end
  else
  begin
    History(TYPE_PACKET_FAULT,'Packets not found');
  end;
end;

function TBrowserDataBase.History(Id: UInt64; Str: AnsiString): Boolean;
var Sql: AnsiString;
begin
  Result:=False;
  if Assigned(FSqlite)=True then
  begin
    if Str<>'' then
    begin
      Sql:='insert into logger (datetimes,types,informs) values (';
      Sql:=Sql+IntToStr(DateTimeToUnix(Now))+',';
      Sql:=Sql+IntToStr(Id)+',';
      Sql:=Sql+'"'+Trim(Str)+'");';
      try
        FSqlite.ExecSQL(Sql);
        Result:=True;
      except
        Result:=False;
      end;
    end;
  end;
end;

function TBrowserDataBase.IsDomain(Str: AnsiString): Boolean;
begin
  Result:=False;
  if (Pos('://',Str)>0) or
     (Pos('www.',Str)>0) or
     (Pos('.gov',Str)>0) or
     (Pos('.com',Str)>0) or
     (Pos('.edu',Str)>0) or
     (Pos('.org',Str)>0) or
     (Pos('.ru',Str)>0) then
   begin
     Result:=True;
   end;
end;

//
function TBrowserDataBase.IsDownload(Str: AnsiString): Boolean;
var n: Integer;
    m: Integer;
    t: AnsiString;
    l: Integer;
    f: String;
begin
  Result:=False;
  if (Str<>'') and (Length(Str)>8) then
  begin
    l:=pos('?',Str);
    if l>0 then Str:=Copy(Str,1,l-1);
    f:='';
    m:=0;
    for n:=1 to Length(Str) do
    begin
      if Str[n]='/' then m:=m+1;
    end;
    if (Pos('=',Str)=0) and
       (Pos('File:',Str)=0) and
       (Pos('file:',Str)=0) and
       (m>=3) and
       (Str[Length(Str)]<>'/') and
       (Str[Length(Str)]<>'\') and
       ((Pos(':/',Str)>0) or (Pos(':\',Str)>0)) then
    begin
       if (Str[Length(Str)-1]='.') or
          (Str[Length(Str)-2]='.') or
          (Str[Length(Str)-3]='.') or
          (Str[Length(Str)-4]='.') or
          (Str[Length(Str)-5]='.') or
          (Str[Length(Str)-6]='.') or
          (Str[Length(Str)-7]='.') then
       begin
         t:=Copy(Str,Length(Str)-7,8);
         if (Pos('.aspx',t)=0) and
            (Pos('.htm',t)=0) and
            (Pos('.xhtm',t)=0) and
            (Pos('.xhtml',t)=0) and
            (Pos('.php',t)=0) and
            (Pos('.html',t)=0) then
         begin
           for n:=Length(Str) downto 1 do
           begin
             if (Str[n]<>'/') and (Str[n]<>'\') then
             begin
               f:=Str[n]+f;
             end
             else
             begin
               Break;
             end;
           end;
           if Length(f)>3 then Result:=True else Result:=False;
         end;
       end;
    end;
  end;
end;

function TBrowserDataBase.GetDownloadFile(Str: String): String;
var n: Integer;
    f: String;
    l: Integer;
begin
  if (Assigned(FMoogle)=True) and (FMoogle.S['option.storage.download.value']<>'') then
  begin
    Result:=FMoogle.S['option.storage.download.value'];
  end
  else
  begin
    Result:='C:\ProgramData\Moogle\Downloads\';
  end;
  if (Str<>'') and (Length(Str)>8) then
  begin
    l:=pos('?',Str);
    if l>0 then Str:=Copy(Str,1,l-1);
    f:='';
    for n:=Length(Str) downto 1 do
    begin
      if (Str[n]<>'/') and (Str[n]<>'\') then
      begin
        f:=Str[n]+f;
      end
      else
      begin
        Break;
      end;
    end;
    Result:=TIdURI.URLDecode(Result+f);
  end
  else
  begin
    Result:=Result+IntToHex(Random(65535),4)+'.dat';
  end;
end;

function TBrowserDataBase.Clear: Boolean;
begin
  Result:=False;
  if Assigned(FSqlite) then
  begin
    try
      FSqlite.ExecSQL('delete from logger');
      Result:=True;
    except
      Result:=False;
    end;
  end;
end;

function TBrowserDataBase.DNWhois(Str: String; var Html: String): Boolean;
var R: String;
    n: Integer;
begin
  Result:=False;
  if LowerCase(Parser(Str,0,''))='whois' then
  begin
  FIdWhois1.Host:='whois.iana.org';
    if Whois(Parser(Str,1,''),R)=True then
    begin
      if R<>'' then
      begin
        FIdWhois1.Host:=R;
        if Whois(Parser(Str,1,''),R)=True then
        begin
          Result:=True;
        end;
      end
      else
      begin
        Result:=True;
      end;
    end;
    Html:='<!DOCTYPE html><html><head><style>';
    Html:=Html+'::-webkit-scrollbar {';
    Html:=Html+'width: 5px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'box-shadow: inset 0 0 5px grey;';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-track {';
    Html:=Html+'background: #f1f1f1;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-thumb {';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'border-radius: 10px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'@font-face {';
    Html:=Html+'font-family: Arial, Helvetica, sans-serif;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'word-spacing: 2px;';
    Html:=Html+'color: #000000;';
    Html:=Html+'font-weight: 400;';
    Html:=Html+'text-decoration: none;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-variant: small-caps;';
    Html:=Html+'text-transform: none;';
    Html:=Html+'text-shadow: 4px 4px 4px #000;';
    Html:=Html+'}';
    Html:=Html+'.form-style-binary {';
    Html:=Html+'position: fixed;';
    Html:=Html+'margin-left: 73%;';
    Html:=Html+'margin-top: 0%;';
    Html:=Html+'overflow:auto;';
    Html:=Html+'color: white;';
    Html:=Html+'border: 1px solid rgba(50, 50, 50, 0.5);';
    Html:=Html+'background-color: rgba(0,0,0,0.5);';
    Html:=Html+'width: 25%;';
    Html:=Html+'height: 97%;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'box-shadow: 0 0 5px rgba(255,255,255,.9), 0 0 1px rgba(255,255,255,.9);';
    Html:=Html+'font-size: 17px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success-header {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(164, 164, 164, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'text-shadow: 2px 1px 2px #555;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px solid black;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(192, 192, 192, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px dotted black;';
    Html:=Html+'border-radius: 0px 0px 0px 0px;';
    Html:=Html+'}';
    Html:=Html+'br {';
    Html:=Html+'display: block;';
    Html:=Html+'margin-bottom: 2px;';
    Html:=Html+'font-size:2px;';
    Html:=Html+'line-height: 8px;';
    Html:=Html+'}';
    Html:=Html+'</style><script>';
    //Html:=Html+'';
    Html:=Html+'</script></head><title></title><body bgcolor="-1">';
    Html:=Html+'<form class="form-style-binary" style="margin-left: 0%; margin-top: 0%; width: 98.5%; height: 97%; padding: 5px;" id="ListBox1">';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">DESCRIPTION</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    for n:=0 to FMemo.Count-1 do
    begin
      Html:=Html+FMemo.Strings[n]+'<br>';
    end;
    Html:=Html+'</footer><br>';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">PARAMETERS</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    for n:=0 to FValueListEditor.Count-1 do
    begin
      Html:=Html+FValueListEditor.Names[n]+': '+FValueListEditor.ValueFromIndex[n]+'<br>';
    end;
    Html:=Html+'</footer>';
    Html:=Html+'</form>';
    Html:=Html+'</body></html>';
    Result:=True;
  end;
end;

function TBrowserDataBase.DNMyip(Str: String; var Html: String): Boolean;
var Obj: ISuperObject;
begin
  Result:=False;
  if LowerCase(Parser(Str,0,''))='myip' then
  begin
    Obj:=RestApiGet('https://api.myip.com');
    Html:='<!DOCTYPE html><html><head><style>';
    Html:=Html+'::-webkit-scrollbar {';
    Html:=Html+'width: 5px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'box-shadow: inset 0 0 5px grey;';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-track {';
    Html:=Html+'background: #f1f1f1;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-thumb {';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'border-radius: 10px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'@font-face {';
    Html:=Html+'font-family: Arial, Helvetica, sans-serif;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'word-spacing: 2px;';
    Html:=Html+'color: #000000;';
    Html:=Html+'font-weight: 400;';
    Html:=Html+'text-decoration: none;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-variant: small-caps;';
    Html:=Html+'text-transform: none;';
    Html:=Html+'text-shadow: 4px 4px 4px #000;';
    Html:=Html+'}';
    Html:=Html+'.form-style-binary {';
    Html:=Html+'position: fixed;';
    Html:=Html+'margin-left: 73%;';
    Html:=Html+'margin-top: 0%;';
    Html:=Html+'overflow:auto;';
    Html:=Html+'color: white;';
    Html:=Html+'border: 1px solid rgba(50, 50, 50, 0.5);';
    Html:=Html+'background-color: rgba(0,0,0,0.5);';
    Html:=Html+'width: 25%;';
    Html:=Html+'height: 97%;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'box-shadow: 0 0 5px rgba(255,255,255,.9), 0 0 1px rgba(255,255,255,.9);';
    Html:=Html+'font-size: 17px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success-header {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(164, 164, 164, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'text-shadow: 2px 1px 2px #555;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px solid black;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(192, 192, 192, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px dotted black;';
    Html:=Html+'border-radius: 0px 0px 0px 0px;';
    Html:=Html+'}';
    Html:=Html+'br {';
    Html:=Html+'display: block;';
    Html:=Html+'margin-bottom: 2px;';
    Html:=Html+'font-size:2px;';
    Html:=Html+'line-height: 8px;';
    Html:=Html+'}';
    Html:=Html+'</style><script>';
    //Html:=Html+'';
    Html:=Html+'</script></head><title></title><body bgcolor="-1">';
    Html:=Html+'<form class="form-style-binary" style="margin-left: 0%; margin-top: 0%; width: 98.5%; height: 97%; padding: 5px;" id="ListBox1">';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">INFORMATION</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'My ip service'+'<br>';
    Html:=Html+'</footer><br>';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">PARAMETERS</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'IP: '+Obj.S['ip']+'<br>';
    Html:=Html+'Country: '+Obj.S['country']+'<br>';
    Html:=Html+'</footer>';
    Html:=Html+'</form>';
    Html:=Html+'</body></html>';
    Result:=True;
  end;
end;

function TBrowserDataBase.DNVpn(Str: String; var Html: String): Boolean;
var Obj: ISuperObject;
    S: String;
begin
  Result:=False;
  if LowerCase(Parser(Str,0,''))='vpn' then
  begin
    S:='--command connect '+Parser(Str,1,'')+'.ovpn';
    ShellExecute(0, nil, 'openvpn-gui.exe', PWChar(S), 'C:\Program Files\OpenVPN\bin\', SW_SHOW);
    Sleep(12000);
    Obj:=RestApiGet('https://api.myip.com');
    Html:='<!DOCTYPE html><html><head><style>';
    Html:=Html+'::-webkit-scrollbar {';
    Html:=Html+'width: 5px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'box-shadow: inset 0 0 5px grey;';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-track {';
    Html:=Html+'background: #f1f1f1;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-thumb {';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'border-radius: 10px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'@font-face {';
    Html:=Html+'font-family: Arial, Helvetica, sans-serif;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'word-spacing: 2px;';
    Html:=Html+'color: #000000;';
    Html:=Html+'font-weight: 400;';
    Html:=Html+'text-decoration: none;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-variant: small-caps;';
    Html:=Html+'text-transform: none;';
    Html:=Html+'text-shadow: 4px 4px 4px #000;';
    Html:=Html+'}';
    Html:=Html+'.form-style-binary {';
    Html:=Html+'position: fixed;';
    Html:=Html+'margin-left: 73%;';
    Html:=Html+'margin-top: 0%;';
    Html:=Html+'overflow:auto;';
    Html:=Html+'color: white;';
    Html:=Html+'border: 1px solid rgba(50, 50, 50, 0.5);';
    Html:=Html+'background-color: rgba(0,0,0,0.5);';
    Html:=Html+'width: 25%;';
    Html:=Html+'height: 97%;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'box-shadow: 0 0 5px rgba(255,255,255,.9), 0 0 1px rgba(255,255,255,.9);';
    Html:=Html+'font-size: 17px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success-header {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(164, 164, 164, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'text-shadow: 2px 1px 2px #555;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px solid black;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(192, 192, 192, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px dotted black;';
    Html:=Html+'border-radius: 0px 0px 0px 0px;';
    Html:=Html+'}';
    Html:=Html+'br {';
    Html:=Html+'display: block;';
    Html:=Html+'margin-bottom: 2px;';
    Html:=Html+'font-size:2px;';
    Html:=Html+'line-height: 8px;';
    Html:=Html+'}';
    Html:=Html+'</style><script>';
    //Html:=Html+'';
    Html:=Html+'</script></head><title></title><body bgcolor="-1">';
    Html:=Html+'<form class="form-style-binary" style="margin-left: 0%; margin-top: 0%; width: 98.5%; height: 97%; padding: 5px;" id="ListBox1">';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">INFORMATION</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'Vpn service'+'<br>';
    Html:=Html+'</footer><br>';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">PARAMETERS</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'Your IP now : '+Obj.S['ip']+'<br>';
    Html:=Html+'Your Country now: '+Obj.S['country']+'<br>';
    Html:=Html+'</footer>';
    Html:=Html+'</form>';
    Html:=Html+'</body></html>';
    Result:=True;
  end;
end;


function TBrowserDataBase.ReadString(Par: String; Def: string): string;
var Select: TSQLiteTable;
begin
  Result:=Def;
  if Par<>'' then
  begin
    Select:=FSqlite.GetTable('select * from inifile where parameter="'+LowerCase(Trim(Par))+'"');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then Result:=HTTPApp.HTTPDecode(Select.FieldAsString(1));
      Select.Destroy;
      Select:=nil;
    end;
  end;
end;

function TBrowserDataBase.WriteString(Par: String; Val: string): Boolean;
var Select: TSQLiteTable;
    Sql: AnsiString;
begin
  Result:=False;
  if Par<>'' then
  begin
    Select:=FSqlite.GetTable('select * from inifile where parameter="'+LowerCase(Par)+'"');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Sql:='update inifile set value="'+HTTPApp.HTTPEncode(Trim(Val))+'" where parameter="'+LowerCase(Trim(Par))+'"';
        try
          FSqlite.ExecSQL(Sql);
          Result:=True;
        except
          Result:=False;
        end;
      end
      else
      begin
        Sql:='insert into inifile (parameter,value) values (';
        Sql:=Sql+'"'+LowerCase(Trim(Par))+'",';
        Sql:=Sql+'"'+HTTPApp.HTTPEncode(Trim(Val))+'");';
        try
          FSqlite.ExecSQL(Sql);
          Result:=True;
        except
          Result:=False;
        end;
      end;
      Select.Destroy;
      Select:=nil;
    end;
  end;
end;

function TBrowserDataBase.ReadJson(Par: String; Def: ISuperObject): ISuperObject;
var Select: TSQLiteTable;
    Json: String;
begin
  if Assigned(Def) then Json:=ReadString(Par,Def.AsJSon) else Json:=ReadString(Par,'{}');
  Result:=SO(Json);
end;

function TBrowserDataBase.WriteJson(Par: String; Val: ISuperObject): Boolean;
begin
  if Assigned(Val) then Result:=WriteString(Par,Val.AsJSon) else Result:=WriteString(Par,'{}');
end;

function TBrowserDataBase.ClearNotebook(NoteBookView1: TTreeView): Boolean;
begin
  NoteBookView1.Items.Clear;
  Result:=WriteJson('notebook.json',SO);
end;

function TBrowserDataBase.LoadNotebook(NoteBookView1: TTreeView): Boolean;
begin
  Result:=ObjToTree(ReadJson('notebook.json',nil),NoteBookView1);
end;

function TBrowserDataBase.SaveNotebook(NoteBookView1: TTreeView): Boolean;
begin
  Result:=WriteJson('notebook.json',TreeToObj(NoteBookView1));
end;

function TBrowserDataBase.LoadSearch(Strings: TStrings; V: Boolean): Boolean;
var Obj: ISuperObject;
    Tmp: ISuperObject;
begin
  Result:=False;
  if Assigned(Strings)=True then
  begin
    Obj:=SO('{"search": []}');
    Tmp:=SO('{}');
    Tmp.S['name']:='VASYA';
    Tmp.S['link']:='http://vasya';
    Tmp.B['visb']:=True;
    Obj.A['search'].Add(Tmp);
    Obj:=ReadJson('search.json',Obj);

  end;
end;

function TBrowserDataBase.ExportNotebook(FileName: String): Boolean;
begin
  if FileName<>'' then
  begin
    try
      ReadJson('notebook.json',nil).SaveTo(FileName);
      Result:=True;
    except
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TBrowserDataBase.ImportNotebook(FileName: String): Boolean;
begin
  if FileExists(FileName)=True then
  begin
    try
      Result:=WriteJson('notebook.json',TSuperObject.ParseFile(FileName,False));
    except
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

procedure TBrowserDataBase.QuickUpdate;
begin
  if Assigned(FMoogle) then
  begin
    FMoogle.SaveTo(GetDatabaseDir+'cache.json');
  end;
end;

function TBrowserDataBase.RtfdNotebook(Rtf: TRichEdit): Boolean;
var q: Integer;
    Obj: ISuperObject;
begin
  Result:=False;
  Obj:=ReadJson('notebook.json',nil);
  if (Assigned(Rtf)=True) and
     (Assigned(Obj)=True) and
     (Assigned(Obj.O['notebook'])=True) then
  begin
    Rtf.Text:='';
    q:=0;
    while Assigned(Obj.A['notebook'].O[q]) do
    begin
      JsonToRtf(Obj.A['notebook'].O[q],True,Rtf);
      q:=q+1;
    end;
    Result:=True;
  end;
end;

function TBrowserDataBase.DNProxy(Str: String; var Html: String): Boolean;
begin
  Result:=False;
  if (LowerCase(Parser(Str,0,''))='proxyhttp') or
     (LowerCase(Parser(Str,0,''))='proxysock5') or
     (LowerCase(Parser(Str,0,''))='proxysock4') or
     (LowerCase(Parser(Str,0,''))='proxyhttps') then
  begin
    Html:='<!DOCTYPE html><html><head><style>';
    Html:=Html+'::-webkit-scrollbar {';
    Html:=Html+'width: 5px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'box-shadow: inset 0 0 5px grey;';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-track {';
    Html:=Html+'background: #f1f1f1;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-thumb {';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'border-radius: 10px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'@font-face {';
    Html:=Html+'font-family: Arial, Helvetica, sans-serif;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'word-spacing: 2px;';
    Html:=Html+'color: #000000;';
    Html:=Html+'font-weight: 400;';
    Html:=Html+'text-decoration: none;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-variant: small-caps;';
    Html:=Html+'text-transform: none;';
    Html:=Html+'text-shadow: 4px 4px 4px #000;';
    Html:=Html+'}';
    Html:=Html+'.form-style-binary {';
    Html:=Html+'position: fixed;';
    Html:=Html+'margin-left: 73%;';
    Html:=Html+'margin-top: 0%;';
    Html:=Html+'overflow:auto;';
    Html:=Html+'color: white;';
    Html:=Html+'border: 1px solid rgba(50, 50, 50, 0.5);';
    Html:=Html+'background-color: rgba(0,0,0,0.5);';
    Html:=Html+'width: 25%;';
    Html:=Html+'height: 97%;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'box-shadow: 0 0 5px rgba(255,255,255,.9), 0 0 1px rgba(255,255,255,.9);';
    Html:=Html+'font-size: 17px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success-header {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(164, 164, 164, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'text-shadow: 2px 1px 2px #555;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px solid black;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(192, 192, 192, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px dotted black;';
    Html:=Html+'border-radius: 0px 0px 0px 0px;';
    Html:=Html+'}';
    Html:=Html+'br {';
    Html:=Html+'display: block;';
    Html:=Html+'margin-bottom: 2px;';
    Html:=Html+'font-size:2px;';
    Html:=Html+'line-height: 8px;';
    Html:=Html+'}';
    Html:=Html+'</style><script>';
    //Html:=Html+'';
    Html:=Html+'</script></head><title></title><body bgcolor="-1">';
    Html:=Html+'<form class="form-style-binary" style="margin-left: 0%; margin-top: 0%; width: 98.5%; height: 97%; padding: 5px;" id="ListBox1">';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">PROXY</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'Server: '+Parser(Str,1,'127.0.0.1')+'<br>';
    Html:=Html+'Port: '+Parser(Str,2,'8080')+'<br>';
    Html:=Html+'Availble: '+BoolToStr(IsAvalible(Parser(Str,1,'127.0.0.1'),StrToInt(Parser(Str,2,'8080'))),True)+'<br>';
    Html:=Html+'</footer>';
    Html:=Html+'</form>';
    Html:=Html+'</body></html>';
    Result:=True;
  end;
end;

function TBrowserDataBase.Dictionary(Id: Word): String;
begin
  if Assigned(FMoogle.O['language.'+FMoogle.S['option.language.name.value']+'.dictionary.'+IntToHex(Id,2)])=True then
  begin
    Result:=FMoogle.S['language.'+FMoogle.S['option.language.name.value']+'.dictionary.'+IntToHex(Id,2)];
  end
  else
  begin
    Result:='UNKNOWN';
  end;
end;

function TBrowserDataBase.ProxyPage(Addr,Port,Protocol,Desc: String; var Html: String): Boolean;
begin
  Result:=False;
  if (Addr<>'') and
     (Port<>'') and
     (Protocol<>'') and
     (Desc<>'') then
  begin
    Html:='<!DOCTYPE html><html><head><style>';
    Html:=Html+'::-webkit-scrollbar {';
    Html:=Html+'width: 5px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'box-shadow: inset 0 0 5px grey;';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-track {';
    Html:=Html+'background: #f1f1f1;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'::-webkit-scrollbar-thumb {';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'border-radius: 10px;';
    Html:=Html+'background-color: hsla(12, 40%, 100%, 0.3);';
    Html:=Html+'}';
    Html:=Html+'@font-face {';
    Html:=Html+'font-family: Arial, Helvetica, sans-serif;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'word-spacing: 2px;';
    Html:=Html+'color: #000000;';
    Html:=Html+'font-weight: 400;';
    Html:=Html+'text-decoration: none;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-variant: small-caps;';
    Html:=Html+'text-transform: none;';
    Html:=Html+'text-shadow: 4px 4px 4px #000;';
    Html:=Html+'}';
    Html:=Html+'.form-style-binary {';
    Html:=Html+'position: fixed;';
    Html:=Html+'margin-left: 73%;';
    Html:=Html+'margin-top: 0%;';
    Html:=Html+'overflow:auto;';
    Html:=Html+'color: white;';
    Html:=Html+'border: 1px solid rgba(50, 50, 50, 0.5);';
    Html:=Html+'background-color: rgba(0,0,0,0.5);';
    Html:=Html+'width: 25%;';
    Html:=Html+'height: 97%;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'box-shadow: 0 0 5px rgba(255,255,255,.9), 0 0 1px rgba(255,255,255,.9);';
    Html:=Html+'font-size: 17px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success-header {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(164, 164, 164, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'text-shadow: 2px 1px 2px #555;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px solid black;';
    Html:=Html+'border-radius: 0.0em;';
    Html:=Html+'}';
    Html:=Html+'.form-param-caption-success {';
    Html:=Html+'width: 99.5%;';
    Html:=Html+'background-color: rgba(192, 192, 192, 0.8);';
    Html:=Html+'padding: 5px;';
    Html:=Html+'text-align: left;';
    Html:=Html+'color: black;';
    Html:=Html+'font-size: 12px;';
    Html:=Html+'font-family: Tahoma;';
    Html:=Html+'font-style: normal;';
    Html:=Html+'font-weight: normal;';
    Html:=Html+'box-shadow: 0 0em 1.0em rgba(192, 192, 192, 1);';
    Html:=Html+'letter-spacing: 1px;';
    Html:=Html+'border-top: 0px dotted black;';
    Html:=Html+'border-right: 0px dotted black;';
    Html:=Html+'border-left: 0px dotted black;';
    Html:=Html+'border-bottom: 0px dotted black;';
    Html:=Html+'border-radius: 0px 0px 0px 0px;';
    Html:=Html+'}';
    Html:=Html+'br {';
    Html:=Html+'display: block;';
    Html:=Html+'margin-bottom: 2px;';
    Html:=Html+'font-size:2px;';
    Html:=Html+'line-height: 8px;';
    Html:=Html+'}';
    Html:=Html+'</style><script>';
    //Html:=Html+'';
    Html:=Html+'</script></head><title></title><body bgcolor="-1">';
    Html:=Html+'<form class="form-style-binary" style="margin-left: 0%; margin-top: 0%; width: 98.5%; height: 97%; padding: 5px;" id="ListBox1">';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">SECURITY</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'Transparent - target server knows your IP address and it knows that you are connecting via a proxy server.'+'<br>';
    Html:=Html+'Anonymous - target server does not know your IP address, but it knows that you''re using a proxy.'+'<br>';
    Html:=Html+'Elite - target server does not know your IP address, or that the request is relayed through a proxy server.'+'<br>';
    Html:=Html+'</footer><br>';
    Html:=Html+'<footer class="form-param-caption-success-header" style="font-weight: bold;">PARAMETERS</footer>';
    Html:=Html+'<footer class="form-param-caption-success">';
    Html:=Html+'Server: '+Addr+'<br>';
    Html:=Html+'Port: '+Port+'<br>';
    Html:=Html+'Protocol: '+Protocol+'<br>';
    Html:=Html+'Security: '+Desc+'<br>';
    Html:=Html+'</footer>';
    Html:=Html+'</form>';
    Html:=Html+'</body></html>';
    Result:=True;
  end;
end;

function TBrowserDataBase.MyIP(var IP: String; var Country: String): Boolean;
var Obj: ISuperObject;
begin
  Obj:=RestApiGet('https://api.myip.com');
  if Assigned(Obj)=True then
  begin
    IP:=Obj.S['ip'];
    Country:=Obj.S['country'];
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

function TBrowserDataBase.TinyUrl(Url: String): String;
var Obj: ISuperObject;
begin
  if (Url<>'') and (Pos('tinyurl',Url)=0) then
  begin
    Obj:=TinyUrlGet('https://tinyurl.com/api-create.php?url='+Url);
    if (Assigned(Obj)=True) and (Obj.I['code']=200) then
    begin
      Result:=Obj.S['url'];
    end
    else
    begin
      Result:=Trim(Url);
    end;
  end
  else
  begin
    Result:=Trim(Url);
  end;
end;

function TBrowserDataBase.DarkNet(Str: String; var Html: String): Boolean;
var Srv: String;
begin
  Result:=False;
  if Str<>'' then
  begin
    if DNWhois(Str,Html)=True then
    begin
      Result:=True;
    end
    else
    begin
      if DNProxy(Str,Html)=True then
      begin
        Result:=True;
      end
      else
      begin
        if DNMyip(Str,Html)=True then
        begin
          Result:=True;
        end
        else
        begin
          if DNVpn(Str,Html)=True then
          begin
            Result:=True;
          end
          else
          begin
            Html:='Invalid protocol';
          end;
        end;
      end;
    end;
  end
  else
  begin
    Html:='Invalid page';
  end;
end;

function TBrowserDataBase.Parser(Link: String; I: Cardinal; Def: String): String;
var
  n: Integer;
  m: Integer;
begin
  Result:='';
  m:=-1;
  if Link<>'' then
  begin
    for n:=1 to Length(Link) do
    begin
      if Link[n]<>' ' then
      begin
        if (Link[n]<>':') and (Link[n]<>'/') then
        begin
          Result:=Result+Link[n];
        end
        else
        begin
          if Result<>'' then
          begin
            m:=m+1;
            if m=i then Exit else Result:='';
          end;
        end;
      end;
    end;
    if Result<>'' then
    begin
      m:=m+1;
      if m<>i then Result:='';
    end;
  end;
  if Result='' then Result:=Trim(Def);
end;

procedure TBrowserDataBase.GetListOptions(List: TListView);
var F: TSuperObjectIter;
    Q: TSuperObjectIter;
    I: TListItem;
begin
  if (Assigned(List)=True) and
     (Assigned(FMoogle)=True) and
     (Assigned(FMoogle.O['option'])=True) then
  begin
    List.Items.Clear;
    if (ObjectFindFirst(FMoogle.O['option'],F)=True) and (Assigned(F.Ite)=True) then
    begin
      F.Ite.First;
      repeat
         case F.Ite.Current.Value.DataType of
           stObject:
            begin
              I:=List.Items.Add;
              I.Caption:='';
              I.ImageIndex:=-1;
              I.SubItems.Add('');
              I.SubItems.Add('');
              I.SubItems.Add('');
              I:=List.Items.Add;
              I.Caption:=F.Ite.Current.Value.S['desc'];
              I.ImageIndex:=6;
              I.SubItems.Add('');
              I.SubItems.Add('');
              I.SubItems.Add('');
              if (ObjectFindFirst(F.Ite.Current.Value,Q)=True) and (Assigned(Q.Ite)=True) then
              begin
                Q.Ite.First;
                repeat
                  case Q.Ite.Current.Value.DataType of
                    stObject:
                    begin
                      I:=List.Items.Add;
                      if Q.Ite.Current.Value.S['desc']<>'' then
                      begin
                        I.Caption:=Q.Ite.Current.Value.S['desc'];
                      end
                      else
                      begin
                        I.Caption:='(None)';
                      end;
                      I.ImageIndex:=15;
                      I.SubItems.Add(Trim(Q.Ite.Current.Value.S['value']));
                      I.SubItems.Add('option.'+F.Ite.Current.Name+'.'+Q.Ite.Current.Name+'.value');
                      case Q.Ite.Current.Value.DataType of
                        stInt: I.SubItems.Add('INTEGER');
                        stBoolean: I.SubItems.Add('BOOL');
                      else
                        begin
                          if (UpperCase(Trim(Q.Ite.Current.Value.S['value']))='TRUE') or
                             (UpperCase(Trim(Q.Ite.Current.Value.S['value']))='FALSE') then
                          begin
                            I.SubItems.Add('BOOL');
                          end
                          else
                          begin
                            I.SubItems.Add('STRING');
                          end;
                        end;
                      end;
                    end;
                  end;
                until Q.Ite.MoveNext = False;
              end;
            end;
         end;
      until F.Ite.MoveNext = False;
    end;
  end;
end;

procedure TBrowserDataBase.GetListProxy(List: TTreeView);
var Node: TTreeNode;
    Countries: array of TTreeNode;
    n: Integer;
    m: Integer;
    F: TSuperObjectIter;
    Obj: ISuperObject;
begin
  Obj:=VpnDefaultList(Core.Login);


  Countries:=nil;
  if(Assigned(List)=True) and
    (Assigned(FMoogle)=True) and
    (Assigned(FMoogle.O['language'])=True) then
  begin
    List.Items.Clear;
    n:=0;
    if Assigned(FMoogle.O['language.english'])=true then
    begin
      while Assigned(FMoogle.A['language.english.country'].O[n])=true do
      begin
        n:=n+1;
      end;
    end;
    if n>0 then
    begin
      SetLength(Countries,n);
      n:=0;
      if Assigned(FMoogle.O['language.english'])=true then
      begin
        while Assigned(FMoogle.A['language.english.country'].O[n])=true do
        begin
          Node:=List.Items.AddChild(nil,FMoogle.A['language.english.country'].O[n].S['name']+' ('+FMoogle.A['language.english.country'].O[n].S['alpha-3']+')');
          Node.ImageIndex:=6;
          Node.SelectedIndex:=6;
          Node.StateIndex:=-1;
          Node.Data:=Pointer(StrToInt(FMoogle.A['language.english.country'].O[n].S['country-code']));
          Countries[n]:=Node;
          n:=n+1;
        end;
      end;
    end;
  end;


  if(Assigned(List)=True) and
    (Assigned(FMoogle)=True) and
    (Assigned(Countries)=True) and
    (Assigned(Obj)=True) and
    (Assigned(Obj.O['list'])=True) then
  begin
    if Obj.A['list'].Length>0 then
    begin
      for n:=0 to Obj.A['list'].Length-1 do
      begin

        if Obj.A['list'].O[n].S['type']='vpn' then
        begin
          for m:=Low(Countries) to High(Countries) do
          begin
            if Integer(Countries[m].Data)=Obj.A['list'].O[n].I['country'] then
            begin
              Countries[m].Expand(False);
              Node:=List.Items.AddChild(Countries[m],Obj.A['list'].O[n].S['url']);
              Node.ImageIndex:=26;
              Node.SelectedIndex:=26;
            end;
          end;
        end;


      end;
    end;
  end;



  if(Assigned(List)=True) and
    (Assigned(FMoogle)=True) and
    (Assigned(FMoogle.O['proxy'])=True) and
    (Assigned(Countries)=True)then
  begin
    if (ObjectFindFirst(FMoogle.O['proxy'],F)=True) and
       (Assigned(F.Ite)=True) then
    begin
      F.Ite.First;
      repeat
        for m:=Low(Countries) to High(Countries) do
        begin
          if F.Ite.Current.Value.I['country']=Integer(Countries[m].Data) then
          begin
            Countries[m].Expand(False);
            Node:=List.Items.AddChild(Countries[m],F.Ite.Current.Value.S['url']);
            if F.Ite.Current.Value.S['type']='web' then
            begin
              Node.ImageIndex:=9;
              Node.SelectedIndex:=9;
            end
            else
            begin
              if F.Ite.Current.Value.S['type']='transparent' then
              begin
                Node.ImageIndex:=20;
                Node.SelectedIndex:=20;
              end
              else
              begin
                if F.Ite.Current.Value.S['type']='anonymous' then
                begin
                  Node.ImageIndex:=21;
                  Node.SelectedIndex:=21;
                end
                else
                begin
                  if F.Ite.Current.Value.S['type']='elite' then
                  begin
                    Node.ImageIndex:=22;
                    Node.SelectedIndex:=22;
                  end
                  else
                  begin
                    if F.Ite.Current.Value.S['type']='vpn' then
                    begin
                      Node.ImageIndex:=26;
                      Node.SelectedIndex:=26;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
        Node.StateIndex:=-1;
      until F.Ite.MoveNext = False;
    end;
    List.Items.EndUpdate;
  end;



end;

procedure TBrowserDataBase.GetListAnonymous(List: TTreeView);
var n: Integer;
    Node: TTreeNode;
begin
  if(Assigned(List)=True) and
    (Assigned(FMoogle)=True) and
    (Assigned(FMoogle.O['language'])=True) then
  begin
    n:=0;
    List.Items.Clear;
    if Assigned(FMoogle.O['language.english'])=true then
    begin
      while Assigned(FMoogle.A['language.english.country'].O[n])=true do
      begin
        Node:=List.Items.AddChild(nil,FMoogle.A['language.english.country'].O[n].S['name']);
        Node.ImageIndex:=6;
        Node.SelectedIndex:=6;
        Node.StateIndex:=-1;
        Node.Data:=Pointer(840);
        n:=n+1;
      end;
    end;
  end;
end;

//List History
function TBrowserDataBase.GetListHistory(List: TListView; Search: String): Boolean;
var Select: TSQLiteTable;
    n: Integer;
    Item: TListItem;
begin
  Result:=False;
  if Assigned(List) then
  begin
    List.Items.BeginUpdate;
    List.Items.Clear;
    if Trim(Search)='' then
    begin
      Select:=FSqlite.GetTable('select * from logger order by datetimes desc');
    end
    else
    begin
      Select:=FSqlite.GetTable('select * from logger where informs like "%'+Trim(Search)+'%" order by datetimes desc');
    end;
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        for n:=0 to Select.Count-1 do
        begin
          Item:=List.Items.Add;
          Item.Caption:=DateTimeToStr(UnixToDateTime(Select.FieldAsInteger(0)));
          case Select.FieldAsInteger(1) of
            TYPE_SEARCH:
            begin
              Item.ImageIndex:=0;
              Item.SubItems.Add(Dictionary(25));
            end;
            TYPE_LINK:
            begin
              Item.ImageIndex:=1;
              Item.SubItems.Add(Dictionary(26));
            end;
            TYPE_START:
            begin
              Item.ImageIndex:=11;
              Item.SubItems.Add(Dictionary(27));
            end;
            TYPE_STOP:
            begin
              Item.ImageIndex:=12;
              Item.SubItems.Add(Dictionary(28));
            end;
            TYPE_BOOKMARK:
            begin
              Item.ImageIndex:=3;
              Item.SubItems.Add('Bookmark');
            end;
            TYPE_FAVORITE:
            begin
              Item.ImageIndex:=4;
              Item.SubItems.Add('Favorite');
            end;
            TYPE_DOWNLOAD:
            begin
              Item.ImageIndex:=8;
              Item.SubItems.Add('Download');
            end;
            TYPE_FILE:
            begin
              Item.ImageIndex:=9;
              Item.SubItems.Add('File');
            end;
            TYPE_FAULT:
            begin
              Item.ImageIndex:=10;
              Item.SubItems.Add('File');
            end;
            TYPE_HOME:
            begin
              Item.ImageIndex:=14;
              Item.SubItems.Add('Home');
            end;
            TYPE_PACKET_SUCCESS:
            begin
              Item.ImageIndex:=16;
              Item.SubItems.Add(Dictionary(29));
            end;
            TYPE_PACKET_FAULT:
            begin
              Item.ImageIndex:=16;
              Item.SubItems.Add(Dictionary(29));
            end;
            TYPE_WHOIS:
            begin
              Item.ImageIndex:=17;
              Item.SubItems.Add('Whois');
            end;
            TYPE_MYIP:
            begin
              Item.ImageIndex:=17;
              Item.SubItems.Add('Myip');
            end;
          end;
          Item.SubItems.Add(Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end;
    List.Items.EndUpdate;
  end;
end;

end.
