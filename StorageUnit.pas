﻿unit StorageUnit;

interface

uses

  Windows,
  Messages,
  SysUtils,
  SyncObjs,
  Classes,
  SuperObject,
  curl_h,
  SqliteTable3,
  DateUtils,
  Web.HTTPApp,
  Winapi.SHFolder,
  StorageData,
  MySql,
  Masks;

type

  TProvider = class
  private
    FActive: Boolean;
    FCritical: TCriticalSection;
    procedure Reset;
    function GetHeaderObject(Url: String): ISuperObject;
    function GetRestObject(Url: AnsiString): ISuperObject;
    procedure SetActive(Val: Boolean);
  protected
    function DoActivation: Boolean; virtual; abstract;
    function DoDeactivation: Boolean; virtual; abstract;
    function DoQuery(Header: ISuperObject; Body: ISuperObject): ISuperObject; virtual; abstract;
  public
    property Active: Boolean read FActive write SetActive;
    function Query(Url: String; Obj: ISuperObject): ISuperObject;
    constructor Create;
    destructor Destroy; override;
  end;

  TBrowser = class (TProvider)
  private
    function UnixToGoogle(V: UInt64): UInt64;
    function GoogleToUnix(V: UInt64): UInt64;
    function UnixToTime(dt: UInt64; frm: Integer): String;
  protected
    function GetCurrentLogin: String;
    function GetFileObject(Url: ISuperObject): ISuperObject;
    function DoQuery(Header: ISuperObject; Body: ISuperObject): ISuperObject; override;

    function DefBrowserInf(Body: ISuperObject; Response: ISuperObject; Name: String; Ver: String): Boolean;
    function DefStorageLstSql(Body: ISuperObject; frm: Integer; tbl: String; fld: String; limit: UInt64): String;
    function DefStorageLst(Body: ISuperObject; Response: ISuperObject; frm: Integer; url: String; dt: UInt64; desc: String): Boolean;
  protected
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; virtual; abstract;
    function StorageAdd(Body: ISuperObject): Boolean; virtual; abstract;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; virtual; abstract;
    function StorageClr(Body: ISuperObject; Response: ISuperObject): Boolean; virtual; abstract;
    function BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean; virtual; abstract;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; virtual; abstract;
  public
    function GetUrlObject(Url: String; dt: UInt64; Desc: String): ISuperObject;
  end;

  TMoogle = class (TBrowser)
  private
    FhMySql: PMYSQL;
    Fhstmt: PMYSQL_STMT;
    FhRes: PMYSQL_RES;
    FhRow: PMYSQL_ROW;
    FSqlite: TSQLiteDatabase;
    function GetDatabaseDir: String;
    function GetDatabaseFile: String;
    function GetMoogleVersion: String;
    function ReadString(Par: String; Def: string): string;
    function WriteString(Par: String; Val: string): Boolean;
    function ReadJson(Par: String; Def: ISuperObject): ISuperObject;
    function WriteJson(Par: String; Val: ISuperObject): Boolean;
  protected
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageClr(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TChrome = class (TBrowser)
  private
    FBookmark: ISuperObject;
    FChrome: TSQLiteDatabase;
    function GoogleToUnix(V: UInt64): UInt64;
    function UnixToGoogle(V: UInt64): UInt64;
    function GetChromePath: AnsiString;
    function GetChromeVersion: AnsiString;
  protected
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TEdge = class (TBrowser)
  private
    FBookmark: ISuperObject;
    FEdge: TSQLiteDatabase;
    function EdgeToUnix(V: UInt64): UInt64;
    function UnixToEdge(V: UInt64): UInt64;
    function GetEdgePath: AnsiString;
    function GetEdgeVersion: AnsiString;
  protected
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TMozilla = class (TBrowser)
  private
    FMozilla: TSQLiteDatabase;
    FLinks: TSQLiteDatabase;
    function GetMozillaPath: AnsiString;
    function GetMozillaVersion: AnsiString;
  protected
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TOpera = class (TBrowser)
  private
    FOpera: TSQLiteDatabase;
    FBookmark: ISuperObject;
    function GetOperaPath: AnsiString;
    function GetOperaVersion: AnsiString;
  protected
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TBrave = class (TBrowser)
  private
    FBrave: TSQLiteDatabase;
    FBookmark: ISuperObject;
    function GetBravePath: AnsiString;
    function GetBraveVersion: AnsiString;
  public
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;

  TVivaldi = class (TBrowser)
  private
    FVivaldi: TSQLiteDatabase;
    FBookmark: ISuperObject;
    function GetVivaldiPath: AnsiString;
    function GetVivaldiVersion: AnsiString;
  public
    function DoActivation: Boolean; override;
    function DoDeactivation: Boolean; override;
    function BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function StorageAdd(Body: ISuperObject): Boolean; override;
    function StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkAdd(Body: ISuperObject; Response: ISuperObject): Boolean; override;
    function BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean; override;
  end;


var

  Browsers: array of TBrowser;

implementation

const

  DEFAULT_LIMIT = 0;

var

  Count: UInt64 = 0;

procedure RegisterBrowser(Browser: TBrowser);
begin
  if Assigned(Browser) then
  begin
    Browser.Active:=True;
    if Browser.Active=True then
    begin
      Count:=Count+1;
      SetLength(Browsers,Count);
      Browsers[Count-1]:=Browser;
    end;
  end;
end;

function GetFileVersion( const sgFileName : string ) : string;
var infoSize: DWORD;
    verBuf:   pointer;
    verSize:  UINT;
    wnd:      UINT;
    FixedFileInfo : PVSFixedFileInfo;
begin
  Result:='0.0.0.0';
  if FileExists(sgFileName) then
  begin
    infoSize:=GetFileVersioninfoSize(PChar(sgFileName), wnd);
    if infoSize <> 0 then
    begin
      GetMem(verBuf, infoSize);
      try
        if GetFileVersionInfo(PChar(sgFileName), wnd, infoSize, verBuf) then
        begin
          VerQueryValue(verBuf, '\', Pointer(FixedFileInfo), verSize);
          Result := IntToStr(FixedFileInfo.dwFileVersionMS div $10000) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionMS and $0FFFF) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionLS div $10000) + '.' +
                    IntToStr(FixedFileInfo.dwFileVersionLS and $0FFFF);
        end;
      finally
        FreeMem(verBuf);
      end;
    end;
  end;
end;

procedure ShowMessage(Str: String);
begin
  MessageBoxW(0,PWChar(Str),'sdfsd',0);
end;

function OnRecvData(Buffer: PChar; Size, nItems: longword; UserData: pointer): integer;  cdecl;
begin
  Result:=Size*nItems;
  if Result>0 then TMemoryStream(UserData).Write(Buffer^,Result);
end;

constructor TProvider.Create;
begin
  inherited Create;
  Reset;
  FCritical:=TCriticalSection.Create;
end;

destructor TProvider.Destroy;
begin
  Active:=False;
  FCritical.Destroy;
  Reset;
  inherited Destroy;
end;

procedure TProvider.Reset;
begin
  FActive:=False;
  FCritical:=nil;
end;

function TProvider.GetHeaderObject(Url: String): ISuperObject;
var n: Integer;
    l: Integer;
    m: Integer;
    Str: String;
begin
  if Url<>'' then
  begin
    Result:=SO('{"array":[]}');
    Result.S['url']:=Trim(Url);
    Str:='';
    m:=0;
    for n:=1 to Length(Url) do
    begin
      if Url[n]<>' ' then
      begin
        if pos(Url[n],':/\&?')=0 then
        begin
          Str:=Str+Url[n];
        end
        else
        begin
          if Str<>'' then
          begin
            l:=pos('=',Str);
            if l>0 then
            begin
              Result.S[LowerCase(Copy(Str,1,l-1))]:=Copy(Str,l+1,Length(Str)-l);
            end
            else
            begin
              Result.A['array'].Add(LowerCase(Str));
            end;
          end;
          Str:='';
        end;
      end;
    end;
    if Str<>'' then
    begin
      l:=pos('=',Str);
      if l>0 then
      begin
        Result.S[LowerCase(Copy(Str,1,l-1))]:=Copy(Str,l+1,Length(Str)-l);
      end
      else
      begin
        Result.A['array'].Add(LowerCase(Str));
      end;
    end;
    Str:='';
  end
  else
  begin
    Result:=nil;
  end;
end;

function TProvider.GetRestObject(Url: AnsiString): ISuperObject;
var FhCurl: pCurl;
    FhCurlCode: CURLcode;
    FhCurlStream: TMemoryStream;
    FParams: pcurl_slist;
    Opt: CURLoption;
begin
  if Url<>'' then
  begin
    FhCurl:=curl_easy_init;
    if FhCurl<>nil then
    begin
      Opt:=CURLoption.CURLOPT_WRITEFUNCTION;
      FhCurlCode:=curl_easy_setopt(FhCurl,Opt, @OnRecvData);
      if FhCurlCode=CURLE_OK then
      begin
        FhCurlStream:=TMemoryStream.Create;
        FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_WRITEDATA,Pointer(FhCurlStream));
        if FhCurlCode=CURLE_OK then
        begin
          FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_URL, PAnsiChar(Url));
          if FhCurlCode=CURLE_OK then
          begin
            //curl_easy_setopt(FhCurl, CURLOPT_CAINFO, PAnsiChar(ExtractFilePath(paramstr(0))+'cacet.pem'));
            FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_SSL_VERIFYPEER,false);
            if FhCurlCode=CURLE_OK  then
            begin
              FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,60);
              if FhCurlCode=CURLE_OK  then
              begin
                FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,60);
                if FhCurlCode=CURLE_OK  then
                begin
                  FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_HTTPGET,1);
                  if FhCurlCode=CURLE_OK  then
                  begin
                    FParams:=nil;
                    FParams:=curl_slist_append(FParams,'Content-type: application/json');
                    FParams:=curl_slist_append(FParams,'Accept: application/json');
                    FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
                    if FhCurlCode=CURLE_OK  then
                    begin
                      FhCurlCode:=curl_easy_perform(FhCurl);
                      if FhCurlCode=CURLE_OK  then
                      begin
                        if FhCurlStream.Size>0 then
                        begin
                          Result:=SO(PAnsiChar(FhCurlStream.Memory));
                        end
                        else
                        begin
                          Result:=SO;
                          Result.B['response.result']:=False;
                          Result.I['response.code']:=400;
                          Result.S['response.status']:='Bad request';
                          Result.S['response.description']:='Curl no data';
                        end;
                      end
                      else
                      begin
                        Result:=SO;
                        Result.B['response.result']:=False;
                        Result.I['response.code']:=400;
                        Result.S['response.status']:='Bad request';
                        Result.S['response.description']:='Curl QUERY '+IntToStr(Integer(FhCurlCode));
                      end;
                    end
                    else
                    begin
                      Result:=SO;
                      Result.B['response.result']:=False;
                      Result.I['response.code']:=400;
                      Result.S['response.status']:='Bad request';
                      Result.S['response.description']:='Curl CURLOPT_HTTPHEADER '+IntToStr(Integer(FhCurlCode));
                    end;
                    curl_slist_free_all(FParams);
                    FParams:=nil;
                  end
                  else
                  begin
                    Result:=SO;
                    Result.B['response.result']:=False;
                    Result.I['response.code']:=400;
                    Result.S['response.status']:='Bad request';
                    Result.S['response.description']:='Curl CURLOPT_HTTPGET '+IntToStr(Integer(FhCurlCode));
                  end;
                end
                else
                begin
                  Result:=SO;
                  Result.B['response.result']:=False;
                  Result.I['response.code']:=400;
                  Result.S['response.status']:='Bad request';
                  Result.S['response.description']:='Curl CURLOPT_TIMEOUT '+IntToStr(Integer(FhCurlCode));
                end;
              end
              else
              begin
                Result:=SO;
                Result.B['response.result']:=False;
                Result.I['response.code']:=400;
                Result.S['response.status']:='Bad request';
                Result.S['response.description']:='Curl CURLOPT_CONNECTTIMEOUT '+IntToStr(Integer(FhCurlCode));
              end;
            end
            else
            begin
              Result:=SO;
              Result.B['response.result']:=False;
              Result.I['response.code']:=400;
              Result.S['response.status']:='Bad request';
              Result.S['response.description']:='Curl CURLOPT_SSL_VERIFYPEER '+IntToStr(Integer(FhCurlCode));
            end;
          end
          else
          begin
            Result:=SO;
            Result.B['response.result']:=False;
            Result.I['response.code']:=400;
            Result.S['response.status']:='Bad request';
            Result.S['response.description']:='Curl CURLOPT_URL '+IntToStr(Integer(FhCurlCode));
          end;
        end
        else
        begin
          Result:=SO;
          Result.B['response.result']:=False;
          Result.I['response.code']:=400;
          Result.S['response.status']:='Bad request';
          Result.S['response.description']:='Curl CURLOPT_WRITEDATA '+IntToStr(Integer(FhCurlCode));
        end;
        FhCurlStream.Destroy;
        FhCurlStream:=nil;
      end
      else
      begin
        Result:=SO;
        Result.B['response.result']:=False;
        Result.I['response.code']:=400;
        Result.S['response.status']:='Bad request';
        Result.S['response.description']:='Curl CURLOPT_WRITEFUNCTION '+IntToStr(Integer(FhCurlCode));
      end;
      curl_easy_cleanup(FhCurl);
      FhCurl:=nil;
    end
    else
    begin
      Result:=SO;
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Curl initialization fault';
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['response.result']:=False;
    Result.I['response.code']:=400;
    Result.S['response.status']:='Bad request';
    Result.S['response.description']:='Url not found';
  end;
end;

procedure TProvider.SetActive(Val: Boolean);
begin
  if (FActive=False) and (Val=True) and (DoActivation=True) then
  begin
    FActive:=True;
  end;
  if (FActive=True) and (Val=False) and (DoDeactivation=True) then
  begin
    FActive:=False;
  end;
end;

function TProvider.Query(Url: String; Obj: ISuperObject): ISuperObject;
var Header: ISuperObject;
begin
  FCritical.Enter;
  if FActive=True then
  begin
    Header:=GetHeaderObject(Url);
    if (Assigned(Header)=True) and
       (Assigned(Header.O['url'])=True) and
       (Header.S['url']<>'') then
    begin
      if (Header.A['array'].S[0]='http:') or
         (Header.A['array'].S[0]='https:') then
      begin
        Result:=GetRestObject(Header.S['url']);
      end
      else
      begin
        if (Header.A['array'].S[0]='c:') or
           (Header.A['array'].S[0]='d:') then
        begin
          //Disc
        end
        else
        begin
          if (Header.A['array'].S[0]='service') then
          begin
            Result:=DoQuery(Header,Obj);
          end
          else
          begin
            Result:=DoQuery(Header,Obj);
          end;
        end;
      end;
    end
    else
    begin
      Result:=SO;
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Object url not found';
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['response.result']:=False;
    Result.I['response.code']:=400;
    Result.S['response.status']:='Bad request';
    Result.S['response.description']:='Not activated';
  end;
  FCritical.Leave;
end;

//------------------------------------------------------------------------------

function TBrowser.UnixToGoogle(V: UInt64): UInt64;
begin
  Result:=(V+11644473600)*1000000;
end;

function TBrowser.GoogleToUnix(V: UInt64): UInt64;
begin
  if V>0 then
  begin
    if (Round(V/1000000)-11644473600)>0 then Result:=Round(V/1000000)-11644473600 else Result:=0;
  end
  else
  begin
    Result:=0;
  end;
end;

function TBrowser.UnixToTime(dt: UInt64; frm: Integer): String;
begin
  if dt=0 then dt:=DateTimeToUnix(EncodeDateTime(YearOf(Now),MonthOf(Now),DayOf(Now),23,59,59,0));
  case frm of
    0: Result:=IntToStr(dt);
    1: Result:=IntToStr(UnixToGoogle(dt));
  else
    Result:=IntToStr(dt);
  end;
end;

function TBrowser.GetUrlObject(Url: String; dt: UInt64; Desc: String): ISuperObject;
var l: Integer;
    n: Integer;
    m: Integer;
    q: Integer;
    b: String;
    s: String;
    d: String;
    k: String;
    O: ISuperObject;
    H: ISuperObject;
    P: ISuperObject;
    z: String;
    e: Integer;
begin
  Url:=Trim(Url);
  if Url<>'' then
  begin
    b:=':/\?;#&';
    q:=0;
    s:='';
    try
      d:=HTTPDecode(Url);
    except
      d:=StringReplace(Url,'%','',[rfReplaceAll]);
    end;
    Result:=SO('{"raw":[]}');
    Result.S['org']:=Url;
    Result.S['dsc']:=Trim(Desc);
    Result.S['url']:=d;
    if (Pos(':\',d)>0) or (Pos(':/',d)>0) then
    begin
      O:=SO;
      H:=SA([]);
      P:=SA([]);
      for n:=1 to Length(d) do
      begin
        if pos(d[n],b)=0 then
        begin
          s:=s+d[n];
        end
        else
        begin
          if d[n]=':' then Delete(b,1,1);
          if d[n]='?' then b:='&';
          if s<>'' then
          begin
            if q=0 then
            begin
              Result.A['raw'].Add(LowerCase(Trim(s)));
            end
            else
            begin
              if q=1 then
              begin
                k:='';
                for m:=1 to Length(S) do
                begin
                  if pos(s[m],':@.')=0 then
                  begin
                    k:=k+s[m];
                  end
                  else
                  begin
                    if k<>'' then
                    begin
                      H.AsArray.Add(Trim(k));
                      k:='';
                    end;
                  end;
                end;
                if k<>'' then
                begin
                  H.AsArray.Add(Trim(k));
                  k:='';
                end;
              end
              else
              begin
                l:=pos('=',s);
                if l>0 then
                begin
                  z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
                  z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
                  z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
                  O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
                end
                else
                begin
                  P.AsArray.Add(Trim(s));
                end;
              end;
            end;
            q:=q+1;
            s:='';
          end;
        end;
      end;
      if s<>'' then
      begin
        if q=0 then
        begin
          Result.A['raw'].Add(LowerCase(Trim(s)));
        end
        else
        begin
          if q=1 then
          begin
            k:='';
            for m:=1 to Length(S) do
            begin
              if pos(s[m],':@.')=0 then
              begin
                k:=k+s[m];
              end
              else
              begin
                if k<>'' then
                begin
                  H.AsArray.Add(Trim(k));
                  k:='';
                end;
              end;
            end;
            if k<>'' then
            begin
              H.AsArray.Add(Trim(k));
              k:='';
            end;
          end
          else
          begin
            l:=pos('=',s);
            if l>0 then
            begin
              z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
              z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
              z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
              O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
            end
            else
            begin
              P.AsArray.Add(Trim(s));
            end;
          end;
        end;
        q:=q+1;
        s:='';
      end;
      Result.A['raw'].Add(H);
      Result.A['raw'].Add(P);
      Result.A['raw'].Add(O);
    end
    else
    begin
      if Pos(';',d)>0 then
      begin
        for n:=1 to Length(d) do
        begin
          if d[n]<>';' then
          begin
            s:=s+d[n];
          end
          else
          begin
            if s<>'' then
            begin
              Result.A['raw'].Add(Trim(s));
              s:='';
            end;
          end;
        end;
        if s<>'' then
        begin
          Result.A['raw'].Add(s);
          s:='';
        end;
      end
      else
      begin
        for n:=1 to Length(d) do
        begin
          if d[n]<>' ' then
          begin
            s:=s+d[n];
          end
          else
          begin
            if s<>'' then
            begin
              Result.A['raw'].Add(s);
              s:='';
            end;
          end;
        end;
        if s<>'' then
        begin
          Result.A['raw'].Add(s);
          s:='';
        end;
      end;
    end;
    if dt=0 then Result.I['dtm.dt']:=DateTimeToUnix(Now) else Result.I['dtm.dt']:=dt;
    Result.S['dtm.st']:=DateTimeToStr(UnixToDateTime(Result.I['dtm.dt']));
    if Result.A['raw'].Length>0 then
    begin
      Result.S['obj.link.protocol']:=LowerCase(Result.A['raw'].S[0]);

      if (Result.S['obj.link.protocol']='http') or
         (Result.S['obj.link.protocol']='https') then
      begin
        Result.S['obj.link.name']:='localhost';
        Result.S['obj.link.group']:='localhost';
        Result.S['obj.link.domain']:='localhost';
        if Result.S['obj.link.protocol']='http' then Result.I['obj.link.port']:=80 else Result.I['obj.link.port']:=443;
        Result.S['obj.link.path']:='/';
        if Result.A['raw'].Length>1 then
        begin
          if Result.A['raw'].O[1].AsArray.Length>2 then
          begin
            Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
            Result.S['obj.link.group']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-3]);
            Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
            for n:=Result.A['raw'].O[1].AsArray.Length-4 downto 0 do
            begin
              Result.S['obj.link.group']:=Result.S['obj.link.group']+'.'+LowerCase(Result.A['raw'].O[1].AsArray.S[n]);
            end;
            Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
          end
          else
          begin
            if Result.A['raw'].O[1].AsArray.Length=2 then
            begin
              Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
              Result.S['obj.link.group']:='main';
              Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
              Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
            end
            else
            begin
              if Result.A['raw'].O[1].AsArray.Length=1 then
              begin
                Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[0]);
                Result.S['obj.link.group']:=Result.S['obj.link.name'];
                Result.S['obj.link.domain']:=Result.S['obj.link.name'];
                Result.S['obj.link.top']:=Result.S['obj.link.name'];
              end;
            end;
          end;
          if Result.A['raw'].Length>2 then
          begin
            if Result.A['raw'].O[2].AsArray.Length>0 then
            begin
              Result.S['obj.link.path']:='';
              for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
              begin
                Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
              end;
            end;
          end;

          if Result.A['raw'].Length=4 then
          begin
            Result.O['obj.link.value']:=Result.A['raw'].O[3];
            for n:=Low(DICTIONARY) to High(DICTIONARY) do
            begin
              if (Result.S['obj.link.name']=DICTIONARY[n].N) and
                 ((Result.S['obj.link.path']=DICTIONARY[n].M) or (DICTIONARY[n].M='')) then
              begin
                if Assigned(Result.O['obj.link.value.'+DICTIONARY[n].V])=True then
                begin
                  Result.S['obj.option.typ']:=DICTIONARY[n].T;
                  Result.S['obj.option.url']:=Result.S['org'];
                  Result.S['obj.option.res']:=Result.S['obj.link.value.'+DICTIONARY[n].V];
                  if Result.S['dsc']<>'' then Result.S['obj.option.dsc']:=Result.S['dsc'] else Result.S['obj.option.dsc']:=Result.S['obj.option.res'];
                end
                else
                begin
                  if (Result.A['raw'].O[2].AsArray.Length>0) and
                     (pos(DICTIONARY[n].V,'0123456789')>0) and
                     (Result.A['raw'].O[2].AsArray.Length>StrToInt(DICTIONARY[n].V)) then
                  begin
                    Result.S['obj.option.typ']:=DICTIONARY[n].T;
                    Result.S['obj.option.url']:=Result.S['org'];
                    Result.S['obj.option.res']:=Result.A['raw'].O[2].AsArray.S[StrToInt(DICTIONARY[n].V)];
                    if Result.S['dsc']<>'' then Result.S['obj.option.dsc']:=Result.S['dsc'] else Result.S['obj.option.dsc']:=Result.S['obj.option.res'];
                  end;
                end;
                Break;
              end;
            end;

            if Result.S['obj.option.typ']='' then
            begin
              if (Result.A['raw'].O[2].AsArray.Length>0) and
                 (pos('.',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])>0) and
                 (pos(',',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])=0) then
              begin
                Result.S['obj.option.typ']:='file';
                Result.S['obj.option.url']:=Result.S['org'];
                Result.S['obj.option.dsc']:=Result.S['dsc'];
                Result.S['obj.option.res']:=Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1];
                Result.S['obj.option.nam']:=Result.S['obj.option.res'];
                Result.S['obj.option.ext']:=ExtractFileExt(Result.S['obj.option.res']);
              end
              else
              begin
                Result.S['obj.option.typ']:='link';
                Result.S['obj.option.url']:=Result.S['org'];
                Result.S['obj.option.dsc']:=Result.S['dsc'];
              end;
            end;
          end;
        end;
      end
      else
      begin
        if Result.S['obj.link.protocol']='ftp' then
        begin
          Result.S['obj.link.name']:='localhost';
          Result.S['obj.link.group']:='localhost';
          Result.S['obj.link.domain']:='localhost';
          Result.I['obj.link.port']:=21;
          Result.S['obj.link.login']:='anonymous';
          Result.S['obj.link.password']:='anonymous';
          Result.S['obj.link.path']:='/';
          if Result.A['raw'].Length>1 then
          begin
            if Result.A['raw'].O[1].AsArray.Length=4 then
            begin

            end;
          end;
          //ftp://user:password@host:port/path
          if Result.A['raw'].Length>2 then
          begin
            for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
            begin
              Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
            end;
          end;
        end
        else
        begin
          if Result.S['obj.link.protocol']='file' then
          begin
            if (Result.A['raw'].Length>3) and
               (Result.A['raw'].O[1].AsArray.Length=1) then
            begin
              Result.S['obj.link.value.full']:=Result.A['raw'].O[1].AsArray.S[0]+':';
              for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
              begin
                Result.S['obj.link.value.full']:=Result.S['obj.link.value.full']+'\'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
              end;
              Result.B['obj.link.value.exst']:=FileExists(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.path']:=ExtractFilePath(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.name']:=ExtractFileName(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.extn']:=ExtractFileExt(Result.S['obj.link.value.full']);
            end;
          end;
        end;
      end;



    end;
  end
  else
  begin
    Result:=nil;
  end;
end;

function TBrowser.GetCurrentLogin: String;
var Len: Cardinal;
    Buf: array [0..255] of AnsiChar;
begin
  Len:=256;
  FillChar(Buf,256,0);
  if GetUserNameA(@Buf,Len)=True then
  begin
    SetString(Result,Buf,Len-1);
  end
  else
  begin
    Result:='Default';
  end;
end;

function TBrowser.GetFileObject(Url: ISuperObject): ISuperObject;

  function ExtractFileSize(p_sFilePath : string): Int64;
  var
    oFile : file of Byte;
  begin
    Result:=0;
    //System.AssignFile(oFile, p_sFilePath);
    //try
    //  System.Reset(oFile);
    //  Result:= System.FileSize(oFile);
    //finally
    //   System.CloseFile(oFile);
    //end;
  end;

begin
  if (Assigned(Url)=True) and
     (Assigned(Url.O['array'])=True) and
     (Url.A['array'].Length>0) then
  begin
    if Url.A['array'].S[0]='file' then
    begin
      Result:=SO;
      Result.S['origin']:=Url.S['origin'];
      Result.S['type']:='local';
      Result.S['full']:=StringReplace(StringReplace(Url.S['origin'],'file:///','',[rfReplaceAll, rfIgnoreCase]),'/','\',[rfReplaceAll, rfIgnoreCase]);
      Result.S['path']:=ExtractFilePath(Result.S['full']);
      Result.S['name']:=ExtractFileName(Result.S['full']);
      Result.S['ext']:=ExtractFileExt(Result.S['full']);
      if FileExists(Result.S['full'])=True then
      begin
        Result.B['exists']:=True;
        Result.I['size']:=ExtractFileSize(Result.S['full']);
      end
      else
      begin
        Result.B['exists']:=False;
        Result.I['size']:=-1;
      end;
    end
    else
    begin
      if ((Url.A['array'].S[0]='http') or (Url.A['array'].S[0]='https')) and
         ((pos('.pdf',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.iso',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.exe',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.jpg',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.jpeg',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.doc',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.rtf',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.avi',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.mp3',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.png',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.msi',Url.A['array'].S[Url.A['array'].Length-1])>0) or
          (pos('.wav',Url.A['array'].S[Url.A['array'].Length-1])>0)) then
      begin
        Result:=SO;
        Result.S['origin']:=Url.S['origin'];
        Result.S['type']:='remote';
        Result.S['full']:=Url.S['origin'];
        Result.S['path']:='unknown';
        Result.S['name']:=Url.A['array'].S[Url.A['array'].Length-1];
        Result.S['ext']:=ExtractFileExt(Result.S['name']);
        Result.B['exists']:=True;
        Result.I['size']:=0;
      end
      else
      begin
        Result:=nil;
      end;
    end;
  end
  else
  begin
    Result:=nil;
  end;
end;

function TBrowser.DoQuery(Header: ISuperObject; Body: ISuperObject): ISuperObject;
var n,M: Integer;
begin
  Result:=SO;
  Result.B['response.result']:=False;
  Result.I['response.code']:=400;
  Result.S['response.status']:='Bad request';
  Result.S['response.description']:='Request not supported';
  Result.O['response']:=SO('{"array":[]}');

  if pos('api/browser/info',Header.S['url'])>0 then
  begin
    if BrowserInf(Body,Result)=True then
    begin
      Result.B['response.result']:=True;
      Result.I['response.code']:=200;
      Result.S['response.status']:='OK';
      Result.S['response.description']:='Browser info';
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Cannot get browser info';
    end;
    Exit;
  end;

  if pos('api/browser/storage/clr',Header.S['url'])>0 then
  begin
    if StorageClr(Body,Result)=True then
    begin
      Result.B['response.result']:=True;
      Result.I['response.code']:=200;
      Result.S['response.status']:='OK';
      Result.S['response.description']:='Storage clear';
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Cannot storage clear';
    end;
    Exit;
  end;

  if pos('api/browser/storage/add',Header.S['url'])>0 then
  begin
    if (Assigned(Body)=True) and
       (Assigned(Body.O['link'])=True) and
       (Trim(Body.S['link'])<>'') and
       (Assigned(Body.O['desc'])=True) and
       (Trim(Body.S['desc'])<>'') then
    begin
      if StorageAdd(Body)=True then
      begin
        Result.B['response.result']:=True;
        Result.I['response.code']:=200;
        Result.S['response.status']:='OK';
        Result.S['response.description']:='Record added';
      end
      else
      begin
        Result.B['response.result']:=False;
        Result.I['response.code']:=400;
        Result.S['response.status']:='Bad request';
        Result.S['response.description']:='Cannot add record';
      end;
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Variables not found';
    end;
    Exit;
  end;

  if pos('api/browser/storage/lst',Header.S['url'])>0 then
  begin
    if StorageLst(Body,Result)=True then
    begin
      Result.B['response.result']:=True;
      Result.I['response.code']:=200;
      Result.S['response.status']:='OK';
      Result.S['response.description']:='Record list get it';
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Cannot get list records';
    end;
    Exit;
  end;

  if pos('api/browser/bookmark/add',Header.S['url'])>0 then
  begin
    if BookmarkAdd(Body,Result)=True then
    begin
      Result.B['response.result']:=True;
      Result.I['response.code']:=200;
      Result.S['response.status']:='OK';
      Result.S['response.description']:='Record list get it';
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Cannot get list records';
    end;
    Exit;
  end;

  if pos('api/browser/bookmark/lst',Header.S['url'])>0 then
  begin
    if BookmarkLst(Body,Result)=True then
    begin
      Result.B['response.result']:=True;
      Result.I['response.code']:=200;
      Result.S['response.status']:='OK';
      Result.S['response.description']:='Record list get it';
    end
    else
    begin
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Cannot get list records';
    end;
    Exit;
  end;

end;

function TBrowser.DefBrowserInf(Body: ISuperObject; Response: ISuperObject; Name: String; Ver: String): Boolean;
var Obj: ISuperObject;
begin
  if (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    Obj:=SO;
    if Name<>'' then Obj.S['name']:=Trim(Name) else Obj.S['name']:='unknown';
    if Ver<>'' then Obj.S['version']:=Trim(Ver) else Obj.S['version']:='unknown';
    Response.A['response.array'].Add(Obj);
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

function TBrowser.DefStorageLstSql(Body: ISuperObject; frm: Integer; tbl: String; fld: String; limit: UInt64): String;
begin
  if Assigned(Body)=True then
  begin
    if (Assigned(Body.O['from'])=True) and (Assigned(Body.O['to'])=True) then
    begin
      if Body.I['from']=Body.I['to'] then
      begin
        if limit>0 then
        begin
          Result:='select * from '+tbl+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
        end
        else
        begin
          Result:='select * from '+tbl+' ORDER BY '+fld+' DESC';
        end;
      end
      else
      begin
        if Body.I['from']<Body.I['to'] then
        begin
          if limit>0 then
          begin
            Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['from'],frm)+' and '+fld+'<='+UnixToTime(Body.I['to'],frm)+' ORDER BY '+fld+' ASC limit '+IntToStr(limit);
          end
          else
          begin
            Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['from'],frm)+' and '+fld+'<='+UnixToTime(Body.I['to'],frm)+' ORDER BY '+fld+' ASC';
          end;
        end
        else
        begin
          if Body.I['from']>Body.I['to'] then
          begin
            if limit>0 then
            begin
              Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['to'],frm)+' and '+fld+'<='+UnixToTime(Body.I['from'],frm)+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
            end
            else
            begin
              Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['to'],frm)+' and '+fld+'<='+UnixToTime(Body.I['from'],frm)+' ORDER BY '+fld+' DESC';;
            end;
          end
          else
          begin
            if limit>0 then
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
            end
            else
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC';
            end;
          end;
        end;
      end;
    end
    else
    begin
      if (Assigned(Body.O['from'])=True) and (Assigned(Body.O['to'])=False) then
      begin
        if limit>0 then
        begin
          Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['from'],frm)+' ORDER BY '+fld+' ASC limit '+IntToStr(limit);
        end
        else
        begin
          Result:='select * from '+tbl+' where '+fld+'>='+UnixToTime(Body.I['from'],frm)+' ORDER BY '+fld+' ASC';
        end;
      end
      else
      begin
        if (Assigned(Body.O['to'])=True) and (Assigned(Body.O['from'])=False) then
        begin
          if limit>0 then
          begin
            Result:='select * from '+tbl+' where '+fld+'<='+UnixToTime(Body.I['to'],frm)+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
          end
          else
          begin
            Result:='select * from '+tbl+' where '+fld+'<='+UnixToTime(Body.I['to'],frm)+' ORDER BY '+fld+' DESC';
          end;
        end
        else
        begin
          if (Assigned(Body.O['to'])=False) and (Assigned(Body.O['from'])=False) then
          begin
            if limit>0 then
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
            end
            else
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC';
            end;
          end
          else
          begin
            if limit>0 then
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
            end
            else
            begin
              Result:='select * from '+tbl+' ORDER BY '+fld+' DESC';
            end;
          end;
        end;
      end;
    end;
  end
  else
  begin
    if limit>0 then
    begin
      Result:='select * from '+tbl+' ORDER BY '+fld+' DESC limit '+IntToStr(limit);
    end
    else
    begin
      Result:='select * from '+tbl+' ORDER BY '+fld+' DESC';
    end;
  end;
end;

function TBrowser.DefStorageLst(Body: ISuperObject; Response: ISuperObject; frm: Integer; url: String; dt: UInt64; desc: String): Boolean;
var Obj: ISuperObject;
begin
  if (Assigned(Body)=True) and
     (Assigned(Response)=True) and
     (url<>'') then
  begin
    try
      case frm of
        0: Obj:=GetUrlObject(url,dt,desc);
        1: Obj:=GetUrlObject(url,GoogleToUnix(dt),desc);
      else
        Obj:=GetUrlObject(url,dt,desc);
      end;
    except
      on E: Exception do
      begin
        Obj:=SO;
      end;
    end;
    Response.A['response.array'].Add(Obj);
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

//------------------------------------------------------------------------------

function TMoogle.GetDatabaseDir: String;
begin
  Result:='C:\ProgramData\TAO\';
end;

function TMoogle.GetDatabaseFile: String;
begin
  Result:='tao.db';
end;

function TMoogle.DoActivation: Boolean;
var Sql: String;
begin
  FSqlite:=nil;
  if FileExists(GetDatabaseDir+GetDatabaseFile)=True then
  begin
    try
      FSqlite:=TSQLiteDatabase.Create(GetDatabaseDir+GetDatabaseFile);
      FSqlite.ExecSQL('PRAGMA synchronous=0');
      FSqlite.ExecSQL('PRAGMA encoding = "UTF-8"');
      FSqlite.ExecSQL('PRAGMA cache_size=3000');
      FSqlite.ExecSQL('PRAGMA temp_store=MEMORY');
      Result:=True;
    except
      Result:=False;
    end;
  end
  else
  begin
    ForceDirectories(GetDatabaseDir);
    try
      FSqlite:=TSQLiteDatabase.Create(GetDatabaseDir+GetDatabaseFile);
      FSqlite.ExecSQL('PRAGMA synchronous=0');
      FSqlite.ExecSQL('PRAGMA encoding = "UTF-8"');
      FSqlite.ExecSQL('PRAGMA cache_size=3000');
      FSqlite.ExecSQL('PRAGMA temp_store=MEMORY');
      if FSqlite.TableExists('logger')=False then FSqlite.ExecSQL('create table logger (uxdt INTEGER, link TEXT, descr TEXT)');
      if FSqlite.TableExists('inifile')=False then FSqlite.ExecSQL('create table inifile (parameter TEXT, value TEXT)');
      Result:=True;
    except
      Result:=False;
    end;
  end;
  libmysql_load(nil);
  FhMySql:=mysql_init(nil);
end;

function TMoogle.DoDeactivation: Boolean;
begin
  if Assigned(FSqlite) then
  begin
    FSqlite.Destroy;
    FSqlite:=nil;
  end;
  if Assigned(FhMySql) then
  begin
    mysql_close(FhMySql);
    FhMySql:=nil;
  end;
end;

function TMoogle.GetMoogleVersion: String;
begin
  Result:=GetFileVersion('C:\Program Files (x86)\Moogle\Browser.exe');
end;

function TMoogle.ReadString(Par: String; Def: string): string;
var Select: TSQLiteTable;
begin
  Result:=Def;
  if Par<>'' then
  begin
    Select:=FSqlite.GetTable('select * from inifile where parameter="'+LowerCase(Trim(Par))+'"');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then Result:=HTTPDecode(Select.FieldAsString(1));
      Select.Destroy;
      Select:=nil;
    end;
  end;
end;

function TMoogle.WriteString(Par: String; Val: string): Boolean;
var Select: TSQLiteTable;
    Sql: AnsiString;
begin
  Result:=False;
  if Par<>'' then
  begin
    Select:=FSqlite.GetTable('select * from inifile where parameter="'+LowerCase(Par)+'"');
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Sql:='update inifile set value="'+HTTPEncode(Trim(Val))+'" where parameter="'+LowerCase(Trim(Par))+'"';
        try
          FSqlite.ExecSQL(Sql);
          Result:=True;
        except
          Result:=False;
        end;
      end
      else
      begin
        Sql:='insert into inifile (parameter,value) values (';
        Sql:=Sql+'"'+LowerCase(Trim(Par))+'",';
        Sql:=Sql+'"'+HTTPEncode(Trim(Val))+'");';
        try
          FSqlite.ExecSQL(Sql);
          Result:=True;
        except
          Result:=False;
        end;
      end;
      Select.Destroy;
      Select:=nil;
    end;
  end;
end;

function TMoogle.ReadJson(Par: String; Def: ISuperObject): ISuperObject;
var Select: TSQLiteTable;
    Json: String;
begin
  if Assigned(Def) then Json:=ReadString(Par,Def.AsJSon) else Json:=ReadString(Par,'{}');
  Result:=SO(Json);
end;

function TMoogle.WriteJson(Par: String; Val: ISuperObject): Boolean;
begin
  if Assigned(Val) then Result:=WriteString(Par,Val.AsJSon) else Result:=WriteString(Par,'{}');
end;

function TMoogle.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'TAO',GetMoogleVersion);
end;

function TMoogle.StorageClr(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=False;
  if Assigned(FSqlite) then
  begin
    try
      FSqlite.ExecSQL('delete from logger');
      Result:=True;
    except
      Result:=False;
    end;
  end;
end;

function TMoogle.BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean;
var Obj: ISuperObject;
begin
  Obj:=ReadJson('bookmarks.json',nil);
  if Assigned(Obj)=True then
  begin
    if Assigned(Obj.O['bookmarks'])=False then Obj.O['bookmarks']:=SO('[]');
    Obj.A['bookmarks'].Add(Body);
    Result:=WriteJson('bookmarks.json',Obj);
  end
  else
  begin
    Result:=False;
  end;
end;

function TMoogle.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  Obj:=ReadJson('bookmarks.json',nil);
  if (Assigned(Obj)=True) and
     (Assigned(Obj.O['bookmarks'])=True) and
     (Obj.A['bookmarks'].Length>0) then
  begin
    for n:=0 to Obj.A['bookmarks'].Length-1 do
    begin
      Response.A['response.array'].Add(Obj.A['bookmarks'].O[n]);
    end;
    Result:=True;
  end
  else
  begin
    Result:=True;
  end;
end;

function TMoogle.StorageAdd(Body: ISuperObject): Boolean;
var Sql: String;

function NowUTC: TDateTime;
var
  SystemTime: TSystemTime;
begin
  GetSystemTime(SystemTime);
  with SystemTime do
  begin
    Result:=EncodeTime(wHour, wMinute, wSecond, wMilliSeconds)+EncodeDate (wYear, wMonth, wDay);
  end;
end;

begin
  Result:=False;
  if (Assigned(Body)=True) and
     (Assigned(FSqlite)=True) then
  begin
    Sql:='insert into logger (uxdt,link,descr) values (';
    if Body.I['time']<>0 then
    begin
      Sql:=Sql+IntToStr(Body.I['time'])+',';
    end
    else
    begin
      Sql:=Sql+IntToStr(DateTimeToUnix(NowUTC))+',';
    end;
    Sql:=Sql+'"'+HTTPEncode(Body.S['link'])+'",';
    Sql:=Sql+'"'+HTTPEncode(Body.S['desc'])+'");';
    try
      FSqlite.ExecSQL(Sql);
      Result:=True;
    except
      Result:=False;
    end;
    if Body.B['cloud']=True then
    begin
      if Assigned(FhMySql) then
      begin
        FhMySql:=mysql_real_connect(FhMySql,PAnsiChar(AnsiString('127.0.0.1')),PAnsiChar(AnsiString('root')), PAnsiChar(AnsiString('root')),PAnsiChar(AnsiString('tao')),49155, nil, 0);
        if Assigned(FhMySql) then
        begin
          if mysql_query(FhMySql, 'CREATE TABLE IF NOT EXISTS logger (uxdt INT, link VARCHAR(256), descr VARCHAR(256))')=0 then Result:=True;
          mysql_query(FhMySql, PAnsiChar(AnsiString(Sql)));
          Result:=True;
        end;
      end;
    end
    else
    begin
      Result:=True;
    end;
  end;
end;

function TMoogle.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Select: TSQLiteTable;
    Obj: ISuperObject;
    n: Integer;
    Count: UInt64;
begin
  if Body.B['cloud']=True then
  begin
    Result:=False;
    if Assigned(FhMySql) then
    begin
      FhMySql:=mysql_real_connect(FhMySql,PAnsiChar(AnsiString('127.0.0.1')),PAnsiChar(AnsiString('root')), PAnsiChar(AnsiString('root')),PAnsiChar(AnsiString('tao')),49155, nil, 0);
      if mysql_query(FhMySql, PAnsiChar(AnsiString(DefStorageLstSql(Body,0,'logger','uxdt',DEFAULT_LIMIT))))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            while Assigned(FhRow) do
            begin
              DefStorageLst(Body,Response,0,HTTPDecode(FhRow^[1]),StrToInt(FhRow^[0]),HTTPDecode(FhRow^[2]));
              Count:=Count+1;
              mysql_data_seek(FhRes,Count);
              FhRow:=mysql_fetch_row(FhRes);
            end;
            Result:=True;
          end
          else
          begin
            Result:=True;
          end;
        end
        else
        begin
          Result:=False;
        end;
      end
      else
      begin
        Result:=False;
      end;
    end;
  end
  else
  begin
    Select:=FSqlite.GetTable(DefStorageLstSql(Body,0,'logger','uxdt',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,0,HTTPDecode(Select.FieldAsString(1)),Select.FieldAsInteger(0),HTTPDecode(Select.FieldAsString(2)));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end;
end;

{TChrome}

function TChrome.GoogleToUnix(V: UInt64): UInt64;
begin
  if V>0 then
  begin
    if (Round(V/1000000)-11644473600)>0 then Result:=Round(V/1000000)-11644473600 else Result:=0;
  end
  else
  begin
    Result:=0;
  end;
end;

function TChrome.UnixToGoogle(V: UInt64): UInt64;
begin
  Result:=(V+11644473600)*1000000;
end;

//Get current chrome path history
function TChrome.GetChromePath: AnsiString;
var Buf: array [0..255] of AnsiChar;
begin
  Result:='';
  FillChar(Buf,256,0);
  if SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,MAXDWORD,0,@Buf)=S_OK then
  begin
    Result:=AnsiString(Buf)+PathDelim+'Google\Chrome\User Data\Default\';
  end
  else
  begin
    Result:='C:\Users\'+GetCurrentLogin+'\AppData\Local\Google\Chrome\User Data\Default\';
  end;
end;

function TChrome.GetChromeVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Program Files\Google\Chrome\Application\chrome.exe');
end;

function TChrome.DoActivation: Boolean;
begin
  FChrome:=nil;
  if FileExists(GetChromePath+'History')=True then
  begin
    try
      FChrome:=TSQLiteDatabase.Create(GetChromePath+'History');
      try
        FChrome.ExecSQL('PRAGMA synchronous=0');
        FChrome.ExecSQL('PRAGMA encoding = "UTF-8"');
        FChrome.ExecSQL('PRAGMA cache_size=3000');
        FChrome.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
    if FileExists(GetChromePath+'Bookmarks')=True then
    begin
      FBookmark:=TSuperObject.ParseFile(GetChromePath+'Bookmarks',False);
    end
    else
    begin
      FBookmark:=nil;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TChrome.DoDeactivation: Boolean;
begin
  if Assigned(FChrome) then
  begin
    FChrome.Destroy;
    FChrome:=nil;
  end;
  Result:=True;
end;

function TChrome.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Google Chrome',GetChromeVersion);
end;

function TChrome.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TChrome.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var
  Select: TSQLiteTable;
  n: Integer;
  Obj: ISuperObject;
begin
  if Assigned(FChrome)=True then
  begin
    Select:=FChrome.GetTable(DefStorageLstSql(Body,1,'urls','last_visit_time',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(5),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TChrome.BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TChrome.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  if (Assigned(FBookmark)=True) and
     (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    if (Assigned(FBookmark.O['roots.bookmark_bar.children'])=True) and
       (FBookmark.A['roots.bookmark_bar.children'].Length>0) then
    begin
      for n:=0 to FBookmark.A['roots.bookmark_bar.children'].Length-1 do
      begin
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='url' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='url';
          Obj.S['link']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['url'];
          Response.A['response.array'].Add(Obj);
        end;
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='folder' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='folder';
          Obj.O['chld']:=SO('[]');
          Response.A['response.array'].Add(Obj);
        end;
      end;
    end;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

////////////////////////////////////////////////////////////////////////////////

{TEdge}

function TEdge.EdgeToUnix(V: UInt64): UInt64;
begin
  if V>0 then
  begin
    if (Round(V/1000000)-11644473600)>0 then Result:=Round(V/1000000)-11644473600 else Result:=0;
  end
  else
  begin
    Result:=0;
  end;
end;

function TEdge.UnixToEdge(V: UInt64): UInt64;
begin
  Result:=(V+11644473600)*1000000;
end;

function TEdge.GetEdgePath: AnsiString;
var Buf: array [0..255] of AnsiChar;
begin
  Result:='';
  FillChar(Buf,256,0);
  if SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,MAXDWORD,0,@Buf)=S_OK then
  begin
    Result:=AnsiString(Buf)+PathDelim+'Microsoft\Edge\User Data\Default\';
  end
  else
  begin
    Result:='C:\Users\'+GetCurrentLogin+'\AppData\Local\Microsoft\Edge\User Data\Default\';
  end;
end;

function TEdge.GetEdgeVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe');
end;

function TEdge.DoActivation: Boolean;
begin
  FEdge:=nil;
  if FileExists(GetEdgePath+'History')=True then
  begin
    try
      FEdge:=TSQLiteDatabase.Create(GetEdgePath+'History');
      try
        FEdge.ExecSQL('PRAGMA synchronous=0');
        FEdge.ExecSQL('PRAGMA encoding = "UTF-8"');
        FEdge.ExecSQL('PRAGMA cache_size=3000');
        FEdge.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
    if FileExists(GetEdgePath+'Bookmarks')=True then
    begin
      FBookmark:=TSuperObject.ParseFile(GetEdgePath+'Bookmarks',False);
    end
    else
    begin
      FBookmark:=nil;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TEdge.DoDeactivation: Boolean;
begin
  if Assigned(FEdge) then
  begin
    FEdge.Destroy;
    FEdge:=nil;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

function TEdge.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Microsoft Edge',GetEdgeVersion);
end;

function TEdge.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TEdge.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Select: TSQLiteTable;
    n: Integer;
    Obj: ISuperObject;
begin
  if Assigned(FEdge)=True then
  begin
    Select:=FEdge.GetTable(DefStorageLstSql(Body,1,'urls','last_visit_time',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(5),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TEdge.BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TEdge.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  if (Assigned(FBookmark)=True) and
     (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    if (Assigned(FBookmark.O['roots.bookmark_bar.children'])=True) and
       (FBookmark.A['roots.bookmark_bar.children'].Length>0) then
    begin
      for n:=0 to FBookmark.A['roots.bookmark_bar.children'].Length-1 do
      begin
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='url' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='url';
          Obj.S['link']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['url'];
          Response.A['response.array'].Add(Obj);
        end;
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='folder' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='folder';
          Obj.O['chld']:=SO('[]');
          Response.A['response.array'].Add(Obj);
        end;
      end;
    end;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

{TMozilla}

function TMozilla.GetMozillaPath: AnsiString;
begin
  Result:='C:\Users\'+GetCurrentLogin+'\AppData\Roaming\Mozilla\Firefox\Profiles\wlsjhf47.default-release\';
end;

function TMozilla.GetMozillaVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Program Files\Mozilla Firefox\firefox.exe');
end;

function TMozilla.DoActivation: Boolean;
begin
  FMozilla:=nil;
  FLinks:=nil;
  if FileExists(GetMozillaPath+'formhistory.sqlite')=True then
  begin
    try
      FMozilla:=TSQLiteDatabase.Create(GetMozillaPath+'formhistory.sqlite');
      try
        FMozilla.ExecSQL('PRAGMA synchronous=0');
        FMozilla.ExecSQL('PRAGMA encoding = "UTF-8"');
        FMozilla.ExecSQL('PRAGMA cache_size=3000');
        FMozilla.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      FLinks:=TSQLiteDatabase.Create(GetMozillaPath+'places.sqlite');
      try
        FLinks.ExecSQL('PRAGMA synchronous=0');
        FLinks.ExecSQL('PRAGMA encoding = "UTF-8"');
        FLinks.ExecSQL('PRAGMA cache_size=3000');
        FLinks.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TMozilla.DoDeactivation: Boolean;
begin
  if Assigned(FMozilla) then
  begin
    FMozilla.Destroy;
    FMozilla:=nil;
  end;
  if Assigned(FLinks) then
  begin
    FLinks.Destroy;
    FLinks:=nil;
  end;
  Result:=True;
end;

function TMozilla.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Mozilla firefox',GetMozillaVersion);
end;

function TMozilla.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TMozilla.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Select: TSQLiteTable;
    n: Integer;
    Obj: ISuperObject;
begin
  if Assigned(FLinks)=True then
  begin
    Select:=FLinks.GetTable(DefStorageLstSql(Body,1,'moz_places','last_visit_date',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(8),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TMozilla.BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TMozilla.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

{Opera}

function TOpera.GetOperaPath: AnsiString;
var Buf: array [0..255] of AnsiChar;
begin
  Result:='';
  FillChar(Buf,256,0);
  if SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,MAXDWORD,0,@Buf)=S_OK then
  begin
    Result:=AnsiString(Buf)+PathDelim+'Opera software\Opera stable\';
  end
  else
  begin
    Result:='C:\Users\'+GetCurrentLogin+'\AppData\Roaming\Opera software\Opera stable\';
  end;
end;

function TOpera.GetOperaVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Users\'+GetCurrentLogin+'\AppData\Local\Programs\Opera\opera.exe');
end;

function TOpera.DoActivation: Boolean;
begin
  FOpera:=nil;
  if FileExists(GetOperaPath+'History')=True then
  begin
    try
      FOpera:=TSQLiteDatabase.Create(GetOperaPath+'History');
      try
        FOpera.ExecSQL('PRAGMA synchronous=0');
        FOpera.ExecSQL('PRAGMA encoding = "UTF-8"');
        FOpera.ExecSQL('PRAGMA cache_size=3000');
        FOpera.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
    if FileExists(GetOperaPath+'Bookmarks')=True then
    begin
      FBookmark:=TSuperObject.ParseFile(GetOperaPath+'Bookmarks',False);
    end
    else
    begin
      FBookmark:=nil;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TOpera.DoDeactivation: Boolean;
begin
  if Assigned(FOpera) then
  begin
    FOpera.Destroy;
    FOpera:=nil;
  end;
end;

function TOpera.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Opera',GetOperaVersion);
end;

function TOpera.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TOpera.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var
  Select: TSQLiteTable;
  n: Integer;
  Obj: ISuperObject;
begin
  if Assigned(FOpera)=True then
  begin
    Select:=FOpera.GetTable(DefStorageLstSql(Body,1,'urls','last_visit_time',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(5),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TOpera.BookmarkAdd(Body: ISuperObject;Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TOpera.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  if (Assigned(FBookmark)=True) and
     (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    if (Assigned(FBookmark.O['roots.bookmark_bar.children'])=True) and
       (FBookmark.A['roots.bookmark_bar.children'].Length>0) then
    begin
      for n:=0 to FBookmark.A['roots.bookmark_bar.children'].Length-1 do
      begin
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='url' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='url';
          Obj.S['link']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['url'];
          Response.A['response.array'].Add(Obj);
        end;
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='folder' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='folder';
          Obj.O['chld']:=SO('[]');
          Response.A['response.array'].Add(Obj);
        end;
      end;
    end;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

{Brave}

function TBrave.GetBravePath: AnsiString;
var Buf: array [0..255] of AnsiChar;
begin
  Result:='';
  FillChar(Buf,256,0);
  if SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,MAXDWORD,0,@Buf)=S_OK then
  begin
    Result:=AnsiString(Buf)+PathDelim+'Opera software\Opera stable\';
  end
  else
  begin
    Result:='C:\Users\'+GetCurrentLogin+'\AppData\Local\BraveSoftware\Brave-Browser\User Data\Default\';
  end;
end;

function TBrave.GetBraveVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe');
end;

function TBrave.DoActivation: Boolean;
begin
  FBrave:=nil;
  if FileExists(GetBravePath+'History')=True then
  begin
    try
      FBrave:=TSQLiteDatabase.Create(GetBravePath+'History');
      try
        FBrave.ExecSQL('PRAGMA synchronous=0');
        FBrave.ExecSQL('PRAGMA encoding = "UTF-8"');
        FBrave.ExecSQL('PRAGMA cache_size=3000');
        FBrave.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
    if FileExists(GetBravePath+'Bookmarks')=True then
    begin
      FBookmark:=TSuperObject.ParseFile(GetBravePath+'Bookmarks',False);
    end
    else
    begin
      FBookmark:=nil;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TBrave.DoDeactivation: Boolean;
begin
  if Assigned(FBrave) then
  begin
    FBrave.Destroy;
    FBrave:=nil;
  end;
end;

function TBrave.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Brave',GetBraveVersion);
end;

function TBrave.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TBrave.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var
  Select: TSQLiteTable;
  n: Integer;
  Obj: ISuperObject;
begin
  if Assigned(FBrave)=True then
  begin
    Select:=FBrave.GetTable(DefStorageLstSql(Body,1,'urls','last_visit_time',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(5),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TBrave.BookmarkAdd(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TBrave.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  if (Assigned(FBookmark)=True) and
     (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    if (Assigned(FBookmark.O['roots.bookmark_bar.children'])=True) and
       (FBookmark.A['roots.bookmark_bar.children'].Length>0) then
    begin
      for n:=0 to FBookmark.A['roots.bookmark_bar.children'].Length-1 do
      begin
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='url' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='url';
          Obj.S['link']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['url'];
          Response.A['response.array'].Add(Obj);
        end;
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='folder' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='folder';
          Obj.O['chld']:=SO('[]');
          Response.A['response.array'].Add(Obj);
        end;
      end;
    end;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

{TVivaldi}

function TVivaldi.GetVivaldiPath: AnsiString;
var Buf: array [0..255] of AnsiChar;
begin
  Result:='';
  FillChar(Buf,256,0);
  if SHGetFolderPath(0,CSIDL_LOCAL_APPDATA,MAXDWORD,0,@Buf)=S_OK then
  begin
    Result:=AnsiString(Buf)+PathDelim+'Opera software\Opera stable\';
  end
  else
  begin
    Result:='C:\Users\'+GetCurrentLogin+'\AppData\Local\Vivaldi\User Data\Default\';
  end;
end;

function TVivaldi.GetVivaldiVersion: AnsiString;
begin
  Result:=GetFileVersion('C:\Users\MaxMix\AppData\Local\Vivaldi\Application\vivaldi.exe');
end;

function TVivaldi.DoActivation: Boolean;
begin
  FVivaldi:=nil;
  if FileExists(GetVivaldiPath+'History')=True then
  begin
    try
      FVivaldi:=TSQLiteDatabase.Create(GetVivaldiPath+'History');
      try
        FVivaldi.ExecSQL('PRAGMA synchronous=0');
        FVivaldi.ExecSQL('PRAGMA encoding = "UTF-8"');
        FVivaldi.ExecSQL('PRAGMA cache_size=3000');
        FVivaldi.ExecSQL('PRAGMA temp_store=MEMORY');
      except
        Result:=True;
      end;
      Result:=True;
    except
      Result:=False;
    end;
    if FileExists(GetVivaldiPath+'Bookmarks')=True then
    begin
      FBookmark:=TSuperObject.ParseFile(GetVivaldiPath+'Bookmarks',False);
    end
    else
    begin
      FBookmark:=nil;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TVivaldi.DoDeactivation: Boolean;
begin
  if Assigned(FVivaldi) then
  begin
    FVivaldi.Destroy;
    FVivaldi:=nil;
  end;
end;

function TVivaldi.BrowserInf(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=DefBrowserInf(Body,Response,'Vivaldi',GetVivaldiVersion);
end;

function TVivaldi.StorageAdd(Body: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TVivaldi.StorageLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var
  Select: TSQLiteTable;
  n: Integer;
  Obj: ISuperObject;
begin
  if Assigned(FVivaldi)=True then
  begin
    Select:=FVivaldi.GetTable(DefStorageLstSql(Body,1,'urls','last_visit_time',DEFAULT_LIMIT));
    if Assigned(Select)=True then
    begin
      if Select.Count>0 then
      begin
        Select.MoveFirst;
        for n:=0 to Select.Count-1 do
        begin
          DefStorageLst(Body,Response,1,Select.FieldAsString(1),Select.FieldAsInteger(5),Select.FieldAsString(2));
          Select.Next;
        end;
      end;
      Select.Destroy;
      Select:=nil;
      Result:=True;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=False;
  end;
end;

function TVivaldi.BookmarkAdd(Body: ISuperObject; Response: ISuperObject): Boolean;
begin
  Result:=True;
end;

function TVivaldi.BookmarkLst(Body: ISuperObject; Response: ISuperObject): Boolean;
var Obj: ISuperObject;
      n: Integer;
begin
  if (Assigned(FBookmark)=True) and
     (Assigned(Body)=True) and
     (Assigned(Response)=True) then
  begin
    if (Assigned(FBookmark.O['roots.bookmark_bar.children'])=True) and
       (FBookmark.A['roots.bookmark_bar.children'].Length>0) then
    begin
      for n:=0 to FBookmark.A['roots.bookmark_bar.children'].Length-1 do
      begin
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='url' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='url';
          Obj.S['link']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['url'];
          Response.A['response.array'].Add(Obj);
        end;
        if FBookmark.A['roots.bookmark_bar.children'].O[n].S['type']='folder' then
        begin
          Obj:=SO;
          Obj.S['name']:=FBookmark.A['roots.bookmark_bar.children'].O[n].S['name'];
          Obj.S['type']:='folder';
          Obj.O['chld']:=SO('[]');
          Response.A['response.array'].Add(Obj);
        end;
      end;
    end;
    Result:=True;
  end
  else
  begin
    Result:=False;
  end;
end;

initialization

  RegisterBrowser(TMoogle.Create);
  RegisterBrowser(TChrome.Create);
  RegisterBrowser(TEdge.Create);
  RegisterBrowser(TMozilla.Create);
  RegisterBrowser(TOpera.Create);
  RegisterBrowser(TBrave.Create);
  RegisterBrowser(TVivaldi.Create);

end.
