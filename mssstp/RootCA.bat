echo off
cls
set OPENSSL_CONF=C:\ssl\openssl.cfg


echo ----------------------------------
echo Create own Certificate Authority
echo ----------------------------------

echo 1. CreateKey
openssl genrsa -out rootCA.key 2048

echo 2. Create root certificate CA
openssl req -x509 -new -key rootCA.key -days 10000 -out rootCA.crt