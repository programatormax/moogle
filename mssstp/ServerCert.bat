echo off
cls
set OPENSSL_CONF=C:\ssl\openssl.cfg

echo ----------------------------------------------------------
echo Creaate certificate signed by Certificate Authority
echo ----------------------------------------------------------

echo 1. CreatKey
openssl genrsa -out server.key 2048

echo 2. Create query on certificate
openssl req -new -key server.key -out server.csr

echo 3. Sign certificate
openssl x509 -req -in server.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out server.crt -days 5000

