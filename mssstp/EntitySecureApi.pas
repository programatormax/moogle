unit EntitySecureApi;

interface

{$I EntitySystemOpt.inc}

uses

  EntityChannelConst,
  EntitySocketApi;

type

  HSSL = Pointer;
  HSSLMETHOD = Pointer;
  HSSLCONTEXT = Pointer;
  HX509 = Pointer;
  PX509_NAME = Pointer;
  PASN1_INTEGER = Pointer;

function SSLSend(hSSL: HSSL; Buf: Pointer; Len: Cardinal; var Error: Integer): Integer;
function SSLRecv(hSSL: HSSL; Buf: Pointer; Len: Cardinal; var Error: Integer): Integer;
function SSLConnect(hSocket: Integer; CertFile: AnsiString; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
function SSLAccept(hSocket: Integer; CertFile: AnsiString; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
function SSLDisconnect(hSocket: Integer; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
function SSLX509Cert(var SSLContext: HSSLCONTEXT; CertFile: AnsiString): Boolean;
function SSLX509Verify(var SSL: HSSL; var Cert: HX509): Boolean;
function SSLX509Subject(var Cert: HX509): AnsiString;
function SSLX509Issuer(var Cert: HX509): AnsiString;

implementation

const

  SSLEAY32_LIBRARY   = 'ssleay32.dll';
  LIBEAY32_LIBRARY   = 'libeay32.dll';

  
  SSL_SSL_ERROR_WANT_READ  = 2;
  SSL_SSL_ERROR_WANT_WRITE = 3;

  SSL_VERIFY_NONE = 0;
  SSL_VERIFY_PEER	= 1;
  SSL_VERIFY_FAIL_IF_NO_PEER_CERT	= 2;
  SSL_VERIFY_CLIENT_ONCE = 4;
  SSL_FILETYPE_ASN1	= 2;
  SSL_FILETYPE_PEM = 1;
  X509_V_OK = 0;

function SSL_library_init: Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_get_verify_result(SSL: HSSL): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_load_error_strings: Integer; cdecl; external SSLEAY32_LIBRARY;
function SSLv2_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSLv23_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSLv3_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSLv2_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSLv23_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSLv3_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLS_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_1_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_2_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLS_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_1_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function TLSv1_2_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLS_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLSv1_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLSv1_2_client_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLS_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLSv1_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function DTLSv1_2_server_method: HSSLMETHOD; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_new(Method: HSSLMETHOD): HSSLCONTEXT; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_free(Context: HSSLCONTEXT): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_set_verify(Context: HSSLCONTEXT; Mode: Integer; verify_callback: Pointer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_set_verify_depth(Context: HSSLCONTEXT; depth: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_use_certificate_file(Context: HSSLCONTEXT; CertFile: PAnsiChar; FileType: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_use_PrivateKey_file(Context: HSSLCONTEXT; KeyFile: PAnsiChar; FileType: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_check_private_key(Context: HSSLCONTEXT): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_CTX_load_verify_locations(Context: HSSLCONTEXT; CAFile: PAnsiChar; CAPath: PAnsiChar): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_get_peer_certificate(SSL: HSSL): HX509 cdecl; external SSLEAY32_LIBRARY;
function SSL_shutdown(SSL: HSSL): Integer cdecl; external SSLEAY32_LIBRARY;
function SSL_accept(SSL: HSSL): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_new(Context: HSSLCONTEXT): HSSL; cdecl; external SSLEAY32_LIBRARY;
function SSL_set_fd(SSL: HSSL; hSocket: Integer): Integer cdecl; external SSLEAY32_LIBRARY;
function SSL_connect(SSL: HSSL): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_free(SSL: HSSL): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_read(SSL: HSSL; Buf: Pointer; Len: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_write(SSL: HSSL; Buf: Pointer; Len: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function SSL_get_error(SSL: HSSL; RetCode: Integer): Integer; cdecl; external SSLEAY32_LIBRARY;
function X509_get_subject_name(X509: HX509): PX509_NAME; cdecl; external LIBEAY32_LIBRARY;
function X509_NAME_oneline(Name: PX509_NAME; buf: PAnsiChar; Size: Integer):PAnsiChar; cdecl; external LIBEAY32_LIBRARY;
function X509_get_issuer_name(X509: HX509): PX509_NAME; cdecl; external LIBEAY32_LIBRARY;

function SSLX509Cert(var SSLContext: HSSLCONTEXT; CertFile: AnsiString): Boolean;
begin
  Result:=False;
  if Assigned(SSLContext)=True then
  begin
    if CertFile<>'' then
    begin
      if SSL_CTX_use_certificate_file(SSLContext,PAnsiChar(CertFile),SSL_FILETYPE_PEM)=1 then
      begin
        if SSL_CTX_use_PrivateKey_file(SSLContext,PAnsiChar(CertFile),SSL_FILETYPE_PEM)=1 then
        begin
          if SSL_CTX_check_private_key(SSLContext)=1 then
          begin
            if SSL_CTX_load_verify_locations(SSLContext,PAnsiChar(CertFile),nil)=1 then
            begin
              SSL_CTX_set_verify(SSLContext,SSL_VERIFY_PEER,nil);
              SSL_CTX_set_verify_depth(SSLContext,1);
              Result:=True;
            end;
          end;
        end;
      end;
    end
    else
    begin
      Result:=True;
    end;
  end;
end;

function SSLConnect(hSocket: Integer; CertFile: AnsiString; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
var Error: Integer;
begin
  Result:=False;
  if ESASocketBlocking(hSocket,Error)=True then
  begin
    if SSL_library_init=1 then
    begin
      SSLMethod:=TLSv1_2_client_method;
      if Assigned(SSLMethod)=True then
      begin
        SSLContext:=SSL_CTX_new(SSLMethod);
        if Assigned(SSLContext)=True then
        begin
          if SSLX509Cert(SSLContext,CertFile)=True then
          begin
            SSL:=SSL_new(SSLContext);
            if Assigned(SSL)=True then
            begin
              if SSL_set_fd(SSL, hSocket)=1 then
              begin
                if SSL_connect(SSL)=1 then
                begin
                  Cert:=SSL_get_peer_certificate(SSL);
                  if ESASocketUnblocking(hSocket,Error)=True then
                  begin
                    Result:=True;
                    Exit;
                  end;
                  Cert:=nil;
                end;
              end;
              SSL_free(SSL);
              SSL:=nil;
            end;
          end;
          SSLContext:=nil;
        end;
        SSLMethod:=nil;
      end;
    end;
  end;
end;

function SSLAccept(hSocket: Integer; CertFile: AnsiString; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
var Error: Integer;
begin
  Result:=False;
  if ESASocketBlocking(hSocket,Error)=True then
  begin
    if SSL_library_init=1 then
    begin
      SSLMethod:=TLSv1_2_server_method;
      if Assigned(SSLMethod)=True then
      begin
        SSLContext:=SSL_CTX_new(SSLMethod);
        if Assigned(SSLContext)=True then
        begin
          if SSLX509Cert(SSLContext,CertFile)=True then
          begin
            SSL:=SSL_new(SSLContext);
            if Assigned(SSL)=True then
            begin
              if SSL_set_fd(SSL,hSocket)=1 then
              begin
                if SSL_accept(SSL)=1 then
                begin
                  Cert:=SSL_get_peer_certificate(SSL);
                  if ESASocketUnblocking(hSocket,Error)=True then
                  begin
                    Result:=True;
                    Exit;
                  end;
                  Cert:=nil;
                end;
              end;
              SSL_free(SSL);
              SSL:=nil;
            end;
          end;
          SSL_CTX_free(SSLContext);
          SSLContext:=nil;
        end;
        SSLMethod:=nil;
      end;
    end;
  end;
end;

function SSLDisconnect(hSocket: Integer; var SSLMethod: HSSLMETHOD; var SSLContext: HSSLCONTEXT; var SSL: HSSL; var Cert: HX509): Boolean;
var Error: Integer;
begin
  Result:=False;
  if ESASocketBlocking(hSocket,Error)=True then
  begin
    SSL_Shutdown(SSL);
    SSL_free(SSL);
    SSL:=nil;
    SSL_CTX_free(SSLContext);
    SSLContext:=nil;
    SSLMethod:=nil;
    Cert:=nil;
    Result:=True;
  end;
end;

function SSLX509Verify(var SSL: HSSL; var Cert: HX509): Boolean;
begin
  if (Assigned(SSL)=True) and (Assigned(Cert)=True) and (SSL_get_verify_result(SSL)=X509_V_OK) then Result:=True else Result:=False;
end;

function SSLX509Subject(var Cert: HX509): AnsiString;
begin
  if Assigned(Cert)=True then
  begin
    SetLength(Result,4096);
    Result:=X509_NAME_oneline(X509_get_subject_name(Cert),PAnsiChar(Result),Length(Result));
  end
  else
  begin
    Result:='';
  end;
end;

function SSLX509Issuer(var Cert: HX509): AnsiString;
begin
  if Assigned(Cert)=True then
  begin
    SetLength(Result,4096);
    Result:=X509_NAME_oneline(X509_get_issuer_name(Cert),PAnsiChar(Result),Length(Result));
  end
  else
  begin
    Result:='';
  end;
end;

function SSLSend(hSSL: HSSL; Buf: Pointer; Len: Cardinal; var Error: Integer): Integer;
begin
  Result:=SSL_write(hSSL,Buf,Len);
  Error:=SSL_get_error(hSSL,Result);
  if Result=CHANNEL_ERROR_LINK then
  begin
    if Error=SSL_SSL_ERROR_WANT_WRITE then
    begin
      Result:=CHANNEL_ERROR_NONE;
    end
    else
    begin
      Result:=CHANNEL_ERROR_LINK;
    end;
  end;
end;

function SSLRecv(hSSL: HSSL; Buf: Pointer; Len: Cardinal; var Error: Integer): Integer;
begin
  Result:=SSL_read(hSSL,Buf,Len);
  Error:=SSL_get_error(hSSL,Result);
  if Result=CHANNEL_ERROR_LINK then
  begin
    if Error=SSL_SSL_ERROR_WANT_READ then
    begin
      Result:=CHANNEL_ERROR_NONE;
    end
    else
    begin
      Result:=CHANNEL_ERROR_LINK;
    end;
  end;
end;

end.
