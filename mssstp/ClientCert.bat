echo off
cls
set OPENSSL_CONF=C:\ssl\openssl.cfg

echo ---------------------------------------
echo Create client certificate
echo ---------------------------------------

echo 1. Create key
openssl genrsa -out client.key 2048

echo 2. Create query on certificate
openssl req -new -key client.key -out client.csr

echo 3. Sign request
openssl x509 -req -in client.csr -CA server.crt -CAkey server.key -CAcreateserial -out client.crt -days 5000

echo 4. Receive public key
openssl rsa -in client.key -pubout > client.pub





