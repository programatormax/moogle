unit SearchSortUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.CheckLst;

type
  TForm3 = class(TForm)
    CheckListBox1: TCheckListBox;
    Edit1: TEdit;
    procedure CheckListBox1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure CheckListBox1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.CheckListBox1DblClick(Sender: TObject);
var i1,i2: Integer;
    c1,c2: String;
    v1,v2: Boolean;
begin
  if CheckListBox1.ItemIndex>0 then
  begin
    i1:=CheckListBox1.ItemIndex;
    c1:=CheckListBox1.Items.Strings[i1];
    v1:=CheckListBox1.Checked[i1];

    i2:=CheckListBox1.ItemIndex-1;
    c2:=CheckListBox1.Items.Strings[i2];
    v2:=CheckListBox1.Checked[i2];

    CheckListBox1.Items.Strings[i1]:=c2;
    CheckListBox1.Checked[i1]:=v2;
    CheckListBox1.Items.Strings[i2]:=c1;
    CheckListBox1.Checked[i2]:=v1;
  end
  else
  begin
    if CheckListBox1.ItemIndex=0 then
    begin
      i1:=CheckListBox1.ItemIndex;
      c1:=CheckListBox1.Items.Strings[i1];
      v1:=CheckListBox1.Checked[i1];
      i2:=CheckListBox1.Items.Count-1;
      c2:=CheckListBox1.Items.Strings[i2];
      v2:=CheckListBox1.Checked[i2];
      CheckListBox1.Items.Strings[i1]:=c2;
      CheckListBox1.Checked[i1]:=v2;
      CheckListBox1.Items.Strings[i2]:=c1;
      CheckListBox1.Checked[i2]:=v1;
    end;
  end;
end;

procedure TForm3.CheckListBox1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=True;
end;

end.
