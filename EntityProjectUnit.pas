unit EntityProjectUnit;

interface

{$ifdef fpc}
  {$mode delphi}
  {$H+}
  {$codepage cp1252}
{$endif}

uses

  Windows,
  ShellApi,
  Registry,
  SysUtils,
  Classes,
  DateUtils,
  SuperObject,
  RestApiClient,
  Math;

const

  REST_API_SERVER = 'https://api.moogle.com/license/';

type

  TCore = class
  private
    function GetCoreGuid: AnsiString;
    function GetCoreType: AnsiString;
    function GetCoreCapacity: AnsiString;
    function GetCorePath: AnsiString;
    function GetCoreName: AnsiString;
    function GetCoreBuild: AnsiString;
    function GetCoreLogin: AnsiString;
    function GetCoreAdmin: Boolean;
    function GetCoreInstall: UInt64;
    function GetCoreStart: UInt64;
  public
    property Guid: AnsiString read GetCoreGuid;
    property Family: AnsiString read GetCoreType;
    property Capacity: AnsiString read GetCoreCapacity;
    property Path: AnsiString read GetCorePath;
    property Name: AnsiString read GetCoreName;
    property Build: AnsiString read GetCoreBuild;
    property Install: UInt64 read GetCoreInstall;
    property Start: UInt64 read GetCoreStart;
    property Login: AnsiString read GetCoreLogin;
    property Admin: Boolean read GetCoreAdmin;
    constructor Create;
    destructor Destroy; override;
  end;

  TProject = class
  private
    FGuid: AnsiString;
    FSystem: AnsiString;
    FCapacity: AnsiString;
    FPath: AnsiString;
    FName: AnsiString;
    FBuild: AnsiString;
    FSize: UInt64;
    FInstall: UInt64;
    FStart: UInt64;
    FCommand: TStringList;
    FVendor: AnsiString;
    FCopyright: AnsiString;
    FVersion: AnsiString;
    FAvailable: Boolean;
    FIssueDate: AnsiString;
    FExpiryDate: AnsiString;
    procedure Reset;
  protected
    function GetFileCRC32(p: Pointer; ByteCount: LongWord; var CRCValue: LongWord): Boolean;
    function GetProjectGuid: AnsiString;
    function GetProjectSystem: AnsiString;
    function GetProjectCapacity: AnsiString;
    function GetProjectPath: AnsiString;
    function GetProjectName: AnsiString;
    function GetProjectBuild: AnsiString;
    function GetProjectSize: UInt64;
    function GetProjectChecksum: LongWord;
    function GetProjectCRC32: LongWord;
    function GetProjectInstall: UInt64;
    function GetProjectStart: UInt64;
    function GetProjectCommand: TStringList;
    function GetProjectVendor: AnsiString;
    function GetProjectCopyright: AnsiString;
    function GetProjectMajor: AnsiString;
    function GetProjectVersion: AnsiString;
    function GetFileVersion(const sgFileName : string ) : string;
    function GetLicenseObject: ISuperObject;
    function GetProjectAvailable: Boolean;
    function GetProjectIssue: AnsiString;
    function GetProjectExpiry: AnsiString;
  public
    property Guid: AnsiString read FGuid;
    property System: AnsiString read FSystem;
    property Capacity: AnsiString read FCapacity;
    property Path: AnsiString read FPath;
    property Name: AnsiString read FName;
    property Version: AnsiString read FVersion;
    property Build: AnsiString read FBuild;
    property Size: UInt64 read FSize;
    property Install: UInt64 read FInstall;
    property Start: UInt64 read FStart;
    property Command: TStringList read FCommand;
    property Vendor: AnsiString read FVendor;
    property Copyright: AnsiString read FCopyright;
    property Available: Boolean read FAvailable;
    property IssueDate: AnsiString read FIssueDate;
    property ExpiryDate: AnsiString read FExpiryDate;
    constructor Create;
    destructor Destroy; override;
  end;

var

  Core: TCore;
  Project: TProject;


implementation

const

  Table: array[0..255] of LongWord =
    ($00000000, $77073096, $EE0E612C, $990951BA,
    $076DC419, $706AF48F, $E963A535, $9E6495A3,
    $0EDB8832, $79DCB8A4, $E0D5E91E, $97D2D988,
    $09B64C2B, $7EB17CBD, $E7B82D07, $90BF1D91,
    $1DB71064, $6AB020F2, $F3B97148, $84BE41DE,
    $1ADAD47D, $6DDDE4EB, $F4D4B551, $83D385C7,
    $136C9856, $646BA8C0, $FD62F97A, $8A65C9EC,
    $14015C4F, $63066CD9, $FA0F3D63, $8D080DF5,
    $3B6E20C8, $4C69105E, $D56041E4, $A2677172,
    $3C03E4D1, $4B04D447, $D20D85FD, $A50AB56B,
    $35B5A8FA, $42B2986C, $DBBBC9D6, $ACBCF940,
    $32D86CE3, $45DF5C75, $DCD60DCF, $ABD13D59,
    $26D930AC, $51DE003A, $C8D75180, $BFD06116,
    $21B4F4B5, $56B3C423, $CFBA9599, $B8BDA50F,
    $2802B89E, $5F058808, $C60CD9B2, $B10BE924,
    $2F6F7C87, $58684C11, $C1611DAB, $B6662D3D,

    $76DC4190, $01DB7106, $98D220BC, $EFD5102A,
    $71B18589, $06B6B51F, $9FBFE4A5, $E8B8D433,
    $7807C9A2, $0F00F934, $9609A88E, $E10E9818,
    $7F6A0DBB, $086D3D2D, $91646C97, $E6635C01,
    $6B6B51F4, $1C6C6162, $856530D8, $F262004E,
    $6C0695ED, $1B01A57B, $8208F4C1, $F50FC457,
    $65B0D9C6, $12B7E950, $8BBEB8EA, $FCB9887C,
    $62DD1DDF, $15DA2D49, $8CD37CF3, $FBD44C65,
    $4DB26158, $3AB551CE, $A3BC0074, $D4BB30E2,
    $4ADFA541, $3DD895D7, $A4D1C46D, $D3D6F4FB,
    $4369E96A, $346ED9FC, $AD678846, $DA60B8D0,
    $44042D73, $33031DE5, $AA0A4C5F, $DD0D7CC9,
    $5005713C, $270241AA, $BE0B1010, $C90C2086,
    $5768B525, $206F85B3, $B966D409, $CE61E49F,
    $5EDEF90E, $29D9C998, $B0D09822, $C7D7A8B4,
    $59B33D17, $2EB40D81, $B7BD5C3B, $C0BA6CAD,

    $EDB88320, $9ABFB3B6, $03B6E20C, $74B1D29A,
    $EAD54739, $9DD277AF, $04DB2615, $73DC1683,
    $E3630B12, $94643B84, $0D6D6A3E, $7A6A5AA8,
    $E40ECF0B, $9309FF9D, $0A00AE27, $7D079EB1,
    $F00F9344, $8708A3D2, $1E01F268, $6906C2FE,
    $F762575D, $806567CB, $196C3671, $6E6B06E7,
    $FED41B76, $89D32BE0, $10DA7A5A, $67DD4ACC,
    $F9B9DF6F, $8EBEEFF9, $17B7BE43, $60B08ED5,
    $D6D6A3E8, $A1D1937E, $38D8C2C4, $4FDFF252,
    $D1BB67F1, $A6BC5767, $3FB506DD, $48B2364B,
    $D80D2BDA, $AF0A1B4C, $36034AF6, $41047A60,
    $DF60EFC3, $A867DF55, $316E8EEF, $4669BE79,
    $CB61B38C, $BC66831A, $256FD2A0, $5268E236,
    $CC0C7795, $BB0B4703, $220216B9, $5505262F,
    $C5BA3BBE, $B2BD0B28, $2BB45A92, $5CB36A04,
    $C2D7FFA7, $B5D0CF31, $2CD99E8B, $5BDEAE1D,

    $9B64C2B0, $EC63F226, $756AA39C, $026D930A,
    $9C0906A9, $EB0E363F, $72076785, $05005713,
    $95BF4A82, $E2B87A14, $7BB12BAE, $0CB61B38,
    $92D28E9B, $E5D5BE0D, $7CDCEFB7, $0BDBDF21,
    $86D3D2D4, $F1D4E242, $68DDB3F8, $1FDA836E,
    $81BE16CD, $F6B9265B, $6FB077E1, $18B74777,
    $88085AE6, $FF0F6A70, $66063BCA, $11010B5C,
    $8F659EFF, $F862AE69, $616BFFD3, $166CCF45,
    $A00AE278, $D70DD2EE, $4E048354, $3903B3C2,
    $A7672661, $D06016F7, $4969474D, $3E6E77DB,
    $AED16A4A, $D9D65ADC, $40DF0B66, $37D83BF0,
    $A9BCAE53, $DEBB9EC5, $47B2CF7F, $30B5FFE9,
    $BDBDF21C, $CABAC28A, $53B39330, $24B4A3A6,
    $BAD03605, $CDD70693, $54DE5729, $23D967BF,
    $B3667A2E, $C4614AB8, $5D681B02, $2A6F2B94,
    $B40BBE37, $C30C8EA1, $5A05DF1B, $2D02EF8D);

function IsUserAnAdmin: BOOL; external shell32;

//������� �����
constructor TCore.Create;
begin
  inherited Create;
  //Reset;
end;

//���������� �����
destructor TCore.Destroy;
begin
  //Reset;
  inherited Destroy;
end;

//�������� ������������� ��
function TCore.GetCoreGuid: AnsiString;
var RegEdit: TRegistry;
begin
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.OpenKeyReadOnly('Software\Microsoft\Windows NT\CurrentVersion')=True then
  begin
    Result:='{'+AnsiUpperCase(Trim(RegEdit.ReadString('BuildGUID')))+'}';
  end
  else
  begin
    Result:='';
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//�������� ��
function TCore.GetCoreType: AnsiString;
begin
{$ifdef mswindows}
  Result:='Windows';
{$else}
  Result:='Linux';
{$endif}
end;

//�������� ��������� ����
function TCore.GetCorePath: AnsiString;
var Len: Cardinal;
    Buf: array [0..255] of AnsiChar;
begin
  FillChar(Buf,256,0);
  Len:=GetWindowsDirectory(@Buf,256);
  if Len>0 then
  begin
    SetString(Result,Buf,Len);
    Result:=Result+'\';
  end
  else
  begin
    Result:='';
  end;
end;

//�������� ������������ ��
function TCore.GetCoreCapacity: AnsiString;
var RegEdit: TRegistry;
begin
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.OpenKeyReadOnly('Software\Microsoft\Windows NT\CurrentVersion')=True then
  begin
    if pos('amd64',RegEdit.ReadString('BuildLabEx'))>0 then
    begin
      Result:='x64';
    end
    else
    begin
      Result:='x32';
    end;
  end
  else
  begin
    Result:='x64';
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//�������� ���
function TCore.GetCoreName: AnsiString;
var RegEdit: TRegistry;
begin
  Result:='';
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.OpenKeyReadOnly('Software\Microsoft\Windows NT\CurrentVersion')=True then
  begin
    if RegEdit.ValueExists('ProductName')=True then Result:=RegEdit.ReadString('ProductName')+' '+GetCoreCapacity;
    if RegEdit.ValueExists('CSDVersion')=True then Result:=Result+' '+RegEdit.ReadString('CSDVersion');
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//�������� ������ ��
function TCore.GetCoreBuild: AnsiString;
var RegEdit: TRegistry;
begin
  Result:='';
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.OpenKeyReadOnly('Software\Microsoft\Windows NT\CurrentVersion')=True then
  begin
    if RegEdit.ValueExists('BuildLabEx')=True then
    begin
      Result:=RegEdit.ReadString('BuildLabEx');
    end
    else
    begin
      if RegEdit.ValueExists('BuildLab')=True then
      begin
        Result:=RegEdit.ReadString('BuildLab');
      end;
    end;
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//�������� ������������
function TCore.GetCoreLogin: AnsiString;
var Len: Cardinal;
    Buf: array [0..255] of AnsiChar;
begin
  Len:=256;
  FillChar(Buf,256,0);
  if GetUserNameA(@Buf,Len)=True then
  begin
    SetString(Result,Buf,Len-1);
  end
  else
  begin
    Result:='';
  end;
end;

//������ � ������� ������
function TCore.GetCoreAdmin: Boolean;
begin
  Result:=IsUserAnAdmin;
end;

//����� ���������
function TCore.GetCoreInstall: UInt64;
var RegEdit: TRegistry;
begin
  Result:=0;
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.OpenKeyReadOnly('Software\Microsoft\Windows NT\CurrentVersion')=True then
  begin
    if RegEdit.ValueExists('InstallDate')=True then Result:=RegEdit.ReadInteger('InstallDate');
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//����� �������
function TCore.GetCoreStart: UInt64;
var SysTime: TSystemTime;
begin
  DateTimeToSystemTime(UnixToDateTime(DateTimeToUnix(Now)-Ceil(GetTickCount64/1000)),SysTime);
  Result:=DateTimeToUnix(EncodeDateTime(SysTime.wYear,SysTime.wMonth,SysTime.wDay,SysTime.wHour,SysTime.wMinute,0,0));
end;

{TProject}

//������� �����
constructor TProject.Create;
begin
  inherited Create;
  Reset;
  FGuid:=GetProjectGuid;
  FSystem:=GetProjectSystem;
  FCapacity:=GetProjectCapacity;
  FPath:=GetProjectPath;
  FName:=GetProjectName;
  FBuild:=GetProjectBuild;
  FSize:=GetProjectSize;
  FInstall:=GetProjectInstall;
  FStart:=GetProjectStart;
  FCommand:=GetProjectCommand;
  FVendor:=GetProjectVendor;
  FCopyright:=GetProjectCopyright;
  FVersion:=GetProjectVersion;
  FAvailable:=GetProjectAvailable;
  FIssueDate:=GetProjectIssue;
  FExpiryDate:=GetProjectExpiry;
end;

//���������� �����
destructor TProject.Destroy;
begin
  FCommand.Destroy;
  Reset;
  inherited Destroy;
end;

//�����
procedure TProject.Reset;
begin
  FGuid:='';
  FSystem:='';
  FCapacity:='';
  FPath:='';
  FName:='';
  FBuild:='';
  FSize:=0;
  FInstall:=0;
  FStart:=0;
  FCommand:=nil;
  FVendor:='';
  FCopyright:='';
  FAvailable:=False;
  FIssueDate:='';
  FExpiryDate:='';
end;

//�������� ����������� ����� �����
function TProject.GetFileCRC32(p: Pointer; ByteCount: LongWord; var CRCValue: LongWord): Boolean;
var
  i:  LongWord;
  q: ^BYTE;
begin
  Result:=False;
  if (Assigned(p)=True) and (ByteCount>0) then
  begin
    CRCValue:=$FFFFFFFF;
    q := p;
    for i := 0 to ByteCount - 1 do
    begin
      CRCvalue := (CRCvalue shr 8) xor Table[q^ xor (CRCvalue and $000000FF)];
      Inc(q);
    end;
    Result:=True;
  end;
end;

//�������� guid �����
function TProject.GetProjectGuid: AnsiString;
var RegEdit: TRegistry;
    ProductId: TGUID;
begin
  RegEdit:=Registry.TRegistry.Create;
  RegEdit.RootKey:=HKEY_LOCAL_MACHINE;
  if RegEdit.KeyExists('Software\Moogle')=True then
  begin
    if RegEdit.OpenKeyReadOnly('Software\Moogle')=True then
    begin
      Result:=RegEdit.ReadString('ProductID');
    end;
  end
  else
  begin
    if RegEdit.OpenKey('Software\Moogle',True)=True then
    begin
      RegEdit.WriteString('ProductID',GUIDToString(ProductId));
      Result:=RegEdit.ReadString('ProductID');
    end
    else
    begin
      Result:='{00000000-0000-0000-0000-000000000000}';
    end;
  end;
  RegEdit.Destroy;
  RegEdit:=nil;
end;

//�������� ��� �����
function TProject.GetProjectSystem: AnsiString;
begin
{$ifdef mswindows}
  Result:='Windows';
{$else}
  Result:='Linux';
{$endif}
end;

//�������� ����������� �����
function TProject.GetProjectCapacity: AnsiString;
begin
{$ifdef fpc}
  {$ifdef cpu64}
    Result:='x64';
  {$else}
    Result:='x32';
  {$endif}
{$else}
  {$ifdef win64}
    Result:='x64';
  {$else}
    Result:='x32';
  {$endif}
{$endif}
end;

//�������� ���� � �����
function TProject.GetProjectPath: AnsiString;
begin
  Result:=ExtractFilePath(ParamStr(0));
end;

//�������� ��� �����
function TProject.GetProjectName: AnsiString;
begin
  Result:=ExtractFileName(ParamStr(0));
end;

//�������� ������ �����
function TProject.GetProjectSize: UInt64;
var hFile: Int64;
begin
  hFile:=FileOpen(GetProjectPath+GetProjectName,fmOpenRead+fmShareDenyWrite);
  if hFile<>-1 then
  begin
    Result:=FileSeek(hFile,0,2);
    FileClose(hFile);
  end
  else
  begin
    Result:=0;
  end;
end;

//�������� ����������� ����� �����
function TProject.GetProjectChecksum: LongWord;
var hFile: Int64;
    hSize: Int64;
    hBuff: array of Byte;
    n: UInt64;
begin
  Result:=0;
  hFile:=FileOpen(GetProjectPath+GetProjectName,fmOpenRead+fmShareDenyWrite);
  if hFile>0 then
  begin
    hSize:=FileSeek(hFile,0,2);
    FileSeek(hFile,0,0);
    if hSize>0 then
    begin
      SetLength(hBuff,hSize);
      if FileRead(hFile,hBuff[0],hSize)=hSize then
      begin
        for n:=Low(hBuff) to High(hBuff) do
        begin
          Result:=Result+hBuff[n];
        end;
      end;
      hBuff:=nil;
    end;
    FileClose(hFile);
  end;
end;

//�������� ������ �����
function TProject.GetProjectCRC32: LongWord;
var hFile: Int64;
    hSize: Int64;
    hBuff: array of Byte;
    hCRCV: LongWord;
begin
  hCRCV:=0;
  hFile:=FileOpen(GetProjectPath+GetProjectName,fmOpenRead+fmShareDenyWrite);
  if hFile>0 then
  begin
    hSize:=FileSeek(hFile,0,2);
    FileSeek(hFile,0,0);
    if hSize>0 then
    begin
      SetLength(hBuff,hSize);
      if FileRead(hFile,hBuff[0],hSize)=hSize then GetFileCRC32(@hBuff[0],hSize,hCRCV);
      hBuff:=nil;
    end;
    FileClose(hFile);
  end;
  Result:=hCRCV;
end;

//�������� ������ �����
function TProject.GetProjectBuild: AnsiString;
var Sum: LongWord;
begin
  Sum:=GetProjectChecksum;
  Result:=IntToStr(PWordArray(@Sum)[1])+'.';
  Result:=Result+IntToStr(PWordArray(@Sum)[0])+'.';
  Result:=Result+Copy(AnsiUpperCase(GetProjectName),1,2);
  Result:=Result+Copy(AnsiUpperCase(GetProjectSystem),1,1);
  Result:=Result+GetProjectCapacity+'.';
  Result:=Result+IntToHex(GetProjectCRC32,8);
end;

//�������� ������������� �����
function TProject.GetProjectVendor: AnsiString;
begin
  Result:='Thomas Chai Limited';
end;

//�������� ������ ���������
function TProject.GetProjectCopyright: AnsiString;
begin
  Result:='Copyright '+GetProjectVendor+'. All rights reserved '+GetProjectMajor;
end;

//�������� ����� ������ ����������
function TProject.GetProjectInstall: UInt64;
begin
  Result:=DateTimeToUnix(FileDateToDateTime(FileAge(GetProjectPath+GetProjectName)));
end;

//�������� ����� ������� �������
function TProject.GetProjectStart: UInt64;
begin
  Result:=DateTimeToUnix(Now);
end;

//�������� ��������� ������
function TProject.GetProjectCommand: TStringList;
var n: Cardinal;
begin
  Result:=TStringList.Create;
  if ParamCount>0 then
  begin
    for n:=1 to ParamCount do
    begin
      Result.Add(Trim(ParamStr(n)));
    end;
  end;
end;

//�������� ��� ���������� �����
function TProject.GetProjectMajor: AnsiString;
begin
  Result:=IntToStr(YearOf(FileDateToDateTime(FileAge(GetProjectPath+GetProjectName))));
end;

function TProject.GetFileVersion( const sgFileName : string ) : string;
var infoSize: DWORD;
var verBuf:   pointer;
var verSize:  UINT;
var wnd:      UINT;
var FixedFileInfo : PVSFixedFileInfo;
begin
  infoSize := GetFileVersioninfoSize(PChar(GetProjectPath+sgFileName), wnd);
  result := '(Unknown)';
  if infoSize <> 0 then
  begin
    GetMem(verBuf, infoSize);
    try
      if GetFileVersionInfo(PChar(sgFileName), wnd, infoSize, verBuf) then
      begin
        VerQueryValue(verBuf, '\', Pointer(FixedFileInfo), verSize);

        result := IntToStr(FixedFileInfo.dwFileVersionMS div $10000) + '.' +
                  IntToStr(FixedFileInfo.dwFileVersionMS and $0FFFF) + '.' +
                  IntToStr(FixedFileInfo.dwFileVersionLS div $10000) + '.' +
                  IntToStr(FixedFileInfo.dwFileVersionLS and $0FFFF);
      end;
    finally
      FreeMem(verBuf);
    end;
  end;
end;

function TProject.GetLicenseObject: ISuperObject;
var L: UInt64;
begin
  {
  //just for test
  L:=DateTimeToUnix(Now);
  Result:=SO;
  Result.S['guid']:=FGuid;
  Result.I['issue']:=L;
  Result.I['expiry']:=L+3600;
  Result.I['current']:=L+1;
  }
  Result:=RestApiGet('http://127.0.0.1/'+FGuid+'.key');
end;

function TProject.GetProjectAvailable: Boolean;
var Obj: ISuperObject;
begin
  Result:=False;
  Obj:=GetLicenseObject;
  if (Assigned(Obj)=True) and
     (Assigned(Obj.O['issue'])=True) and
     (Obj.I['issue']<>0) and
     (Obj.I['issue']<>-1) and
     (Assigned(Obj.O['expiry'])=True) and
     (Obj.I['expiry']<>0) and
     (Obj.I['expiry']<>-1) and
     (Assigned(Obj.O['current'])=True) and
     (Obj.I['current']<>0) and
     (Obj.I['current']<>-1) then
  begin
    if (Obj.I['current']>Obj.I['issue']) and (Obj.I['current']<Obj.I['expiry']) then Result:=True;
  end;
end;

function TProject.GetProjectIssue: AnsiString;
var Obj: ISuperObject;
begin
  Obj:=GetLicenseObject;
  if (Assigned(Obj)=True) and
     (Assigned(Obj.O['issue'])=True) and
     (Obj.I['issue']<>0) and
     (Obj.I['issue']<>-1) then
  begin
    Result:=DateTimeToStr(UnixToDateTime(Obj.I['issue']));
  end
  else
  begin
    Result:='Not available';
  end;
end;

function TProject.GetProjectExpiry: AnsiString;
var Obj: ISuperObject;
begin
  Obj:=GetLicenseObject;
  if (Assigned(Obj)=True) and
     (Assigned(Obj.O['expiry'])=True) and
     (Obj.I['expiry']<>0) and
     (Obj.I['expiry']<>-1) then
  begin
    Result:=DateTimeToStr(UnixToDateTime(Obj.I['expiry']));
  end
  else
  begin
    Result:='Not available';
  end;
end;


function TProject.GetProjectVersion: AnsiString;
begin
  Result:=GetFileVersion('Browser.exe');
end;

initialization

  Core:=TCore.Create;
  Project:=TProject.Create;

finalization

  FreeAndNil(Project);
  FreeAndNil(Core);

end.
