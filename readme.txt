--------------------------------------------------
           HOW TO INSTALL SOURCES
--------------------------------------------------
0. Clone https://gitlab.com/programatormax/moogle.git
1. Download and install Installer Maker (innosetup-6.1.2.exe) from https://jrsoftware.org/isdl.php#stable
2. Open [packages] directory and install CEF4Delph.dpk (You must to use Delphi IDE)
3. Build CEF4Delph.dpk (You must to use Delphi IDE)
4. Install CEF4Delph.dpk (You must to use Delphi IDE)
5. Open Delphi IDE and start project Browser


--------------------------------------------------
             HOW TO INSTALL TAO
--------------------------------------------------
0. The Fist of all Install OpenVPN-2.5.5-I602-amd64.msi
1. And then Install TAOBrowserInstall.msi
2. Enjoy 