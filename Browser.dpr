// ************************************************************************
// ***************************** CEF4Delphi *******************************
// ************************************************************************
//
// CEF4Delphi is based on DCEF3 which uses CEF3 to embed a chromium-based
// browser in Delphi applications.
//
// The original license of DCEF3 still applies to CEF4Delphi.
//
// For more information about CEF4Delphi visit :
//         https://www.briskbard.com/index.php?lang=en&pageid=cef
//
//        Copyright � 2018 Salvador D�az Fau. All rights reserved.
//
// ************************************************************************
// ************ vvvv Original license and comments below vvvv *************
// ************************************************************************
(*
 *                       Delphi Chromium Embedded 3
 *
 * Usage allowed under the restrictions of the Lesser GNU General Public License
 * or alternatively the restrictions of the Mozilla Public License 1.1
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * Unit owner : Henri Gourvest <hgourvest@gmail.com>
 * Web site   : http://www.progdigy.com
 * Repository : http://code.google.com/p/delphichromiumembedded/
 * Group      : http://groups.google.com/group/delphichromiumembedded
 *
 * Embarcadero Technologies, Inc is not permitted to use or redistribute
 * this source code without explicit permission.
 *
 *)

program Browser;

{$I cef.inc}

uses
  {$IFDEF DELPHI16_UP}
  Vcl.Forms,
  {$ELSE}
  Forms,
  Windows,
  {$ENDIF }
  SysUtils,
  SuperObject,
  uCEFApplication,
  BrowserUnit in 'BrowserUnit.pas' {Form1},
  BrowserDB in 'BrowserDB.pas',
  ConfigFormUnit in 'ConfigFormUnit.pas' {ConfigForm},
  AboutUnit in 'AboutUnit.pas' {Form2},
  LanguageUnit in 'LanguageUnit.pas' {FormLang},
  SearchSortUnit in 'SearchSortUnit.pas' {Form3};

{$R *.res}

// CEF3 needs to set the LARGEADDRESSAWARE flag which allows 32-bit processes to use up to 3GB of RAM.
// If you don't add this flag the rederer process will crash when you try to load large images.
//{$SetPEFlags IMAGE_FILE_LARGE_ADDRESS_AWARE}


function GetDatabaseDir: String;
begin
  Result:='C:\ProgramData\Moogle\Database\';
end;

function IsClearCache: Boolean;
var M: ISuperObject;
begin
  Result:=False;
  if FileExists(GetDatabaseDir+'cache.json')=True then
  begin
    try
      M:=TSuperObject.ParseFile(GetDatabaseDir+'cache.json',False);
      Result:=M.B['option.cookies.clear.value'];
    except
      Result:=False;
    end;
  end;
end;

procedure RemoveCache(const Dir: string);
var
  Result: TSearchRec;
begin
  if FindFirst(Dir + '\*', faAnyFile, Result) = 0 then
  begin
    Try
      repeat
        if (Result.Attr and faDirectory) = faDirectory then
        begin
          if (Result.Name <> '.') and (Result.Name <> '..') then
            RemoveCache(Dir + '\' + Result.Name)
        end
        else if not DeleteFile(Dir + '\' + Result.Name) then begin {RaiseLastOSError} end;
      until FindNext(Result) <> 0;
    Finally
      FindClose(Result);
    End;
  end;
  if not RemoveDir(Dir) then
  begin
    //RaiseLastOSError;
  end;

end;

begin

  if IsClearCache=True then RemoveCache(ExtractFilePath(ParamStr(0))+'\cache');
  GlobalCEFApp := TCefApplication.Create;
  GlobalCEFApp.cache := 'cache';
  // In case you want to use custom directories for the CEF3 binaries, cache and user data.
  // If you don't set a cache directory the browser will use in-memory cache.
{
  GlobalCEFApp.FrameworkDirPath     := 'c:\cef';
  GlobalCEFApp.ResourcesDirPath     := 'c:\cef';
  GlobalCEFApp.LocalesDirPath       := 'c:\cef\locales';
  GlobalCEFApp.EnableGPU            := True;      // Enable hardware acceleration
  GlobalCEFApp.cache                := 'c:\cef\cache';
  GlobalCEFApp.UserDataPath         := 'c:\cef\User Data';
}
  // You *MUST* call GlobalCEFApp.StartMainProcess in a if..then clause
  // with the Application initialization inside the begin..end.
  // Read this https://www.briskbard.com/index.php?lang=en&pageid=cef
  if GlobalCEFApp.StartMainProcess then
  begin
    Application.Initialize;
    {$IFDEF DELPHI11_UP}
    Application.MainFormOnTaskbar := True;
    {$ENDIF}
    Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TConfigForm, ConfigForm);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TFormLang, FormLang);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
  end;
  GlobalCEFApp.Free;
  GlobalCEFApp := nil;
  if IsClearCache=True then RemoveCache(ExtractFilePath(ParamStr(0))+'\cache');
end.
