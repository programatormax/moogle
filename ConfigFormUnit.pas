unit ConfigFormUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.IniFiles, Vcl.StdCtrls, Vcl.CheckLst, BrowserDB,
  Vcl.Grids, Vcl.ValEdit, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdWhois, RestApiClient;

type
  TConfigForm = class(TForm)
    ValueListEditor1: TValueListEditor;
    IdWhois1: TIdWhois;
    Label1: TLabel;
    Memo1: TMemo;
  protected
   function Whois(Whois: String; var Refer: String): Boolean;
  public
   function ShowConfig(D: String): Boolean;
   function ShowMyIp(var MyIP: String): Boolean;
  end;

var
  ConfigForm: TConfigForm;

implementation

{$R *.dfm}

function IsItIPV4(Str: String): Boolean;
var t: String;
    n: Integer;
begin
  Result:=True;
  T:='0123456789.';
  for n:=1 to Length(Str) do
  begin
    if Pos(Str[n],T)=0 then
    begin
      Result:=False;
      Break;
    end;
  end;
end;

function TConfigForm.Whois(Whois: String; var Refer: String): Boolean;
var n: Integer;
    l: String;
    m: Integer;
    q: Integer;
    Domain: String;
    Response: String;
begin
  Result:=False;
  Refer:='';
  if (Whois<>'') and (pos('.',Whois)>0) then
  begin
    Memo1.Lines.Clear;
    ValueListEditor1.Strings.Clear;
    Domain:=Trim(LowerCase(Whois));
    Domain:=StringReplace(Domain,'whois://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'www.','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'http://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'https://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,'ftp://','',[rfReplaceAll, rfIgnoreCase]);
    Domain:=StringReplace(Domain,' ','',[rfReplaceAll, rfIgnoreCase]);
    for n:=1 to Length(Domain) do
    begin
      if Domain[n]='/' then
      begin
        Domain:=Copy(Domain,1,n-1);
        Break;
      end;
    end;
    if IsItIPV4(Domain)=False then
    begin
      q:=0;
      for n:=1 to Length(Domain) do
      begin
        if Domain[n]='.' then q:=q+1;
      end;
      if q>1 then
      begin
        for n:=1 to Length(Domain) do
        begin
          if Domain[n]='.' then
          begin
            Delete(Domain,1,n);
            Break;
          end;
        end;
      end;
      q:=0;
      for n:=1 to Length(Domain) do
      begin
        if Domain[n]='.' then q:=q+1;
      end;
      if q>1 then
      begin
        for n:=1 to Length(Domain) do
        begin
          if Domain[n]='.' then
          begin
            Delete(Domain,1,n);
            Break;
          end;
        end;
      end;
    end;
    Label1.Caption:=Domain;
    Response:=IdWhois1.WhoIs(Domain);
    if Response<>'' then
    begin
      l:='';
      for n:=1 to Length(Response) do
      begin
        if (Response[n]<>#$0D) and (Response[n]<>#$0A) then
        begin
          l:=l+Response[n];
        end
        else
        begin
          l:=Trim(l);
          if l<>'' then
          begin
            if (l[1]='%') or (l[1]='#') or (pos(':',l)=0) then
            begin
              l:=StringReplace(l,'%','',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'#','',[rfReplaceAll, rfIgnoreCase]);
              l:=Trim(l);
              if l<>'' then Memo1.Lines.Add(l);
            end
            else
            begin
              l:=StringReplace(l,'Last updated on','Last updated on: ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'     ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'    ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'   ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'  ',' ',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,': ','=',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'= ','=',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'>>>','',[rfReplaceAll, rfIgnoreCase]);
              l:=StringReplace(l,'<<<','',[rfReplaceAll, rfIgnoreCase]);
              l:=Trim(l);
              if l<>'' then
              begin
                if (pos('NOTICE=',l)>0) or (pos('TERMS OF USE=',l)>0) then
                begin
                  l:=StringReplace(l,'=',': ',[rfReplaceAll, rfIgnoreCase]);
                  Memo1.Lines.Add(l);
                end
                else
                begin
                  if (l[Length(l)]='=') then l:=l+'(None)';
                  if (l[Length(l)]=':') then
                  begin
                    l[Length(l)]:='=';
                    l:=l+'(None)';
                  end;
                  if pos('=',l)>0 then
                  begin
                    q:=0;
                    for m:=1 to Length(l) do
                    begin
                      if l[m]=' ' then q:=q+1;
                    end;
                    if q>7 then Memo1.Lines.Add(l) else ValueListEditor1.Strings.Add(l);
                  end
                  else
                  begin
                    Memo1.Lines.Add(l);
                  end;
                end;
              end;
            end;
            l:='';
          end;
        end;
      end;
      l:=Trim(l);
      if l<>'' then
      begin
        if (l[1]='%') or (l[1]='#') or (pos(':',l)=0) then
        begin
          l:=StringReplace(l,'%','',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'#','',[rfReplaceAll, rfIgnoreCase]);
          l:=Trim(l);
          if l<>'' then Memo1.Lines.Add(l);
        end
        else
        begin
          l:=StringReplace(l,'Last updated on','Last updated on: ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'     ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'    ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'   ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'  ',' ',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,': ','=',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'= ','=',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'>>>','',[rfReplaceAll, rfIgnoreCase]);
          l:=StringReplace(l,'<<<','',[rfReplaceAll, rfIgnoreCase]);
          l:=Trim(l);
          if l<>'' then
          begin
            if (pos('NOTICE=',l)>0) or (pos('TERMS OF USE=',l)>0) then
            begin
              l:=StringReplace(l,'=',': ',[rfReplaceAll, rfIgnoreCase]);
              Memo1.Lines.Add(l);
            end
            else
            begin
              if (l[Length(l)]='=') then l:=l+'(None)';
              if (l[Length(l)]=':') then
              begin
                l[Length(l)]:='=';
                l:=l+'(None)';
              end;
              if pos('=',l)>0 then
              begin
                q:=0;
                for m:=1 to Length(l) do
                begin
                  if l[m]=' ' then q:=q+1;
                end;
                if q>7 then Memo1.Lines.Add(l) else ValueListEditor1.Strings.Add(l);
              end
              else
              begin
                Memo1.Lines.Add(l);
              end;
            end;
          end;
        end;
        l:='';
      end;
      if ValueListEditor1.Strings.IndexOfName('refer')<>-1 then Refer:=ValueListEditor1.Strings.Values['refer'];
      Result:=True;
    end;
  end;
end;

function TConfigForm.ShowConfig(D: String): Boolean;
var R: String;
begin
  Result:=False;
  IdWhois1.Host:='whois.iana.org';
  if Whois(D,R)=True then
  begin
    if Trim(R)<>'' then
    begin
      IdWhois1.Host:=R;
      if Whois(D,R)=True then
      begin
        Result:=True;
        ShowModal;
      end;
    end
    else
    begin
      Result:=True;
      ShowModal;
    end;
  end;
end;

function TConfigForm.ShowMyIp(var MyIP: String): Boolean;
begin
  Result:=False;
  Memo1.Lines.Clear;
  ValueListEditor1.Strings.Clear;
  Label1.Caption:='My ip service';
  Memo1.Lines.Add('IP Service');
  ValueListEditor1.Strings.Add('IP=127.0.0.1');
  ValueListEditor1.Strings.Add('Country=United states');
  MyIP:='127.0.0.1';
  Result:=True;
  ShowModal;
end;


end.
