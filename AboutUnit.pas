unit AboutUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Grids, Vcl.ValEdit, BrowserDB,
  Vcl.StdCtrls, Vcl.Buttons, Vcl.Imaging.pngimage, Vcl.ExtCtrls, EntityProjectUnit;

type
  TForm2 = class(TForm)
    ValueListEditor1: TValueListEditor;
    Label1: TLabel;
    Label2: TLabel;
    Image1: TImage;
    Label3: TLabel;
    ValueListEditor2: TValueListEditor;
  private
    { Private declarations }
  public
   procedure ShowAbout(Moogle: TBrowserDataBase);
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.ShowAbout(Moogle: TBrowserDataBase);
var n: Integer;
begin
  ValueListEditor1.Strings.Clear;
  ValueListEditor2.Strings.Clear;
  ValueListEditor1.InsertRow('Guid',Moogle.DB.S['project.guid'],True);
  ValueListEditor1.InsertRow('System',Moogle.DB.S['project.system'],True);
  ValueListEditor1.InsertRow('Capacity',Moogle.DB.S['project.capacity'],True);
  ValueListEditor1.InsertRow('Path',Moogle.DB.S['project.path'],True);
  ValueListEditor1.InsertRow('Name',Moogle.DB.S['project.name'],True);
  ValueListEditor1.InsertRow('Size',Moogle.DB.S['project.size'],True);
  ValueListEditor1.InsertRow('Version',Moogle.DB.S['project.version'],True);
  ValueListEditor1.InsertRow('Build',Moogle.DB.S['project.build'],True);
  ValueListEditor1.InsertRow('Vendor',Moogle.DB.S['project.vendor'],True);
  ValueListEditor1.InsertRow('Operating system',Moogle.DB.S['os.name']+' ('+Moogle.DB.S['os.build']+')',True);
  ValueListEditor1.InsertRow('User',Moogle.DB.S['os.login'],True);
  if Project.Available=True then
  begin
    ValueListEditor1.InsertRow('Licensed','Yes',True);
  end
  else
  begin
    ValueListEditor1.InsertRow('Licensed','No',True);
  end;
  ValueListEditor1.InsertRow('License issue date',Moogle.DB.S['project.issue'],True);
  ValueListEditor1.InsertRow('License expiry date',Moogle.DB.S['project.expiry'],True);
  Label1.Caption:='MOOGLE';
  Label2.Caption:=Moogle.DB.S['project.copyright'];
  n:=0;
  if Assigned(Moogle.DB.O['packet']) then
  begin
    while Assigned(Moogle.DB.A['packet'].O[n])=True do
    begin
      ValueListEditor2.InsertRow(Moogle.DB.A['packet'].O[n].S['description'],Moogle.DB.A['packet'].O[n].S['version'],True);
      n:=n+1;
    end;
  end;
  ShowModal;
end;

end.
