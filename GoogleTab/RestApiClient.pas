unit RestApiClient;

interface

{$ifdef fpc}
  {$mode delphi}
  {$H+}
  {$codepage cp1251}
{$endif}

uses

  SysUtils,
  Classes,
  superobject,
  curl_h,
  IniFiles,
  Process,
  DateUtils;

type

  TQuery = (jGet,jPost);

  TGoogleRestApi = class
  private
    FProcess: TProcess;
    FIniFile: TIniFile;
    FActive: Boolean;
    FLogging: Boolean;
    FHost: AnsiString;
    FPort: Word;
    FhCurl: pCurl;
    FParams: pcurl_slist;
    FIncomming: AnsiString;
    FAccessToken: AnsiString;
    FTimeOut: UInt64;
    FStream: TMemoryStream;
    procedure Reset;
    procedure SetActive(Act: Boolean);
    procedure SetHost(Str: AnsiString);
    procedure SetLogging(Val: Boolean);
    procedure Log(Str: AnsiString);
    function GetClientId: AnsiString;
    procedure SetClientId(Val: AnsiString);
    function GetClientSecret: AnsiString;
    procedure SetClientSecret(Val: AnsiString);
    function GetAccessToken: AnsiString;
    procedure SetAccessToken(Val: AnsiString);
    function GetRefreshToken: AnsiString;
    procedure SetRefreshToken(Val: AnsiString);
    function GetRotDir: AnsiString;
    function GetLogDir: AnsiString;
    function GetCfgDir: AnsiString;
    function GetBrwDir: AnsiString;
  public
    property Active: Boolean read FActive write SetActive;
    property Host: AnsiString read FHost write SetHost;
    property Logging: Boolean read FLogging write SetLogging;
    property ClientId: AnsiString read GetClientId;
    property ClientSecret: AnsiString read GetClientSecret;
    property ClientRoot: AnsiString read GetRotDir;
    function OAuth2p0GoogleStart: Boolean;
    function OAuth2p0GoogleComplite(Key: AnsiString): Boolean;
    function OAuth2p0Refresh: Boolean;
    function Query(Method: TQuery; Header: AnsiString; Obj: ISuperObject): ISuperObject;
    function Download(Method: TQuery; Mime: AnsiString; Header: AnsiString): TMemoryStream;
    function GCorn(Str: AnsiString; Mime: AnsiString): AnsiString;
    function GDect(Obj: ISuperObject; Id: AnsiString): Boolean;
    function GDest(Obj: ISuperObject; Id: AnsiString; var Warn: Boolean): AnsiString;
    function GAbout: ISuperObject;
    function GDisk(Npg: AnsiString): ISuperObject;
    function GLoad(Filter: Boolean): ISuperObject;
    function GDown(Mime: AnsiString; Link: AnsiString): TMemoryStream;
    constructor Create;
    destructor Destroy; override;
  end;

implementation

const

  B64Table= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
  TIMEOUT = 3600+3600;

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

//Receiver data 1
function OnRecv1(Buffer: PChar; Size, nItems: longword; UserData: pointer): integer;  cdecl;
var s: string;
begin
  Result:=Size*nItems;
  SetLength(S, Result);
  if Result>0 then
  begin
    Move(Buffer^, S[1], Result);
    TGoogleRestApi(UserData).FIncomming:=TGoogleRestApi(UserData).FIncomming + S;
  end;
end;

//Receiver data
function OnRecv2(Buffer: PChar; Size, nItems: longword; UserData: pointer): integer;  cdecl;
var s: string;
begin
  Result:=Size*nItems;
  if Result>0 then TGoogleRestApi(UserData).FStream.Write(Buffer^,Result);
end;

//Encode to Base64
function B64Encode(S: AnsiString): AnsiString;
var
  i: integer;
  InBuf: array[0..2] of byte;
  OutBuf: array[0..3] of char;
begin
  SetLength(Result,((Length(S)+2) div 3)*4);
  for i:= 1 to ((Length(S)+2) div 3) do
  begin
    if Length(S)< (i*3) then
      Move(S[(i-1)*3+1],InBuf,Length(S)-(i-1)*3)
    else
      Move(S[(i-1)*3+1],InBuf,3);
    OutBuf[0]:= B64Table[((InBuf[0] and $FC) shr 2) + 1];
    OutBuf[1]:= B64Table[(((InBuf[0] and $03) shl 4) or ((InBuf[1] and $F0) shr 4)) + 1];
    OutBuf[2]:= B64Table[(((InBuf[1] and $0F) shl 2) or ((InBuf[2] and $C0) shr 6)) + 1];
    OutBuf[3]:= B64Table[(InBuf[2] and $3F) + 1];
    Move(OutBuf,Result[(i-1)*4+1],4);
  end;
  if (Length(S) mod 3)= 1 then
  begin
    Result[Length(Result)-1]:= '=';
    Result[Length(Result)]:= '=';
  end
  else if (Length(S) mod 3)= 2 then
    Result[Length(Result)]:= '=';
end;

//------------------------------------------------------------------------------
// {TGoogleRestApi}
//------------------------------------------------------------------------------

//Create class
constructor TGoogleRestApi.Create;
begin
  inherited Create;
  Reset;
  ForceDirectories(GetCfgDir);
  FIniFile:=TIniFile.Create(GetCfgDir+'GoogleDrive.ini');
  FStream:=TMemoryStream.Create;
  FAccessToken:=GetAccessToken;
  FTimeOut:=0;
end;

//Destroy class
destructor TGoogleRestApi.Destroy;
begin
  Active:=False;
  FIniFile.Destroy;
  FStream.Destroy;
  Reset;
  inherited Destroy;
end;

//Reset params
procedure TGoogleRestApi.Reset;
begin
  FActive:=False;
  FLogging:=False;
  FHost:='';
  FhCurl:=nil;
  FParams:=nil;
  FIncomming:='';
  FIniFile:=nil;
  FProcess:=nil;
  FAccessToken:='';
  FTimeOut:=0;
  FStream:=nil;
end;

//Activation
procedure TGoogleRestApi.SetActive(Act: Boolean);
begin
  if (FActive=False) and (Act=True) then
  begin
    FhCurl:=curl_easy_init;
    if FhCurl<>nil then
    begin
      curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv1);
      curl_easy_setopt(FhCurl, CURLOPT_WRITEDATA,Pointer(Self));
      curl_easy_setopt(FhCurl,CURLOPT_HTTPAUTH, CURLAUTH_NONE);
      curl_easy_setopt(FhCurl,CURLOPT_SSL_VERIFYPEER,0);
      curl_easy_setopt(FhCurl,CURLOPT_SSL_VERIFYHOST,0);
      FActive:=True;
    end;
  end;
  if (FActive=True) and (Act=False) then
  begin
    curl_slist_free_all(FParams);
    FParams:=nil;
    curl_easy_cleanup(FhCurl);
    FhCurl:=nil;
    FActive:=False;
  end;
end;

//Set primary host
procedure TGoogleRestApi.SetHost(Str: AnsiString);
begin
  if FActive=False then FHost:=Trim(Str);
end;

//Enable logging
procedure TGoogleRestApi.SetLogging(Val: Boolean);
begin
  if FActive=False then FLogging:=Val;
end;

//Read client id
function TGoogleRestApi.GetClientId: AnsiString;
begin
  Result:=Trim(FIniFile.ReadString('Google','ClientId',''));
end;

//Write client id
procedure TGoogleRestApi.SetClientId(Val: AnsiString);
begin
  FIniFile.WriteString('Google','ClientId',Trim(Val));
end;

//Get client secret
function TGoogleRestApi.GetClientSecret: AnsiString;
begin
  Result:=Trim(FIniFile.ReadString('Google','ClientSecret',''));
end;

//Set client secret
procedure TGoogleRestApi.SetClientSecret(Val: AnsiString);
begin
  FIniFile.WriteString('Google','ClientSecret',Trim(Val));
end;

//Get access token
function TGoogleRestApi.GetAccessToken: AnsiString;
begin
  Result:=Trim(FIniFile.ReadString('Token','AccessToken',''));
end;

//Set access token
procedure TGoogleRestApi.SetAccessToken(Val: AnsiString);
begin
  FIniFile.WriteString('Token','AccessToken',Trim(Val));
end;

//Get refresh token
function TGoogleRestApi.GetRefreshToken: AnsiString;
begin
  Result:=Trim(FIniFile.ReadString('Token','RefreshToken',''));
end;

//Set refresh token
procedure TGoogleRestApi.SetRefreshToken(Val: AnsiString);
begin
  FIniFile.WriteString('Token','RefreshToken',Trim(Val));
end;

//Get root path
function TGoogleRestApi.GetRotDir: AnsiString;
begin
{$ifdef linux}
    Result:=Trim(FIniFile.ReadString('Google','Root','/tmp/googledrive/'));
{$else}
    Result:=Trim(FIniFile.ReadString('Google','Root',ExtractFilePath(ParamStr(0))+'GoogleDrive\'));
{$endif}
    if (Length(Result)>=1) and (Result[Length(Result)]<>PathDelim) then Result:=Result+PathDelim;
end;

//Get log root
function TGoogleRestApi.GetLogDir: AnsiString;
begin
{$ifdef linux}
  Result:='/var/log/googledrive/'+DateToStr(Now)+'/';
{$else}
  Result:=ExtractFilePath(ParamStr(0))+'GoogleDrive\Log\'+DateToStr(Now)+'\';
{$endif}
end;

//Get config file
function TGoogleRestApi.GetCfgDir: AnsiString;
begin
{$ifdef linux}
  Result:='/var/googledrive/';
{$else}
  Result:=ExtractFilePath(ParamStr(0));
{$endif}
end;

//Get browser dir
function TGoogleRestApi.GetBrwDir: AnsiString;
begin
{$ifdef linux}
  Result:='/tmp/';
{$else}
  Result:=ExtractFilePath(ParamStr(0));
{$endif}
end;

//Logging
procedure TGoogleRestApi.Log(Str: AnsiString);
var Path: AnsiString;
    CrLf: AnsiString;
    hFile: Integer;
    hLen: Int64;
begin
  if FLogging=True then
  begin
    Path:=GetLogDir;
    CrLf:=#13#10;
    ForceDirectories(Path);
    hFile:=FileOpen(Path+ClassName+'.log',1);
    if hFile=-1 then hFile:=FileCreate(Path+ClassName+'.log');
    if hFile<>-1 then
    begin
      FileSeek(hFile,0,soFromEnd);
      if Str<>'' then FileWrite(hFile,Str[1],Length(Str));
      FileWrite(hFile,CrLf[1],Length(CrLf));
      FileClose(hFile);
    end;
  end;
end;

//Correct
function TGoogleRestApi.GCorn(Str: AnsiString; Mime: AnsiString): AnsiString;
var n: Integer;
    Ext: AnsiString;
begin
  Result:='';
  if Str<>'' then
  begin
    for n:=1 to Length(Str) do
    begin
      if Str[n]='à' then Str[n]:='a';
      if Str[n]='è' then Str[n]:='e';
      if Str[n]='é' then Str[n]:='e';
      if Str[n]='ì' then Str[n]:='i';
      if Str[n]='í' then Str[n]:='i';
      if Str[n]='ò' then Str[n]:='o';
      if Str[n]='ó' then Str[n]:='o';
      if Str[n]='ù' then Str[n]:='u';
      if Str[n]='ú' then Str[n]:='u';
      if Str[n]='''' then Str[n]:='_';
      if Str[n]='#' then Str[n]:='_';
      if Str[n]='?' then Str[n]:='_';
      if Str[n]='!' then Str[n]:='_';
      if Str[n]='&' then Str[n]:='_';
      if Str[n]='*' then Str[n]:='_';
      if Str[n]='(' then Str[n]:='_';
      if Str[n]=')' then Str[n]:='_';
      if (n>5) and (Str[n]=':') then Str[n]:='_';
      if (Byte(Str[n])>=48) and (Byte(Str[n])<=57) then
      begin
        Result:=Result+Str[n];
      end
      else
      begin
        if (Byte(Str[n])>=65) and (Byte(Str[n])<=90) then
        begin
          Result:=Result+Str[n];
        end
        else
        begin
          if(Byte(Str[n])>=97) and (Byte(Str[n])<=122) then
          begin
            Result:=Result+Str[n];
          end
          else
          begin
            if (Str[n]='/') or
               (Str[n]='\') or
               (Str[n]=' ') or
               (Str[n]='.') or
               (Str[n]='_') or
               (Str[n]=':') then Result:=Result+Str[n];
          end;
        end;
      end;
    end;
    if Mime<>'application/vnd.google-apps.folder' then
    begin
      if (Length(Result)>4) and (Pos('.',Copy(Result,Length(Result)-4,4))=0) then
      begin
        Ext:='.dat';
        if Mime='application/vnd.google-apps.document' then Ext:='.docx';
        if Mime='image/jpeg' then Ext:='.jpg';
        if Mime='text/html' then Ext:='.html';
        if Mime='text/plain' then Ext:='.txt';
        if Mime='application/x-msdownload' then Ext:='.exe';
        if Mime='application/rar' then Ext:='.rar';
        Result:=Result+Ext;
      end;
    end;
  end;
end;

//Detect parent
function TGoogleRestApi.GDect(Obj: ISuperObject; Id: AnsiString): Boolean;
var n: Integer;
begin
  Result:=False;
  if Assigned(Obj.O['items'])=True then
  begin
    n:=0;
    while Assigned(Obj.A['items'].O[n])=True do
    begin
      if Obj.A['items'].O[n].S['id']=id then
      begin
        Result:=True;
        Exit;
      end;
      n:=n+1;
    end;
  end;
end;

//Get destination drive path
function TGoogleRestApi.GDest(Obj: ISuperObject; Id: AnsiString; var Warn: Boolean): AnsiString;
var n: Integer;
begin
  Result:='';
  if Assigned(Obj.O['items'])=True then
  begin
    n:=0;
    while Assigned(Obj.A['items'].O[n])=True do
    begin
      if Obj.A['items'].O[n].S['id']=id then
      begin
        if Obj.A['items'].O[n].S['mimeType']='application/vnd.google-apps.folder' then
        begin
          Result:=Obj.A['items'].O[n].S['title'];
        end
        else
        begin
          if Obj.A['items'].O[n].S['originalFilename']<>'' then
          begin
            Result:=Obj.A['items'].O[n].S['originalFilename'];
          end
          else
          begin
            Result:=Obj.A['items'].O[n].S['title'];
          end;
        end;
        if (Assigned(Obj.A['items'].O[n].A['parents'])=True) and
           (Assigned(Obj.A['items'].O[n].A['parents'].O[0])=True) then
        begin
          if Obj.A['items'].O[n].A['parents'].O[0].B['isRoot']=True then
          begin
            Result:=Trim(GetRotDir)+Result;
          end
          else
          begin
            if GDect(Obj,Obj.A['items'].O[n].A['parents'].O[0].S['id'])=True then
            begin
              Result:=Trim(GDest(Obj,Obj.A['items'].O[n].A['parents'].O[0].S['id'],Warn))+PathDelim+Result;
            end
            else
            begin
              Warn:=True;
            end;
          end;
          break;
        end
        else
        begin
          Warn:=True;
          Break;
        end;
      end;
      n:=n+1;
    end;
  end;
end;

//Authorization start
function TGoogleRestApi.OAuth2p0GoogleStart: Boolean;
var PostData: string;
    Res: CurlCode;
    FStrings: TStringList;
begin
  Result:=False;
  if FActive=True then
  begin
    PostData:='client_id='+GetClientId;
    PostData:=PostData+'&api_key=AIzaSyCraItwOgZcse8pzQ83MwZ_zqSVfn7aczA';
    PostData:=PostData+'&scope=https://www.googleapis.com/auth/drive.readonly';
    PostData:=PostData+'&response_type=code';
    PostData:=PostData+'&state=security_token%3D138r5719ru3e1%26url%3Dhttps%3A%2F%2Foauth2.example.com%2Ftoken';
    PostData:=PostData+'&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob';
    Log('---Start---');
    Log('');
    Log('Method: POST');
    Log('Host: accounts.google.com');
    Log('Header: /o/oauth2/v2/auth?');
    Log('');
    Log(PostData);
    FIncomming:='';
    curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv1);
    curl_easy_setopt(FhCurl, CURLOPT_URL, PChar('https://accounts.google.com/o/oauth2/v2/auth?'));
    curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,TIMEOUT*TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_HTTPPOST,1);
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDS,PChar(PostData));
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDSIZE,Length(PostData));
    FParams:=curl_slist_append(FParams,'Host: accounts.google.com');
    FParams:=curl_slist_append(FParams,'Content-Type: application/x-www-form-urlencoded');
    FParams:=curl_slist_append(FParams,'Accept: text/plain');
    FParams:=curl_slist_append(FParams,'Connection: Keep-Alive');
    curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
    Res:=curl_easy_perform(FhCurl);
    Log('');
    Log('---Response---');
    Log('');
    Log('Curl: '+IntToStr(Qword(Res)));
    Log('');
    if Res=CURLE_OK then
    begin
      if FIncomming<>'' then
      begin
        DeleteFile(GetBrwDir+'autorization.html');
        Log(FIncomming);
        Log('');
        ForceDirectories(GetBrwDir);
        FStrings:=TStringList.Create;
        FStrings.LineBreak:=#13#10;
        FStrings.Text:=FIncomming;
        FStrings.SaveToFile(GetBrwDir+'autorization.html');
        FStrings.Destroy;
        FStrings:=nil;
        FProcess:=TProcess.Create(nil);
        FProcess.Parameters.Clear;
{$ifdef linux}
        FProcess.Executable:='sensible-browser';
        FProcess.Parameters.Add(GetBrwDir+'autorization.html');
{$else}
        FProcess.Executable:='rundll32';
        FProcess.Parameters.Add('url.dll,FileProtocolHandler');
        FProcess.Parameters.Add('autorization.html');
{$endif}
        FProcess.ShowWindow:=swoShowNormal;
        //FProcess.Options:=FProcess.Options + [poWaitOnExit];
        try
          FProcess.Execute;
          Result:=True;
        except
          Result:=False;
        end;
        FProcess.Destroy;
        FProcess:=nil;
      end
      else
      begin
        Log('No Answer');
      end;
    end;
    curl_slist_free_all(FParams);
    FParams:=nil;
  end;
end;

//Complite autorization
function TGoogleRestApi.OAuth2p0GoogleComplite(Key: AnsiString): Boolean;
var PostData: string;
    Res: CurlCode;
    Obj: ISuperObject;
begin
  Result:=False;
  if FActive=True then
  begin
    PostData:='client_id='+GetClientId;
    PostData:=PostData+'&client_secret='+GetClientSecret;
    PostData:=PostData+'&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob';
    PostData:=PostData+'&response_type=0';
    PostData:=PostData+'&grant_type=authorization_code';
    PostData:=PostData+'&code='+Key;
    PostData:=PostData+'&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fadwords';
    Log('---Start---');
    Log('');
    Log('Method: POST');
    Log('Host: accounts.google.com');
    Log('Header: /o/oauth2/v2/auth?');
    Log('');
    Log(PostData);
    FIncomming:='';
    curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv1);
    curl_easy_setopt(FhCurl, CURLOPT_URL, PChar('https://oauth2.googleapis.com/token'));
    curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,TIMEOUT*TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_HTTPPOST,1);
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDS,PChar(PostData));
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDSIZE,Length(PostData));
    FParams:=curl_slist_append(FParams,'Host: oauth2.googleapis.com');
    FParams:=curl_slist_append(FParams,'Content-Type: application/x-www-form-urlencoded');
    FParams:=curl_slist_append(FParams,'Accept: text/plain');
    FParams:=curl_slist_append(FParams,'Connection: Keep-Alive');
    curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
    Res:=curl_easy_perform(FhCurl);
    Log('');
    Log('---Response---');
    Log('');
    Log('Curl: '+IntToStr(Qword(Res)));
    Log('');
    if Res=CURLE_OK then
    begin
      if FIncomming<>'' then
      begin
        Log(FIncomming);
        Log('');
        Obj:=TSuperObject.ParseString(@WideString(FIncomming)[1],False);
        if (Obj.S['access_token']<>'') and (Obj.S['refresh_token']<>'') then
        begin
          FAccessToken:=Obj.S['access_token'];
          SetAccessToken(FAccessToken);
          SetRefreshToken(Obj.S['refresh_token']);
          Result:=True;
        end
        else
        begin
          Result:=False;
        end;
      end
      else
      begin
        Log('No Answer');
      end;
    end;
    curl_slist_free_all(FParams);
    FParams:=nil;
  end;
end;

//Refresh
function TGoogleRestApi.OAuth2p0Refresh: Boolean;
var PostData: string;
    Res: CurlCode;
    Obj: ISuperObject;
begin
  Result:=False;
  if FActive=True then
  begin
    PostData:=PostData+'{"client_id": "'+GetClientId+'",';
    PostData:=PostData+'"client_secret": "'+GetClientSecret+'",';
    PostData:=PostData+'"refresh_token": "'+GetRefreshToken+'",';
    PostData:=PostData+'"grant_type": "refresh_token"}';
    Log('---Start---');
    Log('');
    Log('Method: POST');
    Log('Host: oauth2.googleapis.com');
    Log('Header: /o/oauth2/v2/auth?');
    Log('');
    Log(PostData);
    FIncomming:='';
    curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv1);
    curl_easy_setopt(FhCurl, CURLOPT_URL, PChar('https://oauth2.googleapis.com/token'));
    curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,TIMEOUT*TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_HTTPPOST,1);
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDS,PChar(PostData));
    curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDSIZE,Length(PostData));
    FParams:=curl_slist_append(FParams,'Host: oauth2.googleapis.com');
    FParams:=curl_slist_append(FParams,'Content-Type: application/json');
    FParams:=curl_slist_append(FParams,'Accept: application/json');
    FParams:=curl_slist_append(FParams,'Connection: Keep-Alive');
    curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
    Res:=curl_easy_perform(FhCurl);
    Log('');
    Log('---Response---');
    Log('');
    Log('Curl: '+IntToStr(Qword(Res)));
    Log('');
    if Res=CURLE_OK then
    begin
      if FIncomming<>'' then
      begin
        FTimeOut:=GetTickCount64;
        Log(FIncomming);
        Log('');
        try
          Obj:=TSuperObject.ParseString(@WideString(FIncomming)[1],False);
          if (Assigned(Obj)=True) and (Assigned(Obj.O['error'])=False) then
          begin
            FAccessToken:=Obj.S['access_token'];
            SetAccessToken(FAccessToken);
            Result:=True;
          end
          else
          begin
            Result:=False;
          end;
        except
          Result:=False;
        end;
      end
      else
      begin
        Log('No Answer');
      end;
    end;
    curl_slist_free_all(FParams);
    FParams:=nil;
  end;
end;

//Execute http querty
function TGoogleRestApi.Query(Method: TQuery; Header: AnsiString; Obj: ISuperObject): ISuperObject;
var PostData: string;
    Res: CurlCode;
begin
  if (GetTickCount64-FTimeOut)>(1000*1200) then OAuth2p0Refresh;
  Log('---Request---');
  Log('');
  case Method of
    jGet:
    begin
      Log('Method: GET');
    end;
    jPost:
    begin
      Log('Method: POST');
    end;
  end;
  Log('Host: '+Host);
  Log('Header: '+Header);
  if Assigned(Obj)=True then
  begin
    Log('');
    Log(Obj.AsJSon);
    Log('');
  end
  else
  begin
    Log('');
    Log('No attachment');
    Log('');
  end;
  if FActive=True then
  begin
    FStream.SetSize(0);
    FIncomming:='';
    curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv2);
    if pos('://',Header)>0 then
    begin
      curl_easy_setopt(FhCurl, CURLOPT_URL, PChar(Header));
    end
    else
    begin
      curl_easy_setopt(FhCurl, CURLOPT_URL, PChar(FHost+Header));
    end;
    curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,TIMEOUT*TIMEOUT);
    case Method of
      jGet:
      begin
        curl_easy_setopt(FhCurl, CURLOPT_HTTPGET,1);
      end;
      jPost:
      begin
        PostData:=Obj.AsJSon;
        curl_easy_setopt(FhCurl, CURLOPT_HTTPPOST,1);
        curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDS,PChar(PostData));
        curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDSIZE,Length(PostData));
      end;
    end;
    FParams:=curl_slist_append(FParams,'Host: www.googleapis.com');
    if FAccessToken<>'' then FParams:=curl_slist_append(FParams,PChar('Authorization: Bearer '+FAccessToken));
    FParams:=curl_slist_append(FParams,'Content-type: application/json');
    FParams:=curl_slist_append(FParams,'Accept: application/json');
    FParams:=curl_slist_append(FParams,'Connection: Keep-Alive');
    curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
    Res:=curl_easy_perform(FhCurl);
    if Res=CURLE_OK then
    begin
      if FStream.Size=0 then
      begin
        FIncomming:='{"error": "No response","code" :-4}}';
      end
      else
      begin
        SetString(FIncomming,FStream.Memory,FStream.Size);
      end;
    end
    else
    begin
      FIncomming:='{"error": "Curl error '+IntToStr(Qword(Res))+'","code": -1}}';
    end;
    curl_slist_free_all(FParams);
    FParams:=nil;
  end
  else
  begin
    FIncomming:='{"error": "No connection server '+FHost+'","code": -2}';
  end;
  try
    Result:=TSuperObject.ParseString(@WideString(FIncomming)[1],False);
  except
    FIncomming:='{"error": "Syntax error", "code": -3}';
    Result:=TSuperObject.ParseString(@WideString(FIncomming)[1],False);
  end;
  Log('---Response---');
  Log('');
  Log(FIncomming);
  Log('');
end;

//Donwload
function TGoogleRestApi.Download(Method: TQuery; Mime: AnsiString; Header: AnsiString): TMemoryStream;
var PostData: string;
    Res: CurlCode;
begin
  Result:=nil;
  if (GetTickCount64-FTimeOut)>(1000*1200) then OAuth2p0Refresh;
  Log('---Request---');
  Log('');
  case Method of
    jGet:
    begin
      Log('Method: GET');
    end;
    jPost:
    begin
      Log('Method: POST');
    end;
  end;
  Log('Host: '+Host);
  Log('Header: '+Header);
  Log('');
  Log('No attachment');
  Log('');
  if FActive=True then
  begin
    FIncomming:='';
    FStream.SetSize(0);
    curl_easy_setopt(FhCurl, CURLOPT_WRITEFUNCTION, @OnRecv2);
    if pos('://',Header)>0 then
    begin
      curl_easy_setopt(FhCurl, CURLOPT_URL, PChar(Header));
    end
    else
    begin
      curl_easy_setopt(FhCurl, CURLOPT_URL, PChar(FHost+Header));
    end;
    curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,TIMEOUT);
    curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,TIMEOUT*TIMEOUT);
    case Method of
      jGet:
      begin
        curl_easy_setopt(FhCurl, CURLOPT_HTTPGET,1);
      end;
      jPost:
      begin
        PostData:='';
        curl_easy_setopt(FhCurl, CURLOPT_HTTPPOST,1);
        curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDS,PChar(PostData));
        curl_easy_setopt(FhCurl, CURLOPT_POSTFIELDSIZE,Length(PostData));
      end;
    end;
    FParams:=curl_slist_append(FParams,'Host: www.googleapis.com');
    if FAccessToken<>'' then FParams:=curl_slist_append(FParams,PChar('Authorization: Bearer '+FAccessToken));
    FParams:=curl_slist_append(FParams,PChar('Content-type: '+Mime));
    FParams:=curl_slist_append(FParams,PChar('Accept: '+Mime));
    FParams:=curl_slist_append(FParams,'Connection: Keep-Alive');
    curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
    FIncomming:='';
    if curl_easy_perform(FhCurl)=CURLE_OK then
    begin
      Result:=TMemoryStream.Create;
      FStream.Position:=0;
      if FStream.Size>0 then Result.LoadFromStream(FStream);
      curl_slist_free_all(FParams);
      FParams:=nil;
    end;
    Log('---Response---');
    Log('');
    Log('File');
    Log('');
    FIncomming:='';
  end;
end;

//Google Api (About user)
function TGoogleRestApi.GAbout: ISuperObject;
begin
 Result:=Query(jGet,'/drive/v2/about',nil);
end;

//Google api (Get meta data list)
function TGoogleRestApi.GDisk(Npg: AnsiString): ISuperObject;
begin
  if Npg<>'' then
  begin
    Result:=Query(jGet,'/drive/v2/files?maxResults=5000&pageToken='+Npg,nil);
  end
  else
  begin
    Result:=Query(jGet,'/drive/v2/files?maxResults=5000',nil);
  end;
end;

//Google api (Get meta data full list)
function TGoogleRestApi.GLoad(Filter: Boolean): ISuperObject;
var n,m: UInt64;
    Obj: ISuperObject;
    Npg: AnsiString;
begin
  Result:=TSuperObject.ParseString('{"items": []}',False);
  if Filter=True then
  begin
    m:=1;
    Npg:='';
    while m>0 do
    begin
      m:=0;
      Obj:=GDisk(Npg);
      if (Assigned(Obj)=True) and (Assigned(Obj.O['error'])=False) then
      begin
        Npg:=Obj.S['nextPageToken'];
        n:=0;
        while Assigned(Obj.A['items'].O[n])=True do
        begin
          if (Obj.A['items'].O[n].B['shared']=False) and
             (Obj.A['items'].O[n].B['explicitlyTrashed']=False) and
             (Obj.A['items'].O[n].B['labels.trashed']=False) then
          begin
            Result.A['items'].Add(Obj.A['items'].O[n]);
            m:=m+1;
          end;
          n:=n+1;
        end;
      end;
    end;
  end;
end;

//Google api (Donwload file)
function TGoogleRestApi.GDown(Mime: AnsiString; Link: AnsiString): TMemoryStream;
begin
  Result:=Download(jGet,Mime,Link);
end;

initialization
  curl_global_init(CURL_GLOBAL_ALL);

finalization
  curl_global_cleanup;

end.
