unit GoogleBackupUnit;

{

  Interface from pdf document 155377.pdf

}

interface

{$ifdef fpc}
  {$mode delphi}
  {$H+}
  {$codepage cp1251}
{$endif}

uses

  Classes,
  SysUtils,
  RestApiClient,
  superobject;

const

  //It returns 0 for success
  SUCCESS_OPERATION        = 0;

  //disk full
  ERROR_SYNCONE_DISKFULL    = -155423;
  //Write error
  ERROR_SYNCONE_WRITE       = -155443;
  //Communication error
  ERROR_SYNCONE_COMM        = -155453;
  //timeout
  ERROR_SYNCONE_TIMEOUT     = -155461;
  //future use
  ERROR_SYNCONE_FUTUREUSE1  = -155473;
  //future use
  ERROR_SYNCONE_FUTUREUSE2  = -155501;
  //future use
  ERROR_SYNCONE_FUTUREUSE3  = -155509;

  //Communication error
  ERROR_FILLLIST_COMM       = -155381;
  //time out
  ERROR_FILLLIST_TIMEOUT    = -155383;
  //future use
  ERROR_FILLLIST_FUTUREUSE1 = -155473;
  //future use
  ERROR_FILLLIST_FUTUREUSE2 = -155501;
  //future use
  ERROR_FILLLIST_FUTUREUSE3 = -155509;

type

  TGoogleBackupRecord = class (TComponent)
  public
    FilePath: String;
    FileName: String;
    FileSize: Int64;
    FileDate: TDatetime;
    FileWarn: Boolean;
  end;

  TGoogleBackupList = array of TGoogleBackupRecord;

  TGoogleBackupAbstract = class (TComponent)
  protected
    FileList: TGoogleBackupList;
    // used by Sync. It returns 0 for success of
    // -155423=disk full, -155443=write error, -155453=communication error
    function SyncOne:  Integer; virtual; abstract;
  public
    ClientID: String;
    ClientSecret: String;
    RootPath: String;
    LastSync: TDatetime;
    //The function will fill FileList. It returns the list Count or an error code. It will create a record in
    //FileList for each file in Google Drive updated after LastSync
    //Error codes: -155381=communication error, -155383=time out, -155387, -155399
    function FillList: Integer; virtual; abstract;
    // sync is always from Drive to PC. It will go thru FileList and call SyncOne for each element of the
    // list. It will exit for any error code returned by SyncOne
    function Sync: Integer; virtual; abstract;
  end;

//Implementation

  TGoogleBackupImplement = class (TGoogleBackupAbstract)
  private
    FGoogle: TGoogleRestApi;
    FIndex: Cardinal;
    FFil: array of String;
    FUrl: array of String;
    FMim: array of String;
    FDir: array of Boolean;
    procedure Reset;
    function FileSize(FileName: AnsiString): UInt64;
  protected
    function SyncOne:  Integer; override;
  public
    function Authorize: Boolean;
    function FillList: Integer; override;
    function Sync: Integer; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

implementation

{TGoogleBackupImplement}

//Create class
constructor TGoogleBackupImplement.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Reset;
  FGoogle:=TGoogleRestApi.Create;
  FGoogle.Host:='https://www.googleapis.com';
  FGoogle.Logging:=True;
  FGoogle.Active:=True;
  ClientID:=FGoogle.ClientId;
  ClientSecret:=FGoogle.ClientSecret;
  RootPath:=FGoogle.ClientRoot;
end;

//Destroy class
destructor TGoogleBackupImplement.Destroy;
var n: Integer;
begin
  if Assigned(FileList)=True then
  begin
    for n:=Low(FileList) to High(FileList) do
    begin
      FileList[n].FilePath:='';
      FileList[n].FileName:='';
      FileList[n].FileDate:=0;
      FileList[n].FileSize:=0;
      FileList[n].FileWarn:=False;
      FileList[n].Destroy;
      FileList[n]:=nil;
      FFil[n]:='';
      FUrl[n]:='';
      FMim[n]:='';
      FDir[n]:=False;
    end;
  end;
  FreeAndNil(FGoogle);
  Reset;
  inherited Destroy;
end;

//Reset variables
procedure TGoogleBackupImplement.Reset;
begin
  ClientID:='';
  ClientSecret:='';
  RootPath:='';
  LastSync:=0;
  FileList:=nil;
  FIndex:=0;
  FGoogle:=nil;
  FFil:=nil;
  FUrl:=nil;
  FMim:=nil;
  FDir:=nil;
end;

//Check file size
function TGoogleBackupImplement.FileSize(FileName: AnsiString): UInt64;
  var
    hFile: Int64;
    hSize: Int64;
begin
  Result:=0;
  hFile:=FileOpen(FileName,fmOpenRead+fmShareDenyWrite);
  if hFile<>-1 then
  begin
    hSize:=FileSeek(hFile,0,2);
    if hSize<>-1 then Result:=hSize;
    FileClose(hFile);
  end;
end;

//Download file
function TGoogleBackupImplement.SyncOne:  Integer;
var n: Cardinal;
    Bin: TMemoryStream;
    Old: TMemoryStream;
begin
  if Assigned(FFil)=True then
  begin
    Write('[ DO ] TRY '+FFil[FIndex]);
    if FileList[FIndex].FileWarn=True then
    begin
      WriteLn(#13+'[ WR ] WARNING '+FFil[FIndex]);
      FIndex:=FIndex+1;
      Result:=SUCCESS_OPERATION;
      Exit;
    end;
    if FDir[FIndex]=False then
    begin
      if FileExists(FFil[FIndex])=True then
      begin
        if FileList[FIndex].FileSize<>FileSize(FFil[FIndex]) then
        begin
          Write(#13+'[ DO ] UPDATE FILE '+FFil[FIndex]);
          Bin:=FGoogle.GDown(FMim[FIndex],FUrl[FIndex]);
          if Assigned(Bin)=True then
          begin
            if (FileList[FIndex].FileSize=0) and (Bin.Size>0) then FileList[FIndex].FileSize:=Bin.Size;
            if FileList[FIndex].FileSize=Bin.Size then
            begin
              if DeleteFile(FFil[FIndex])=True then
              begin
                try
                  Bin.SaveToFile(FFil[FIndex]);
                  if FileList[FIndex].FileSize=FileSize(FFil[FIndex]) then
                  begin
                    Writeln(#13+'[ OK ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' '+IntToStr(FileSize(FFil[FIndex])));
                    FIndex:=FIndex+1;
                    Result:=SUCCESS_OPERATION;
                  end
                  else
                  begin
                    Writeln(#13+'[ ER ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' '+IntToStr(FileSize(FFil[FIndex]))+' INVALID SIZE');
                    Result:=ERROR_SYNCONE_WRITE;
                  end;
                except
                  on E : Exception do
                  begin
                    Writeln(#13+'[ ER ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 '+E.Message);
                    Result:=ERROR_SYNCONE_WRITE;
                  end;
                end;
              end
              else
              begin
                Writeln(#13+'[ ER ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 INVALID DELETE');
                Result:=ERROR_SYNCONE_WRITE;
              end;
            end
            else
            begin
              Writeln(#13+'[ ER ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 INVALID SIZE');
              Result:=ERROR_SYNCONE_COMM;
            end;
            Bin.Destroy;
            Bin:=nil;
          end
          else
          begin
            Writeln(#13+'[ ER ] UPDATE FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' 0 0 INVALID DOWNLOAD');
            Result:=ERROR_SYNCONE_COMM;
          end;
        end
        else
        begin
          Writeln(#13+'[ OK ] SKIP FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' 0 '+IntToStr(FileSize(FFil[FIndex])));
          FIndex:=FIndex+1;
          Result:=SUCCESS_OPERATION;
        end;
      end
      else
      begin
        Write(#13+'[ DO ] COPY FILE '+FFil[FIndex]);
        Bin:=FGoogle.GDown(FMim[FIndex],FUrl[FIndex]);
        if Assigned(Bin)=True then
        begin
          if (FileList[FIndex].FileSize=0) and (Bin.Size>0) then FileList[FIndex].FileSize:=Bin.Size;
          if FileList[FIndex].FileSize=Bin.Size then
          begin
            try
              Bin.SaveToFile(FFil[FIndex]);
              Writeln(#13+'[ OK ] COPY FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' '+IntToStr(FileSize(FFil[FIndex])));
              FIndex:=FIndex+1;
              Result:=SUCCESS_OPERATION;
            except
               on E : Exception do
               begin
                 try
                   Bin.SaveToFile(Trim(FileList[FIndex].FilePath)+'RENAME_'+Trim(FileList[FIndex].FileName));
                   FIndex:=FIndex+1;
                   Result:=SUCCESS_OPERATION;
                   writeln(#13+'[ OK ] COPY FILE '+Trim(FileList[FIndex].FilePath)+'RENAME_'+Trim(FileList[FIndex].FileName)+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 ');
                 except
                   Result:=ERROR_SYNCONE_WRITE;
                   writeln(#13+'[ ER ] COPY FILE '+Trim(FileList[FIndex].FilePath)+'RENAME_'+Trim(FileList[FIndex].FileName)+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 FATAL ERROR');
                 end;
               end;
            end;
          end
          else
          begin
            Writeln(#13+'[ ER ] COPY FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' '+IntToStr(Bin.Size)+' 0 ERROR DOWNLOAD SIZE');
            Result:=ERROR_SYNCONE_COMM;
          end;
          Bin.Destroy;
          Bin:=nil;
        end
        else
        begin
          Writeln(#13+'[ ER ] COPY FILE '+FFil[FIndex]+' '+IntToStr(FileList[FIndex].FileSize)+' 0 0 ERROR DOWNLOAD');
          Result:=ERROR_SYNCONE_COMM;
        end;
      end;
    end
    else
    begin
      if DirectoryExists(FFil[FIndex])=True then
      begin
        Writeln(#13+'[ OK ] SKIP DIR '+FFil[FIndex]);
        FIndex:=FIndex+1;
        Result:=SUCCESS_OPERATION;
      end
      else
      begin
        try
          if ForceDirectories(FFil[FIndex])=True then
          begin
            Writeln(#13+'[ OK ] COPY DIR '+FFil[FIndex]);
            FIndex:=FIndex+1;
            Result:=SUCCESS_OPERATION;
          end
          else
          begin
            Writeln(#13+'[ ER ] COPY DIR '+FFil[FIndex]+' INVALID NAME');
            Result:=ERROR_SYNCONE_WRITE;
          end;
        except
          on E : Exception do
          begin
            writeln(#13+'[ ER ] COPY DIR '+FFil[FIndex]+' '+E.Message);
            Result:=ERROR_SYNCONE_WRITE;
          end;
        end;
      end;
    end;
  end
  else
  begin
    Result:=ERROR_SYNCONE_WRITE;
  end;
end;

//Authorization
function TGoogleBackupImplement.Authorize: Boolean;
var Code: AnsiString;
begin
  if FGoogle.OAuth2p0Refresh=False then
  begin
    if FGoogle.OAuth2p0GoogleStart=True then
    begin
      writeln('-------------------------');
      writeln('| GOOGLE AUTHORIZATION  |');
      writeln('-------------------------');
      writeln;
      Code:='';
      writeln('Paste your key and press ENTER: ');
      readln(Code);
      if FGoogle.OAuth2p0GoogleComplite(Code)=True then
      begin
        Result:=True;
      end
      else
      begin
        Result:=False;
      end;
    end
    else
    begin
      Result:=False;
    end;
  end
  else
  begin
    Result:=True;
  end;
end;

//Fill file list
function TGoogleBackupImplement.FillList: Integer;
var Obj: ISuperObject;
      n,m: Cardinal;
      Str: AnsiString;
      War: Boolean;

begin
  Result:=SUCCESS_OPERATION;
  War:=False;
  Write('[ DO ] Download google drive meta data');
  Obj:=FGoogle.GLoad(True);
  if Assigned(Obj)=True then
  begin
    WriteLn(#13+'[ OK ] Download google drive meta data');
    Write('[ DO ] Get files count');
    n:=0;
    while Assigned(Obj.A['items'].O[n])=True do
    begin
      //if (Obj.A['items'].O[n].B['shared']=False) and
      //   (Obj.A['items'].O[n].B['explicitlyTrashed']=False) and
      //   (Obj.A['items'].O[n].B['labels.trashed']=False) then
      //begin
        Result:=Result+1;
      //end;
      n:=n+1;
    end;
    WriteLn(#13+'[ OK ] Get files count '+IntToStr(Result));
    if Result>0 then
    begin
      Write('[ DO ] Preparing the environment');
      FIndex:=0;
      FFil:=nil;
      FUrl:=nil;
      FileList:=nil;
      SetLength(FFil,Result);
      SetLength(FUrl,Result);
      SetLength(FMim,Result);
      SetLength(FDir,Result);
      SetLength(FileList,Result);
      for n:=0 to Result-1 do
      begin
        FileList[n]:=TGoogleBackupRecord.Create(nil);
        FileList[n].FilePath:='';
        FileList[n].FileName:='';
        FileList[n].FileDate:=0;
        FileList[n].FileSize:=0;
        FileList[n].FileWarn:=False;
        FFil[n]:='';
        FUrl[n]:='';
        FMim[n]:='';
        FDir[n]:=False;
      end;
      WriteLn(#13+'[ OK ] Preparing the environment');
      n:=0;
      while Assigned(Obj.A['items'].O[n])=True do
      begin

        //if (Obj.A['items'].O[n].B['shared']=False) and
        //   (Obj.A['items'].O[n].B['explicitlyTrashed']=False) and
        //   (Obj.A['items'].O[n].B['labels.trashed']=False) then
       // begin
          War:=False;
          Str:=FGoogle.GCorn(FGoogle.GDest(Obj,Obj.A['items'].O[n].S['id'],War),Obj.A['items'].O[n].S['mimeType']);
          if Obj.A['items'].O[n].S['mimeType']='application/vnd.google-apps.folder' then
          begin
            FileList[FIndex].FilePath:=Str;
            FileList[FIndex].FileName:='';
            FileList[FIndex].FileSize:=0;
            FileList[FIndex].FileDate:=0;
            FileList[FIndex].FileWarn:=War;
            FFil[FIndex]:=Str;
            FUrl[FIndex]:='';
            FMim[FIndex]:=Obj.A['items'].O[n].S['mimeType'];
            FDir[FIndex]:=True;
            if FileList[FIndex].FileWarn=False then
            begin
              try
                ForceDirectories(Str);
                WriteLn('[ OK ] LIST DIR '+Str);
              except
                WriteLn('[ WR ] WARNING '+Str);
              end;
            end
            else
            begin
              WriteLn('[ WR ] WARNING '+Str);
            end;
          end
          else
          begin
            FileList[FIndex].FilePath:=ExtractFilePath(Str);
            FileList[FIndex].FileName:=ExtractFileName(Str);
            FileList[FIndex].FileSize:=Obj.A['items'].O[n].I['fileSize'];
            FileList[FIndex].FileDate:=0;
            FileList[FIndex].FileWarn:=War;
            FFil[FIndex]:=Str;
            if Obj.A['items'].O[n].S['downloadUrl']<>'' then
            begin
              FUrl[FIndex]:=Obj.A['items'].O[n].S['downloadUrl'];
            end
            else
            begin
              FUrl[FIndex]:=Obj.A['items'].O[n].S['exportLinks.application/zip'];
            end;
            FMim[FIndex]:=Obj.A['items'].O[n].S['mimeType'];
            FDir[FIndex]:=False;
            if FileList[FIndex].FileWarn=False then
            begin
              WriteLn('[ OK ] LIST FILE '+Str+' '+IntToStr(Obj.A['items'].O[n].I['fileSize']));
            end
            else
            begin
              WriteLn('[ WR ] WARNING '+Str+' '+IntToStr(Obj.A['items'].O[n].I['fileSize']));
            end;
          end;
          FIndex:=FIndex+1;
        //end;
        n:=n+1;
      end;
      WriteLn('[ DO ] Search homonyms');
      for n:=Low(FFil) to High(FFil) do
      begin
        if FUrl[n]<>'' then
        begin
          for m:=Low(FFil) to High(FFil) do
          begin
            if (m<>n) and (n<m) and (FFil[n]=FFil[m]) then
            begin
              FFil[m]:=FFil[m]+'.homonym';
              WriteLn('[ OK ] HOMONYM '+FFil[m]);
              break;
            end;
          end;
        end;
      end;
      WriteLn('[ OK ] Search homonyms');
      FIndex:=0;
    end;
  end
  else
  begin
    WriteLn(#13+'[ ER ] Download google drive meta data');
    Result:=ERROR_FILLLIST_COMM;
  end;
  FIndex:=0;
end;

//Download files
function TGoogleBackupImplement.Sync: Integer;
var n: Cardinal;
begin
  if Assigned(FFil)=True then
  begin
    for n:=Low(FFil) to High(FFil) do
    begin
      try
        Result:=SyncOne;
        if Result<>SUCCESS_OPERATION then FIndex:=FIndex+1;
      except
        on E : Exception do
        begin
          Result:=ERROR_SYNCONE_WRITE;
          FIndex:=FIndex+1;
        end;
      end;
    end;
    LastSync:=Now;
  end
  else
  begin
    Result:=ERROR_SYNCONE_WRITE;
  end;
end;

end.

